<?php
namespace Admin\Controller;
use Think\Controller;
class AuthController extends Controller {

	public function _initialize(){
		$this -> namespace = strtolower(MODULE_NAME.'_'.CONTROLLER_NAME);
	}

	public function index(){
		$this -> login();
	}

	// 登录
	public function login(){
		// 登陆页面
		if (IS_GET) {
			$this -> display('login');
		}
		
		// 登录动作
		if (IS_POST) {
			// 验证验证码
			$verify = new \Think\Verify();
			if(!$verify->check(I('verify'))){
				$this -> error('verify');
			}

			// 清空session
			session(null);
			// 读取该账户
			$where = array(
				'nickname' => I('nickname'),
				'password' => crypt_pwd(I('password'))
				);
			if ($res = D('User','RelationModel') -> where($where) -> relation('group') -> find()) {
				// 判断账户状态
				if ($res['status'] != 1) {
					$this -> error('forbidden');
				}
				// 超级管理员识别
				if ($res['nickname'] == C('SUPERADMIN')) {
					session('__superadmin',true);
				}else{
					// 判断分组状态
					$_status = 0;
					foreach ($res['group'] as $k => $v) {
						$_status += $v['status'];
					}
					if (!$_status) {
						$this -> error('forbidden');
					}
				}
				// 更新数据库
				$updata = array(
					'login_ip' => get_client_ip(1,true),
					'login_time' => datetime(),
					'login_times'=>$res['login_times']+1
					);
				M('User') -> where('id='.$res['id']) -> save($updata);
				// 写入新session
				session('__user_id',$res['id']);
				session('__nickname',$res['nickname']);
				$this -> success('登陆成功!',U('Admin/Index/index'),true);
			} else {
				$this -> error('password');
			}

		}
	}

	// 退出
	public function logout(){
		if (IS_GET) {
			session(null);
			$this -> success('退出成功',U('Admin/Index/index'));
		}
	}
}