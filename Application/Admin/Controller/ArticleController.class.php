<?php
namespace Admin\Controller;
class ArticleController extends CommonController {

	public function add(){
		if (IS_GET) {
			$model = M('Articlecate') -> where('id='.I('category_id')) -> getField('model'); 
			$this -> form('',array('ext_id'=>$model));
		}elseif (IS_POST) {
			$p = array(
				'relation' => array('article_body'),
				);
			$this -> ebAdd($p);
		}
	}

	public function save(){
		if (IS_GET) {
			$data = D('Article','RelationModel') -> relation(true) -> find(I('id','','intval'));
			$this -> form($data,array('ext_id'=>$data['articlecate']['model']));
		}elseif (IS_POST) {
			$p = array(
				'relation' => array('article_body'),
				);
			$this -> ebSave($p);
		}
	}

}