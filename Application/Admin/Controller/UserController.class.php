<?php
namespace Admin\Controller;
class UserController extends CommonController {

	public function add(){
		if (IS_GET) {
			$this -> form();
		}elseif (IS_POST) {
			$this -> ebAdd();
		}
	}

	public function save(){
		if (IS_GET) {
			$this -> form(M('User') -> find(I('id','','intval')));
		}elseif (IS_POST) {
			$this -> ebSave();
		}
	}

	// 分配用户组
	public function group(){
		$user_id = I('user_id');
		if (IS_GET) {
			$this -> success($this -> fetch());
		}elseif (IS_POST) {
			if (I('__type') == 'group') {
				// 获取该memeber下的用户组
				$checked = M('Auth_group_access') -> where('uid='.$user_id) -> getField('group_id',true);
				$groups = M('Auth_group') -> where('status=1') -> select();
				$res = array(
					'rows' => array_mark($groups,$checked,'id','checked',true,false),
					);
				$this -> success($res);
			}else{
				// 重新分配用户组
				// 移除老分组
				M('Auth_group_access') -> where(array('uid' => array('eq',$user_id))) -> delete();
				// 重组新分组
				$group_ids = I('group_ids');
				if ($group_ids) {
					$data = array();
					foreach ($group_ids as $key => $value) {
					$data[] = array(
						'uid'=>$user_id,
						'group_id'=>$value,
						);
					}
					if (false !== M('Auth_group_access') -> addAll($data)) {
						$this -> success('用户组分配成功！');
					}else{
						$this -> error(M('Auth_group_access') -> getDbError());
					}
				}else{
					$this -> success('用户组分配成功！');
				}
			}
		}
	}

	// 重置密码
	public function password(){
		if (IS_GET) {
			$this -> display();
		}elseif (IS_POST) {
			$password = I('password');
			$id = I('id','','intval');
			$m = M('User');
			if (false !== $m -> where('id='.$id) -> setField('password',crypt_pwd($password))) {
				$this -> success('密码修改成功','',true);
			}else{
				$this -> error($m -> getDbError(),'',true);
			}
		}
	}
}