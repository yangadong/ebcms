/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : abc1

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2016-01-28 21:31:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ebcms_article`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_article`;
CREATE TABLE `ebcms_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '标志',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `tpl` varchar(255) NOT NULL DEFAULT '' COMMENT '模板',
  `ext` text COMMENT '扩展信息',
  `click` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击量',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='文章基本内容表';

-- ----------------------------
-- Records of ebcms_article
-- ----------------------------
INSERT INTO `ebcms_article` VALUES ('1', '1', 'gsjj', '公司简介', '公司简介', '/image/20160128/56a9c5f54d041.png', '公司简介', '', null, '16', '1453966838', '1453639595', '99', '1', '1');
INSERT INTO `ebcms_article` VALUES ('2', '1', 'lxwm', '联系我们', '联系我们', '', '联系我们', '', null, '98', '1453968820', '1453639691', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('3', '2', '', '易贝后台开发框架1.0正式版发布', '易贝后台框架,ebcore', '', '易贝后台开发框架1.0正式版发布', '', null, '3', '1453639868', '1453639868', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('4', '2', '', '易贝后台开发框架1.0.1修正版发布', '易贝后台开发框架', '', '易贝后台开发框架1.0.1修正版发布', '', null, '2', '1453639904', '1453639904', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('5', '2', '', '易贝后台开发框架1.1正式版发布', '易贝后台开发框架', '', '易贝后台开发框架1.1正式版发布', '', null, '4', '1453639973', '1453639973', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('6', '2', '', '易贝后台开发框架1.2正式版发布', '易贝后台开发框架', '', '易贝后台开发框架1.2正式版发布', '', null, '5', '1453900631', '1453640002', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('7', '2', '', '易贝后台开发框架1.5正式版发布', '易贝后台开发框架', '', '易贝后台开发框架1.5正式版发布', '', null, '8', '1453640102', '1453640102', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('8', '3', '', '易贝企业网站管理系统', '企业网站管理系统,易贝企业网站管理系统', '', '易贝企业网站管理系统是免费的企业网站系统，专门做企业网站开发，更有针对性，操作更简单', '', '{\"pics\":\"\"}', '1', '1453640883', '1453640883', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('9', '3', '', '易贝内容管理系统', '易贝内容管理系统,ebcms,易贝cms,phpcms', '', '易贝内容管理系统是一套强大的免费的内容管理系统', '', '{\"pics\":\"\"}', '3', '1453641439', '1453641439', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('10', '3', '', '易贝后台开发框架', '易贝后台开发框架', '', '易贝后台开发框架是一套底层功能完善的php后台开发框架', '', '{\"pics\":\"\"}', '3', '1453642025', '1453642025', '0', '1', '1');
INSERT INTO `ebcms_article` VALUES ('11', '1', '', '人才招聘', '人才招聘', '', '人才招聘', '', null, '30', '1453642718', '1453642718', '0', '1', '1');

-- ----------------------------
-- Table structure for `ebcms_articlecate`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_articlecate`;
CREATE TABLE `ebcms_articlecate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `pid` int(10) unsigned NOT NULL COMMENT '父ID',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '标识',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `model` varchar(255) NOT NULL DEFAULT '' COMMENT '模型',
  `ext` text COMMENT '扩展信息',
  `pagenum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分页大小',
  `tpl` varchar(255) NOT NULL DEFAULT '' COMMENT '模板',
  `tpl_detail` varchar(255) NOT NULL DEFAULT '' COMMENT '内容页模板',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='文章分类表';

-- ----------------------------
-- Records of ebcms_articlecate
-- ----------------------------
INSERT INTO `ebcms_articlecate` VALUES ('1', '0', 'gsxg', '公司相关', '', '', '', '', null, '0', '', '', '1453639524', '1453639477', '99', '1', '0');
INSERT INTO `ebcms_articlecate` VALUES ('2', '0', 'gsdt', '公司动态', '', '', '', '', null, '0', '', '', '1453639785', '1453639777', '90', '1', '0');
INSERT INTO `ebcms_articlecate` VALUES ('3', '0', 'cpzx', '产品中心', '', '', '', '1', null, '0', '', '', '1453644317', '1453640734', '80', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_article_body`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_article_body`;
CREATE TABLE `ebcms_article_body` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `body` text NOT NULL COMMENT '内容主体',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='文章详细内容表';

-- ----------------------------
-- Records of ebcms_article_body
-- ----------------------------
INSERT INTO `ebcms_article_body` VALUES ('1', '&lt;p&gt;四川易贝网络科技有限公司成立于2015年8月，由5名刚毕业的大学生共同创建&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('2', '&lt;p&gt;地址：成都高新区环球中心A座13楼&lt;/p&gt;&lt;p&gt;电话：18000534006&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;/abc/localhost/Uploads/image/20160128/56a9cdb24c9ad.jpg&quot; title=&quot;56a9cdb24c9ad.jpg&quot; alt=&quot;21144616968.jpg&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('3', '&lt;p&gt;2015-08-01 V1.0 正式版发布&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;box-sizing: border-box; outline: none;&quot;/&gt;&lt;/p&gt;&lt;p&gt;产品主要功能：&lt;/p&gt;&lt;p&gt;RBAC权限管理&lt;/p&gt;&lt;p&gt;功能菜单权限管理&lt;/p&gt;&lt;p&gt;安装模块&lt;/p&gt;&lt;p&gt;配置管理&lt;/p&gt;&lt;p&gt;模板管理&lt;/p&gt;&lt;p&gt;操作日志&lt;/p&gt;&lt;p&gt;调试日志&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;box-sizing: border-box; outline: none;&quot;/&gt;&lt;/p&gt;&lt;p&gt;数据备份&lt;/p&gt;&lt;p&gt;EXCEL导出&lt;/p&gt;&lt;p&gt;邮件功能&lt;/p&gt;&lt;p&gt;多文件上传&lt;/p&gt;&lt;p&gt;百度编辑器深度整合&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('4', '&lt;p&gt;2015-08-05 V1.0.1 修正版发布&lt;/p&gt;&lt;p&gt;1.修复安装模块的少量逻辑bug&lt;/p&gt;&lt;p&gt;2.其他常规更新&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('5', '&lt;p&gt;2015-09-20 V1.1 正式版发布&lt;/p&gt;&lt;ol class=&quot; list-paddingleft-2&quot;&gt;&lt;li&gt;&lt;p&gt;更新tp框架至最新版本&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进前台性能（不用加载不必要的后台函数）&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进配置方式，配置分为核心配置和基本配置，核心配置会生成配置文件，基本配置不会写入配置文件，在需要的地方通过FC函数从缓存文件中调用&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;增强模板编辑安全性&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;去除开发者模式，去除node，menu、config、configcate。等的system字段，核心配置和基本配置仅供开发这使用（用户反映这两个地方较难理解）,增加便捷配置，便捷配置供大众使用&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;优化附件管理操作&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进tp系统的RBAC类的bug&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;优化后台主表面板的排序、操作等&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进数据备份及还原操作&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进编辑器，附件上传等组件的封装函数&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;操作日志、调试日志等存储由json格式改为序列化格式&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;后台的一些样式修复及美化&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;封装筛选函数，开发筛选功能更加方便&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进后台登录操作&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;。。。&lt;/p&gt;&lt;/li&gt;&lt;/ol&gt;&lt;p&gt;这一版本较上一版本做了大量改进，远不止上面描述的这些。&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('6', '&lt;ol class=&quot; list-paddingleft-2&quot;&gt;&lt;li&gt;&lt;p&gt;新增 定时任务&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;新增 行为绑定&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进 配置功能&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;改进 权限控制&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;修复 编辑器 附件上传等若干小bug&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;。。。&lt;/p&gt;&lt;/li&gt;&lt;/ol&gt;');
INSERT INTO `ebcms_article_body` VALUES ('7', '&lt;p&gt;2015-12-9 V1.5 正式版发布&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;新增 自定义字段&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;新增 文章管理 友情链接 留言中心 导航菜单&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;新增 推荐位管理（广告模块）&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;新增 会员管理（AUTH权限）&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;新增 前台展示&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;修复 已知BUG&lt;br style=&quot;box-sizing: border-box; outline: none; color: rgb(50, 50, 50); font-family: &amp;#39;Century Gothic&amp;#39;, &amp;#39;Microsoft yahei&amp;#39;; line-height: 28.8px;&quot;/&gt;改进 前端和后端封装的底层函数&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;box-sizing: border-box; outline: none;&quot;/&gt;&lt;/p&gt;&lt;p&gt;后台演示地址：&lt;a href=&quot;http://ebcore.ebcms.com/admin&quot; target=&quot;_blank&quot;&gt;http://ebcore.ebcms.com/admin&lt;/a&gt;&lt;/p&gt;&lt;p&gt;账户：工程师 密码：123456&lt;/p&gt;&lt;p&gt;账号：管理员 密码：123456&lt;/p&gt;&lt;p&gt;账户：编辑 &amp;nbsp;密码：123456&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;box-sizing: border-box; outline: none;&quot;/&gt;&lt;/p&gt;&lt;p&gt;另附上2.0测试版地址：&lt;a href=&quot;http://beta.ebcms.com/admin&quot; target=&quot;_blank&quot; textvalue=&quot;http://beta.ebcms.com/admin&quot;&gt;http://beta.ebcms.com/admin&lt;/a&gt;&lt;/p&gt;&lt;p&gt;账户：admin 密码：123456&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('8', '&lt;p&gt;易贝企业网站管理系统是免费的企业网站系统，专门做企业网站开发，更有针对性，操作更简单&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('9', '&lt;p&gt;易贝内容管理系统是一套强大的免费的内容管理系统，采用流行的PHP+MYSQL架构，面向对象编程，后台操作便捷，强大的seo优化功能，让你的网站排名更靠前。&lt;/p&gt;&lt;p&gt;功能：&lt;/p&gt;&lt;ol class=&quot; list-paddingleft-2&quot; style=&quot;list-style-type: decimal;&quot;&gt;&lt;li&gt;&lt;p&gt;文章管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;推荐管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;会员管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;导航管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;友情连接&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;留言中心&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;自定义模型&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;自定义导航&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;自定义用户组&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;强大的权限控制机制&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;定时任务&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;附件管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;。。。&lt;/p&gt;&lt;/li&gt;&lt;/ol&gt;&lt;p&gt;在此基础上，可以方便的扩展出一个综合性网站 下站网站 新闻网站 图库网站 信息发布网站。。。&lt;br/&gt;&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('10', '&lt;p&gt;易贝后台开发框架是一套底层功能完善的php后台开发框架，MVC编码，维护更加容易，目前包含两个版本：bootstrap版本和easyui版本可供选择&lt;/p&gt;&lt;p&gt;功能：&lt;/p&gt;&lt;ul class=&quot; list-paddingleft-2&quot; style=&quot;list-style-type: disc;&quot;&gt;&lt;li&gt;&lt;p&gt;AUTH权限管理（角色管理 人员管理 权限规则。）&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;富文本编辑器（ueditor）&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;附件上传（webuploader）&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;功能菜单权限&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;附件管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;操作记录&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;模板管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;自定义模型&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;数据库备份优化&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;模型管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;数据字典管理&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;后台配置编辑&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;后台表单编辑配置&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;。。。&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;有了这样一个强大的基础框架，开发网站更快速，底层更稳健安全，开发者能用更多的时间做有意义的编码。&lt;/p&gt;');
INSERT INTO `ebcms_article_body` VALUES ('11', '&lt;p&gt;&lt;strong&gt;PHP工程师&lt;/strong&gt; 1名 负责功能模块开发&lt;/p&gt;&lt;p&gt;要求：&lt;/p&gt;&lt;ul class=&quot; list-paddingleft-2&quot; style=&quot;list-style-type: disc;&quot;&gt;&lt;li&gt;&lt;p&gt;掌握ThinkPHP框架&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;有作品&lt;br/&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;熟练使用EasyUI和jQuery框架&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;熟练使用bootstrap&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;前端工程师&lt;/strong&gt; 2名 负责模板网站开发&lt;/p&gt;&lt;p&gt;要求&lt;/p&gt;&lt;ul class=&quot; list-paddingleft-2&quot; style=&quot;list-style-type: disc;&quot;&gt;&lt;li&gt;&lt;p&gt;1年开发经验&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;有多份作品&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;掌握jquery框架（异步请求、局部加载、动画特效）&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;掌握javascript&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;掌握html+css&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;美工&lt;/strong&gt; 1名 负责网站外观设计&lt;/p&gt;&lt;p&gt;要求&lt;/p&gt;&lt;ul class=&quot; list-paddingleft-2&quot; style=&quot;list-style-type: disc;&quot;&gt;&lt;li&gt;&lt;p&gt;掌握PS&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;有多份作品&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;');

-- ----------------------------
-- Table structure for `ebcms_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_attachment`;
CREATE TABLE `ebcms_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '图片id',
  `uid` int(11) unsigned NOT NULL DEFAULT '1' COMMENT 'userid',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '图片说明',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '原始名称',
  `type` varchar(255) NOT NULL DEFAULT '',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '图片大小',
  `ext` varchar(20) NOT NULL DEFAULT '' COMMENT '类型',
  `md5` varchar(32) NOT NULL DEFAULT '' COMMENT 'md5',
  `sha1` char(40) NOT NULL DEFAULT '',
  `savename` varchar(200) NOT NULL DEFAULT '' COMMENT '地区英文名称',
  `savepath` varchar(200) NOT NULL DEFAULT '' COMMENT '保存路径',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(11) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '审核状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='附件表';

-- ----------------------------
-- Records of ebcms_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `ebcms_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_auth_group`;
CREATE TABLE `ebcms_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL COMMENT '规则',
  `c_rules` text NOT NULL COMMENT '栏目权限规则',
  `menus` text NOT NULL COMMENT '菜单权限',
  `iconcls` varchar(255) NOT NULL DEFAULT '' COMMENT '小图标',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='后台账户分组表';

-- ----------------------------
-- Records of ebcms_auth_group
-- ----------------------------
INSERT INTO `ebcms_auth_group` VALUES ('1', '0', '系统', '工程师', '1', '2,3,5,193,194,195,6,35,34,36,37,38,7,39,40,41,42,43,8,44,45,46,47,48,9,49,50,51,52,53,10,54,55,56,57,58,11,59,60,61,62,63,12,64,65,66,67,68,161,13,69,70,71,72,73,14,74,75,76,77,78,162,163,15,79,80,81,82,83,164,17,89,18,90,91,92,19,93,94,95,96,20,97,98,99,100,101,102,103,21,104,105,106,107,108,167,168,22,109,110,111,112,113,23,114,115,116,117,118,26,128,129,130,28,140,169,30,150', '', '72,74,79,80,81,95,96,6,11,13,92,98,103,100,105,106', 'icon-user_thief', '77', '1');
INSERT INTO `ebcms_auth_group` VALUES ('2', '0', '系统', '管理员', '1', '2,3,5,193,194,195,6,35,34,36,37,38,7,39,40,41,42,43,8,44,45,46,47,48,9,49,50,51,52,53,10,54,55,56,57,58,11,59,60,61,62,63,12,64,65,66,67,68,161,13,69,70,71,72,73,14,74,75,76,77,78,162,163,15,79,80,81,82,83,164,17,89,18,90,91,92,19,93,94,95,96,20,97,98,99,100,101,102,103,21,104,105,106,107,108,167,168,22,109,110,111,112,113,23,114,115,116,117,118,26,128,129,130,28,140,169,30,150', '', '72,74,79,80,81,95,96,6,13,92,98,103,100,105,106', 'icon-user_business_boss', '33', '1');
INSERT INTO `ebcms_auth_group` VALUES ('3', '0', '系统', '编辑', '1', '2,3,5,193,194,195,6,7,39,40,41,43,8,9,10,54,55,56,58,11,59,60,61,63,12,64,65,66,68,161,13,69,70,71,73,19,93,94,96', '', '72,74,79,80,81,95,96,6', 'icon-user_business', '99', '1');

-- ----------------------------
-- Table structure for `ebcms_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_auth_group_access`;
CREATE TABLE `ebcms_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='后台账号角色权限表';

-- ----------------------------
-- Records of ebcms_auth_group_access
-- ----------------------------
INSERT INTO `ebcms_auth_group_access` VALUES ('2', '2');
INSERT INTO `ebcms_auth_group_access` VALUES ('3', '3');
INSERT INTO `ebcms_auth_group_access` VALUES ('6', '1');
INSERT INTO `ebcms_auth_group_access` VALUES ('6', '2');

-- ----------------------------
-- Table structure for `ebcms_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_auth_rule`;
CREATE TABLE `ebcms_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `iconcls` varchar(255) NOT NULL DEFAULT '' COMMENT '小图标',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  `sys_mark` varchar(255) NOT NULL DEFAULT '' COMMENT '系统标志',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=196 DEFAULT CHARSET=utf8 COMMENT='后台账户权限规则表';

-- ----------------------------
-- Records of ebcms_auth_rule
-- ----------------------------
INSERT INTO `ebcms_auth_rule` VALUES ('2', '0', '节点权限', 'Admin', '核心后台', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('3', '2', '节点权限', 'Admin_Index_index', '首页', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('5', '3', '节点权限', 'Admin_Index_runtime', '清理缓存', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('6', '2', '节点权限', 'Admin_Articlecate_index', '文章分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('7', '2', '节点权限', 'Admin_Article_index', '文章管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('8', '2', '节点权限', 'Admin_Navcate_index', '导航分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('9', '2', '节点权限', 'Admin_Nav_index', '导航管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('10', '2', '节点权限', 'Admin_Link_index', '友情链接', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('35', '6', '节点权限', 'Admin_Articlecate_save', '修改 文章分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('11', '2', '节点权限', 'Admin_Recommendcate_index', '推荐分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('12', '2', '节点权限', 'Admin_Recommend_index', '推荐管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('13', '2', '节点权限', 'Admin_Guestbook_index', '留言管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('14', '2', '节点权限', 'Admin_User_index', '会员管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('15', '2', '节点权限', 'Admin_Group_index', '会员组别管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('18', '2', '节点权限', 'Admin_Template_index', '模板管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('19', '2', '节点权限', 'Admin_Attachment_index', '附件管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('20', '2', '节点权限', 'Admin_Database_index', '数据库管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('21', '2', '节点权限', 'Admin_Cron_index', '定时任务管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('22', '2', '节点权限', 'Admin_Model_index', '模型管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('23', '2', '节点权限', 'Admin_Modelfield_index', '模型字段管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('26', '2', '节点权限', 'Admin_Datadict_index', '数据字典', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('28', '2', '节点权限', 'Admin_Config_index', '配置管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('30', '2', '节点权限', 'Admin_Conf_index', '核心配置管理', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('34', '6', '节点权限', 'Admin_Articlecate_add', '添加 文章分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('36', '6', '节点权限', 'Admin_Articlecate_status', '审核 文章分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('37', '6', '节点权限', 'Admin_Articlecate_delete', '删除 文章分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('38', '6', '节点权限', 'Admin_Articlecate_lock', '锁定 文章分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('39', '7', '节点权限', 'Admin_Article_add', '添加 文章', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('40', '7', '节点权限', 'Admin_Article_save', '修改 文章', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('41', '7', '节点权限', 'Admin_Article_status', '审核 文章', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('42', '7', '节点权限', 'Admin_Article_lock', '锁定 文章', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('43', '7', '节点权限', 'Admin_Article_delete', '删除 文章', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('44', '8', '节点权限', 'Admin_Navcate_add', '添加 导航分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('45', '8', '节点权限', 'Admin_Navcate_save', '修改 导航分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('46', '8', '节点权限', 'Admin_Navcate_status', '审核 导航分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('47', '8', '节点权限', 'Admin_Navcate_lock', '锁定 导航分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('48', '8', '节点权限', 'Admin_Navcate_delete', '删除 导航分类', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('49', '9', '节点权限', 'Admin_Nav_add', '添加 导航', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('50', '9', '节点权限', 'Admin_Nav_save', '修改 导航', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('51', '9', '节点权限', 'Admin_Nav_status', '审核 导航', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('52', '9', '节点权限', 'Admin_Nav_lock', '锁定 导航', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('53', '9', '节点权限', 'Admin_Nav_delete', '删除 导航', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('54', '10', '节点权限', 'Admin_Link_add', '添加 友情链接', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('55', '10', '节点权限', 'Admin_Link_save', '修改 友情链接', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('56', '10', '节点权限', 'Admin_Link_status', '审核 友情链接', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('57', '10', '节点权限', 'Admin_Link_lock', '锁定 友情链接', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('58', '10', '节点权限', 'Admin_Link_delete', '删除 友情链接', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('59', '11', '节点权限', 'Admin_Recommendcate_add', '添加 推荐位', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('60', '11', '节点权限', 'Admin_Recommendcate_save', '修改 推荐位', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('61', '11', '节点权限', 'Admin_Recommendcate_status', '审核 推荐位', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('62', '11', '节点权限', 'Admin_Recommendcate_lock', '锁定 推荐位', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('63', '11', '节点权限', 'Admin_Recommendcate_delete', '删除 推荐位', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('64', '12', '节点权限', 'Admin_Recommend_add', '添加 推荐内容', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('65', '12', '节点权限', 'Admin_Recommend_save', '修改 推荐内容', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('66', '12', '节点权限', 'Admin_Recommend_status', '审核 推荐内容', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('67', '12', '节点权限', 'Admin_Recommend_lock', '锁定 推荐内容', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('68', '12', '节点权限', 'Admin_Recommend_delete', '删除 推荐内容', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('69', '13', '节点权限', 'Admin_Guestbook_save', '修改 留言', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('70', '13', '节点权限', 'Admin_Guestbook_status', '审核 留言', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('71', '13', '节点权限', 'Admin_Guestbook_reply', '回复 留言', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('72', '13', '节点权限', 'Admin_Guestbook_lock', '锁定 留言', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('73', '13', '节点权限', 'Admin_Guestbook_delete', '删除 留言', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('74', '14', '节点权限', 'Admin_User_add', '添加 会员', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('75', '14', '节点权限', 'Admin_User_save', '修改 会员', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('76', '14', '节点权限', 'Admin_User_status', '审核 会员', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('77', '14', '节点权限', 'Admin_User_lock', '锁定 会员', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('78', '14', '节点权限', 'Admin_User_delete', '删除 会员', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('79', '15', '节点权限', 'Admin_Group_add', '添加 会员组', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('80', '15', '节点权限', 'Admin_Group_save', '修改 会员组', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('81', '15', '节点权限', 'Admin_Group_status', '审核 会员组', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('82', '15', '节点权限', 'Admin_Group_lock', '锁定 会员组', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('83', '15', '节点权限', 'Admin_Group_delete', '删除 会员组', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('90', '18', '节点权限', 'Admin_Template_add', '添加 模板', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('91', '18', '节点权限', 'Admin_Template_save', '修改 模板', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('92', '18', '节点权限', 'Admin_Template_delete', '删除 模板', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('93', '19', '节点权限', 'Admin_Attachment_save', '修改 附件', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('94', '19', '节点权限', 'Admin_Attachment_status', '审核 附件', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('95', '19', '节点权限', 'Admin_Attachment_lock', '锁定 附件', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('96', '19', '节点权限', 'Admin_Attachment_delete', '删除 附件', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('97', '20', '节点权限', 'Admin_Database_exports', '备份 数据库', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('98', '20', '节点权限', 'Admin_Database_showcreate', '查看 表信息', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('99', '20', '节点权限', 'Admin_Database_repair', '修复 数据库', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('100', '20', '节点权限', 'Admin_Database_optimize', '优化 数据库', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('101', '20', '节点权限', 'Admin_Database_delete', '删除 备份', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('102', '20', '节点权限', 'Admin_Database_imports', '还原 备份', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('103', '20', '节点权限', 'Admin_Database_showsql', '预览 备份包', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('104', '21', '节点权限', 'Admin_Cron_add', '添加 定时任务', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('105', '21', '节点权限', 'Admin_Cron_save', '修改 定时任务', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('106', '21', '节点权限', 'Admin_Cron_status', '审核 定时任务', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('107', '21', '节点权限', 'Admin_Cron_lock', '锁定 定时任务', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('108', '21', '节点权限', 'Admin_Cron_delete', '删除 定时任务', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('109', '22', '节点权限', 'Admin_Model_add', '添加 模型', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('110', '22', '节点权限', 'Admin_Model_save', '修改 模型', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('111', '22', '节点权限', 'Admin_Model_status', '审核 模型', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('112', '22', '节点权限', 'Admin_Model_lock', '锁定 模型', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('113', '22', '节点权限', 'Admin_Model_delete', '删除 模型', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('114', '23', '节点权限', 'Admin_Modelfield_add', '添加 模型字段', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('115', '23', '节点权限', 'Admin_Modelfield_save', '修改 模型字段', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('116', '23', '节点权限', 'Admin_Modelfield_status', '审核 模型字段', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('117', '23', '节点权限', 'Admin_Modelfield_lock', '锁定 模型字段', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('118', '23', '节点权限', 'Admin_Modelfield_delete', '删除 模型字段', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('128', '26', '节点权限', 'Admin_Datadict_delete', '删除 数据字典', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('129', '26', '节点权限', 'Admin_Datadict_add', '添加 数据字典', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('130', '26', '节点权限', 'Admin_Datadict_save', '修改 数据字典', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('140', '28', '节点权限', 'Admin_Config_save', '修改 配置', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('150', '30', '节点权限', 'Admin_Conf_save', '修改 核心配置', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('161', '12', '节点权限', 'Admin_Recommend_push', '推送到推荐位', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('162', '14', '节点权限', 'Admin_User_password', '重置密码', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('163', '14', '节点权限', 'Admin_User_group', '分配用户组', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('164', '15', '节点权限', 'Admin_Group_rule', '分配权限', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('167', '21', '节点权限', 'Admin_Cron_build', '生成 定时任务', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('168', '21', '节点权限', 'Admin_Cron_config', '配置 定时任务', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('169', '28', '节点权限', 'Admin_Config_build', '生成 配置', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('193', '3', '节点权限', 'Admin_Index_ueditor', '编辑器 附件上传', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('194', '3', '节点权限', 'Admin_Index_upload', '附件上传', '1', '1', '', '', '99', '1', '');
INSERT INTO `ebcms_auth_rule` VALUES ('195', '3', '节点权限', 'Admin_Index_password', '修改密码', '1', '1', '', '', '99', '1', '');

-- ----------------------------
-- Table structure for `ebcms_behavior`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_behavior`;
CREATE TABLE `ebcms_behavior` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `behavior` varchar(255) NOT NULL DEFAULT '' COMMENT '行为名称',
  `hook` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT '生效模块',
  `iconcls` varchar(20) NOT NULL DEFAULT '' COMMENT '图标类',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='行为表';

-- ----------------------------
-- Records of ebcms_behavior
-- ----------------------------
INSERT INTO `ebcms_behavior` VALUES ('22', '0', 'Common\\\\Behavior\\\\CronRunBehavior', 'app_end', 'Common', 'icon-zone', '', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '1', '1', '1');
INSERT INTO `ebcms_behavior` VALUES ('19', '0', 'Behavior\\\\TokenBuildBehavior', 'view_filter', 'Common', 'icon-zone', '', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '1', '1', '1');
INSERT INTO `ebcms_behavior` VALUES ('24', '0', 'Home\\\\Behavior\\\\ClickBehavior', 'app_end', 'Home', 'icon-time', '', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '1', '1', '1');

-- ----------------------------
-- Table structure for `ebcms_conf`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_conf`;
CREATE TABLE `ebcms_conf` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `text` varchar(250) NOT NULL DEFAULT '' COMMENT '标题',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '配置项',
  `value` text COMMENT '配置值',
  `render` varchar(255) NOT NULL DEFAULT '' COMMENT '类型',
  `form` varchar(255) NOT NULL DEFAULT '' COMMENT '表单类型',
  `config` text COMMENT '类型配置',
  `instruction` text COMMENT '说明',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=163 DEFAULT CHARSET=utf8 COMMENT='基本配置表';

-- ----------------------------
-- Records of ebcms_conf
-- ----------------------------
INSERT INTO `ebcms_conf` VALUES ('1', '2', '0', '列出的文件类型', 'fileManagerAllowFiles', '.png\r\n.jpg\r\n.jpeg\r\n.gif\r\n.bmp\r\n.flv\r\n.swf\r\n.mkv\r\n.avi\r\n.rm\r\n.rmvb\r\n.mpeg\r\n.mpg\r\n.ogg\r\n.ogv\r\n.mov\r\n.wmv\r\n.mp4\r\n.webm\r\n.mp3\r\n.wav\r\n.mid\r\n.rar\r\n.zip\r\n.tar\r\n.gz\r\n.7z\r\n.bz2\r\n.cab\r\n.iso\r\n.doc\r\n.docx\r\n.xls\r\n.xlsx\r\n.ppt\r\n.pptx\r\n.pdf\r\n.txt\r\n.md\r\n.xml', 'item', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '', '2016-01-12 10:30:56', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('2', '2', '0', '每次列出文件数量', 'fileManagerListSize', '20', 'number', 'form_numberbox', '{\"min\":1,\"max\":100,\"required\":true}', '', '2015-10-20 16:10:47', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('3', '2', '0', '文件访问路径前缀', 'fileManagerUrlPrefix', '{{__ROOT__.__GROUP__.\'/Uploads\'}}', 'string', 'form_textbox', '\"\"', '', '2016-01-27 22:00:19', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('4', '2', '0', '指定要列出文件的目录', 'fileManagerListPath', '/file/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:11:02', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('5', '2', '0', '执行文件管理的action名称', 'fileManagerActionName', 'listfile', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:11:08', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('6', '2', '0', '列出的文件类型', 'imageManagerAllowFiles', '.png\r\n.jpg\r\n.jpeg\r\n.gif\r\n.bmp', 'item', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '', '2016-01-12 10:31:03', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('7', '2', '0', '插入的图片浮动方式', 'imageManagerInsertAlign', 'none', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:11:22', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('8', '2', '0', '图片访问路径前缀', 'imageManagerUrlPrefix', '{{__ROOT__.__GROUP__.\'/Uploads\'}}', 'string', 'form_textbox', '\"\"', '', '2016-01-27 21:59:10', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('9', '2', '0', '每次列出文件数量', 'imageManagerListSize', '20', 'number', 'form_numberbox', '{\"min\":1,\"max\":100,\"requried\":true}', '', '2015-10-20 16:11:45', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('10', '2', '0', ' 指定要列出图片的目录', 'imageManagerListPath', '/image/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:11:52', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('11', '2', '0', ' 执行图片管理的action名称', 'imageManagerActionName', 'listimage', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:11:58', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('12', '2', '0', '上传文件格式显示', 'fileAllowFiles', '.png\r\n.jpg\r\n.jpeg\r\n.gif\r\n.bmp\r\n.flv\r\n.swf\r\n.mkv\r\n.avi\r\n.rm\r\n.rmvb\r\n.mpeg\r\n.mpg\r\n.ogg\r\n.ogv\r\n.mov\r\n.wmv\r\n.mp4\r\n.webm\r\n.mp3\r\n.wav\r\n.mid\r\n.rar\r\n.zip\r\n.tar\r\n.gz\r\n.7z\r\n.bz2\r\n.cab\r\n.iso\r\n.doc\r\n.docx\r\n.xls\r\n.xlsx\r\n.ppt\r\n.pptx\r\n.pdf\r\n.txt\r\n.md\r\n.xml', 'item', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '允许上传的附件类型', '2016-01-12 10:30:50', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('13', '2', '0', '上传大小限制', 'fileMaxSize', '51200000', 'number', 'form_numberbox', '{\"min\":1,\"max\":51200000,\"requried\":true}', '上传文件时候的文件大小限制', '2015-10-20 16:12:40', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('14', '2', '0', ' 文件访问路径前缀', 'fileUrlPrefix', '{{__ROOT__.__GROUP__.\'/Uploads\'}}', 'string', 'form_textbox', '\"\"', '', '2016-01-27 22:00:11', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('15', '2', '0', '上传保存路径', 'filePathFormat', '/file/{yyyy}{mm}{dd}/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:12:51', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('16', '2', '0', '提交的文件表单名称', 'fileFieldName', 'upfile', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:12:56', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('17', '2', '0', '执行上传视频的action名称', 'fileActionName', 'uploadfile', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:13:00', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('18', '2', '0', '上传视频格式显示', 'videoAllowFiles', '.flv\r\n.swf\r\n.mkv\r\n.avi\r\n.rm\r\n.rmvb\r\n.mpeg\r\n.mpg\r\n.ogg\r\n.ogv\r\n.mov\r\n.wmv\r\n.mp4\r\n.webm\r\n.mp3\r\n.wav\r\n.mid', 'item', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '允许上传的视频格式', '2016-01-12 10:30:45', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('19', '2', '0', '上传大小限制', 'videoMaxSize', '102400000', 'string', 'form_numberbox', '\"\"', '视频上传大小限制', '2015-10-20 16:13:18', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('20', '2', '0', '执行上传视频的action名称', 'videoActionName', 'uploadvideo', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:13:23', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('21', '2', '0', '提交的视频表单名称', 'videoFieldName', 'upfile', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:13:43', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('22', '2', '0', '上传保存路径', 'videoPathFormat', '/video/{yyyy}{mm}{dd}/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:13:47', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('23', '2', '0', '视频访问路径前缀', 'videoUrlPrefix', '', 'string', 'form_textbox', '\"\"', '', '2016-01-27 20:24:26', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('24', '2', '0', '抓取图片格式显示', 'catcherAllowFiles', '.png\r\n.jpg\r\n.jpeg\r\n.gif\r\n.bmp', 'item', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '允许远程抓取的图片类型', '2016-01-12 10:30:39', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('25', '2', '0', '上传大小限制', 'catcherMaxSize', '2048000', 'number', 'form_numberbox', '\"\"', '远程抓取的文件大小限制', '2015-10-20 16:14:04', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('26', '2', '0', '图片访问路径前缀', 'catcherUrlPrefix', '{{__ROOT__.__GROUP__.\'/Uploads\'}}', 'string', 'form_textbox', '\"\"', '', '2016-01-27 22:00:07', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('27', '2', '0', '上传保存路径', 'catcherPathFormat', '/image/{yyyy}{mm}{dd}/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:14:15', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('28', '2', '0', '提交的图片列表表单名称', 'catcherFieldName', 'source', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:14:22', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('29', '2', '0', '执行抓取远程图片的action名称', 'catcherActionName', 'catchimage', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:14:27', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('30', '2', '0', '不必远程抓取的地址', 'catcherLocalDomain', '127.0.0.1\r\nlocalhost\r\nimg.baidu.com', 'item', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '这些域名下面的图片不会被抓取到本地', '2016-01-12 10:30:32', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('31', '2', '0', '插入的图片浮动方式', 'snapscreenInsertAlign', 'none', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:14:44', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('32', '2', '0', '图片访问路径前缀', 'snapscreenUrlPrefix', '{{__ROOT__.__GROUP__.\'/Uploads\'}}', 'string', 'form_textbox', '\"\"', '', '2016-01-27 22:00:02', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('33', '2', '0', '上传保存路径', 'snapscreenPathFormat', '/image/{yyyy}{mm}{dd}/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:14:56', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('34', '2', '0', '执行上传截图的action名称', 'snapscreenActionName', 'uploadimage', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:15:03', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('35', '2', '0', '插入的插图浮动方式', 'scrawlInsertAlign', 'none', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:15:07', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('36', '2', '0', '图片访问路径前缀', 'scrawlUrlPrefix', '{{__ROOT__.__GROUP__.\'/Uploads\'}}', 'string', 'form_textbox', '\"\"', '', '2016-01-27 21:59:58', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('37', '2', '0', '上传大小限制', 'scrawlMaxSize', '2048000', 'number', 'form_numberbox', '\"\"', '涂鸦上传的图片大小限制', '2015-10-20 16:15:19', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('38', '2', '0', '涂鸦上传保存路径', 'scrawlPathFormat', '/image/{yyyy}{mm}{dd}/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:15:26', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('39', '2', '0', '提交的图片表单名称', 'scrawlFieldName', 'upfile', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:15:33', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('40', '2', '0', '执行上传涂鸦的action名称', 'scrawlActionName', 'uploadscrawl', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:15:40', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('41', '2', '0', '图片上传保存路径', 'imagePathFormat', '/image/{yyyy}{mm}{dd}/', 'string', 'form_textbox', '\"\"', '/* {filename} 原文件名,配置这项需要注意中文乱码问题 */\r\n/* {rand:6} 随机数,后面的数字是随机数的位数 */\r\n/* {time} 时间戳 */\r\n/* {yyyy} 四位年份 */\r\n/* {yy} 两位年份 */\r\n/* {mm} 两位月份 */\r\n/* {dd} 两位日期 */\r\n/* {hh} 两位小时 */\r\n/* {ii} 两位分钟 */\r\n/* {ss} 两位秒 */\r\n/* 非法字符 \\ : * ? &quot; &lt; &gt; | */', '2015-10-20 16:15:45', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('42', '2', '0', '是否压缩图片', 'imageCompressEnable', '1', 'bool', 'form_bool', '\"\"', '', '2015-10-20 16:15:51', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('43', '2', '0', '图片压缩最长边限制', 'imageCompressBorder', '1600', 'number', 'form_numberbox', '\"\"', '', '2015-10-20 16:15:58', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('44', '2', '0', '插入的图片浮动方式', 'imageInsertAlign', 'none', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:16:04', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('45', '2', '0', '图片访问路径前缀', 'imageUrlPrefix', '{{__ROOT__.__GROUP__.\'/Uploads\'}}', 'string', 'form_textbox', '\"\"', '', '2016-01-27 21:59:52', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('46', '2', '0', '上传图片格式', 'imageAllowFiles', '.png\r\n.jpg\r\n.jpeg\r\n.gif\r\n.bmp', 'item', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '图片上传功能的图片上传类型限制', '2016-01-12 10:30:23', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('47', '2', '0', '上传大小限制，单位B', 'imageMaxSize', '2048000', 'number', 'form_numberbox', '\"\"', '图片上传功能的图片大小限制', '2015-10-20 16:16:42', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('48', '2', '0', '提交的图片表单名称', 'imageFieldName', 'upfile', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:16:47', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('49', '2', '0', '执行上传图片的action名称', 'imageActionName', 'uploadimage', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:16:52', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_conf` VALUES ('50', '3', '0', '文件夹命名', 'subName', 'date\r\nYmd', 'item', 'form_multitextbox', '{\"required\":\"1\",\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '保存路径的文件夹命名规则', '2016-01-12 10:29:38', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('51', '3', '0', '文件命名规则', 'saveName', 'uniqid\r\n', 'item', 'form_textbox', '{\"required\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\"}', '文件命名规则', '2016-01-12 10:29:46', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('52', '3', '0', '文件大小限制', 'maxSize', '3145728', 'number', 'form_numberbox', '{\"min\":1,\"max\":2048000,\"requried\":true}', '单位B', '2015-10-20 15:58:57', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('53', '3', '0', '保存路径', 'savePath', '/image/', 'string', 'form_textbox', '{\"multiline\":false}', '相对于上传根目录', '2015-10-20 16:04:21', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('54', '3', '0', 'autoSub', 'autoSub', '1', 'bool', 'form_bool', '\"\"', '', '2015-10-20 15:59:33', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('55', '3', '0', '允许的文件类型', 'exts', 'jpg\r\ngif\r\npng\r\njpeg\r\nzip\r\nrar\r\ndoc\r\ndocx\r\ntxt', 'item', 'form_multitextbox', '{\"height\":\"10\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '建议剔除可能对系统安全造成威胁的文件，例如.php .exe等等', '2016-01-12 10:29:29', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('56', '3', '0', '上传的根目录', 'rootPath', '.{{__GROUP__}}/Uploads', 'string', 'form_textbox', '\"\"', '可不填，默认为/Uploads', '2015-11-03 18:56:36', '2015-09-01 10:51:52', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('57', '4', '0', '字体大小', 'fontSize', '40', 'number', 'form_numberbox', '{\"required\":true,\"min\":14,\"max\":40,\"showTip\":true}', '不要设置过大，一般填写验证码图片高度的1/3', '2015-10-20 15:51:01', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('58', '4', '0', '图片高度', 'imageH', '80', 'number', 'form_numberbox', '{\"min\":40,\"max\":120,\"required\":true,\"showTip\":true}', '', '2015-10-20 15:51:05', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('59', '4', '0', '图片宽度', 'imageW', '320', 'number', 'form_numberbox', '{\"min\":200,\"max\":600,\"required\":true,\"showTip\":true}', '', '2015-10-20 15:51:10', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('60', '4', '0', '字符设置', 'codeSet', '89', 'string', 'form_textbox', '{\"required\":true}', '建议剔除容易引起混淆的字符，比如0o之类的', '2015-10-20 15:32:30', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('61', '4', '0', '使用干扰线', 'useCurve', '0', 'bool', 'form_bool', '\"\"', '干扰线能防止机器识别，提高系统安全性，但是会降低用户体验', '2015-10-20 15:33:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('62', '4', '0', '使用噪点', 'useNoise', '0', 'bool', 'form_bool', '\"\"', '噪点能防止机器识别，提高系统安全性，但是会降低用户体验', '2015-10-20 15:33:56', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('63', '4', '0', '字符个数', 'length', '5', 'number', 'form_numberbox', '{\"min\":1,\"max\":10,\"requried\":true,\"showTip\":true}', '', '2015-10-20 15:49:37', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('64', '5', '0', 'nonce_str', 'nonce_str', '{{md5(time())}}', 'string', 'form_textbox', '', '', '2015-09-09 15:16:32', '2015-09-09 15:16:32', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('65', '5', '0', 'mch_id', 'mch_id', '', 'number', 'form_textbox', '', '', '2015-09-24 16:42:06', '2015-09-09 15:16:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('66', '5', '0', 'key', 'key', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:10', '2015-09-09 15:17:05', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('67', '5', '0', 'sslcert_path', 'sslcert_path', '{{COMMON_PATH}}WXPAY/cert/apiclient_cert.pem', 'string', 'form_textbox', '', '', '2015-09-09 15:17:33', '2015-09-09 15:17:33', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('68', '5', '0', 'sslkey_path', 'sslkey_path', '{{COMMON_PATH}}WXPAY/cert/apiclient_key.pem', 'string', 'form_textbox', '', '', '2015-09-09 15:17:49', '2015-09-09 15:17:49', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('69', '5', '0', 'notify_url', 'notify_url', 'http://{{$_SERVER[\'HTTP_HOST\']}}{{__ROOT__}}/notify.php', 'string', 'form_textbox', '', '', '2015-09-09 15:18:05', '2015-09-09 15:18:05', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('70', '5', '0', 'curl_timeout', 'curl_timeout', '30', 'number', 'form_textbox', '', '', '2015-09-09 15:21:38', '2015-09-09 15:18:27', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('71', '5', '0', 'curlop_timeout', 'curlop_timeout', '30', 'number', 'form_textbox', '', '', '2015-09-09 15:21:56', '2015-09-09 15:21:51', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('72', '6', '0', 'AppID', 'AppID', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:16', '2015-09-09 15:22:40', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('73', '6', '0', 'AppSecret', 'AppSecret', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:18', '2015-09-09 15:22:53', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('74', '6', '0', 'Token', 'Token', '', 'string', 'form_textbox', '', '', '2015-09-09 15:23:12', '2015-09-09 15:23:12', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('75', '6', '0', 'URL', 'URL', 'http://{{$_SERVER[\'HTTP_HOST\']}}{{$_SERVER[\'REQUEST_URI\']}}', 'string', 'form_textbox', '', '', '2015-09-09 15:23:27', '2015-09-09 15:23:27', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('76', '6', '0', 'EncodingAESKey', 'EncodingAESKey', '', 'string', 'form_textbox', '', '', '2015-09-09 15:23:38', '2015-09-09 15:23:38', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('77', '6', '0', 'nonceStr', 'nonceStr', '', 'string', 'form_textbox', '', '', '2015-09-09 15:23:48', '2015-09-09 15:23:48', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('78', '7', '0', 'AppId', 'AppId', '', 'string', 'form_textbox', '', '', '2015-09-09 15:24:41', '2015-09-09 15:24:41', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('79', '7', '0', 'ServerIP', 'ServerIP', 'sandboxapp.cloopen.com', 'string', 'form_textbox', '', '', '2015-09-09 15:24:56', '2015-09-09 15:24:56', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('80', '7', '0', 'ServerPort', 'ServerPort', '8883', 'string', 'form_textbox', '', '', '2015-09-09 15:25:10', '2015-09-09 15:25:10', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('81', '7', '0', 'SoftVersion', 'SoftVersion', '2013-12-26', 'string', 'form_textbox', '', '', '2015-09-09 15:25:24', '2015-09-09 15:25:24', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('82', '7', '0', 'AccountToken', 'AccountToken', '', 'string', 'form_textbox', '', '', '2015-09-09 15:25:36', '2015-09-09 15:25:36', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('83', '7', '0', 'AccountSid', 'AccountSid', '', 'string', 'form_textbox', '', '', '2015-09-09 15:25:45', '2015-09-09 15:25:45', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('84', '8', '0', '财付通', 'tenpay', '', 'string', 'form_textbox', '', '', '2015-09-09 15:43:11', '2015-09-09 15:43:11', '95', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('85', '8', '84', 'key', 'key', '', 'string', 'form_textbox', '', '加密key，开通财付通账户后给予', '2015-09-24 16:42:26', '2015-09-09 15:45:10', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('86', '8', '84', 'partner', 'partner', '', 'string', 'form_textbox', '', '合作者ID，财付通有该配置，开通财付通账户后给予', '2015-09-24 16:42:28', '2015-09-09 15:45:23', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('87', '8', '0', '支付宝', 'alipay', '', 'string', 'form_textbox', '', '', '2015-09-09 15:43:32', '2015-09-09 15:43:32', '99', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('88', '8', '87', 'email', 'email', '', 'string', 'form_textbox', '', '收款账号邮箱', '2015-09-24 16:42:32', '2015-09-09 15:45:39', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('89', '8', '87', 'key', 'key', '', 'string', 'form_textbox', '', '加密key，开通支付宝账户后给予', '2015-09-24 16:42:34', '2015-09-09 15:45:50', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('90', '8', '87', 'partner', 'partner', '', 'string', 'form_textbox', '', '合作者ID，支付宝有该配置，开通易宝账户后给予', '2015-09-24 16:42:37', '2015-09-09 15:46:08', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('91', '8', '0', '支付宝移动', 'aliwappay', '', 'string', 'form_textbox', '', '', '2015-09-09 15:44:05', '2015-09-09 15:44:05', '98', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('92', '8', '91', 'email', 'email', '', 'string', 'form_textbox', '', '收款账号邮箱', '2015-09-24 16:42:40', '2015-09-09 15:47:04', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('93', '8', '91', 'key', 'key', '', 'string', 'form_textbox', '', '加密key，开通支付宝账户后给予', '2015-09-24 16:42:42', '2015-09-09 15:47:20', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('94', '8', '91', 'partner', 'partner', '', 'string', 'form_textbox', '', '合作者ID，支付宝有该配置，开通易宝账户后给予', '2015-09-24 16:42:44', '2015-09-09 15:47:38', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('95', '8', '0', 'palpay', 'palpay', '', 'string', 'form_textbox', '', '', '2015-09-09 15:44:26', '2015-09-09 15:44:26', '97', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('96', '8', '95', 'business', 'business', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:47', '2015-09-09 15:47:53', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('97', '8', '0', 'yeepay', 'yeepay', '', 'string', 'form_textbox', '', '', '2015-09-09 15:44:35', '2015-09-09 15:44:35', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('98', '8', '97', 'key', 'key', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:50', '2015-09-09 15:48:08', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('99', '8', '97', 'partner', 'partner', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:54', '2015-09-09 15:48:19', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('100', '8', '0', 'kuaiqian', 'kuaiqian', '', 'string', 'form_textbox', '', '', '2015-09-09 15:44:43', '2015-09-09 15:44:43', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('101', '8', '100', 'key', 'key', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:56', '2015-09-09 15:48:31', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('102', '8', '100', 'partner', 'partner', '', 'string', 'form_textbox', '', '', '2015-09-24 16:42:58', '2015-09-09 15:48:43', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('103', '8', '0', 'unionpay', 'unionpay', '', 'string', 'form_textbox', '', '', '2015-09-09 15:44:51', '2015-09-09 15:44:51', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('104', '8', '103', 'key', 'key', '', 'string', 'form_textbox', '', '', '2015-09-24 16:43:01', '2015-09-09 15:48:57', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('105', '8', '103', 'partner', 'partner', '', 'string', 'form_textbox', '', '', '2015-09-24 16:43:14', '2015-09-09 15:49:07', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('106', '9', '0', '邮箱服务器地址', 'HOST', 'smtp.qq.com', 'string', 'form_textbox', '', '邮箱服务器地址', '2015-09-09 15:52:11', '2015-09-09 15:52:11', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('107', '9', '0', '是否开启SMTP验证功能', 'SMTPAUTH', '1', 'bool', 'form_textbox', '', '', '2015-09-09 15:52:35', '2015-09-09 15:52:35', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('108', '9', '0', '发送人邮箱地址', 'USERNAME', '1540837821@qq.com', 'string', 'form_textbox', '', '', '2015-09-09 15:52:58', '2015-09-09 15:52:58', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('109', '9', '0', '发件人邮箱地址', 'FROM', '1540837821@qq.com', 'string', 'form_textbox', '', '', '2015-09-09 15:53:18', '2015-09-09 15:53:18', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('110', '9', '0', '发送人邮箱密码', 'PASSWORD', '', 'string', 'form_textbox', '', '', '2015-09-09 15:53:41', '2015-09-09 15:53:41', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('111', '9', '0', '发送者名称', 'FROMNAME', 'EBCMS', 'string', 'form_textbox', '', '', '2015-09-09 15:54:03', '2015-09-09 15:54:03', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('112', '9', '0', '是否采用HTML方式发送', 'ISHTML', '1', 'bool', 'form_textbox', '', '', '2015-09-09 15:54:24', '2015-09-09 15:54:24', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('113', '9', '0', '邮件编码', 'CHARSET', '{{C(\'DEFAULT_CHARSET\')}}', 'string', 'form_textbox', '', '', '2015-09-09 15:55:24', '2015-09-09 15:55:24', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('114', '9', '0', '服务器端口', 'PORT', '25', 'number', 'form_textbox', '', '', '2015-09-09 15:55:53', '2015-09-09 15:55:53', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('119', '11', '0', '站点', 'site', '', 'string', 'form_hidden', '', '', '2015-12-10 21:17:49', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('120', '11', '119', '站点名称', 'title', '官方网站', 'string', 'form_textbox', '', '', '2015-09-18 20:58:48', '0000-00-00 00:00:00', '11', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('121', '11', '119', '关键字', 'keywords', 'xx公司', 'string', 'form_textbox', '', '', '2015-09-18 20:58:43', '0000-00-00 00:00:00', '5', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('122', '11', '119', '简介', 'description', 'xx公司是一家。。。。', 'string', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '', '2016-01-12 10:28:33', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('131', '11', '0', '留言板', 'site_guestbook', '', 'string', 'form_hidden', '', '', '2015-12-10 21:17:42', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('132', '11', '131', '标题', 'title', '留言板', 'string', 'form_textbox', '', '', '2015-09-18 20:58:01', '0000-00-00 00:00:00', '4', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('133', '11', '131', '关键字', 'keywords', '关键字', 'string', 'form_textbox', '', '', '2015-09-18 20:58:06', '0000-00-00 00:00:00', '3', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('134', '11', '131', '简介', 'description', 'xx公司的留言中心', 'string', 'form_multitextbox', '{\"height\":\"\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"prompt\":\"\"}', '', '2016-01-12 10:28:28', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('139', '12', '0', 'fontSize', 'fontSize', '30', 'number', 'form_textbox', '{\"min\":14,\"max\":40,\"required\":true}', '', '2015-10-20 11:46:24', '2015-09-18 12:18:43', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('140', '12', '0', 'imageH', 'imageH', '80', 'number', 'form_textbox', '', '', '2015-09-18 12:18:59', '2015-09-18 12:18:59', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('141', '12', '0', 'imageW', 'imageW', '320', 'number', 'form_textbox', '', '', '2015-09-18 12:19:40', '2015-09-18 12:19:18', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('142', '12', '0', 'codeSet', 'codeSet', '89', 'string', 'form_textbox', '', '', '2015-09-18 12:19:34', '2015-09-18 12:19:34', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('143', '12', '0', 'useCurve', 'useCurve', '0', 'bool', 'form_textbox', '', '', '2015-09-18 12:19:56', '2015-09-18 12:19:56', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('144', '12', '0', 'useNoise', 'useNoise', '0', 'bool', 'form_textbox', '', '', '2015-09-18 12:20:10', '2015-09-18 12:20:10', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('145', '12', '0', 'length', 'length', '5', 'number', 'form_textbox', '', '', '2015-09-18 12:20:57', '2015-09-18 12:20:25', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('146', '13', '0', '图片水印', 'img', '', 'string', 'form_textbox', '', '', '2015-09-24 16:07:48', '2015-09-24 15:29:05', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('147', '13', '146', '水印图片地址', 'source', '', 'string', 'form_textbox', '', '', '2015-09-24 16:39:44', '2015-09-24 15:29:46', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('148', '13', '146', '位置', 'locate', '3', 'string', 'form_textbox', '', '1 ; 左上角\r\n2 ; 上居中\r\n3 ; 右上角\r\n4 ; 左居中\r\n5 ; 居中\r\n6 ; 右居中\r\n7 ; 左下角\r\n8 ; 下居中\r\n9 ; 右下角', '2015-09-24 16:39:34', '2015-09-24 15:30:15', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('149', '13', '146', '透明度', 'alpha', '', 'number', 'form_textbox', '', '', '2015-09-24 16:39:20', '2015-09-24 15:32:26', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('150', '13', '0', '文字水印', 'text', '', 'string', 'form_textbox', '', '', '2015-09-24 15:59:22', '2015-09-24 15:34:07', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('151', '13', '150', '文字', 'text', '', 'string', 'form_textbox', '', '', '2015-09-24 15:34:35', '2015-09-24 15:34:35', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('152', '13', '150', '字体路径', 'font', '', 'string', 'form_textbox', '', '', '2015-09-24 16:37:35', '2015-09-24 15:34:51', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('153', '13', '150', '文字大小', 'size', '', 'number', 'form_textbox', '', '', '2015-09-24 16:37:46', '2015-09-24 15:35:20', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('154', '13', '150', '文字颜色', 'color', '', 'string', 'form_textbox', '', '', '2015-09-24 16:37:55', '2015-09-24 15:35:43', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('155', '13', '150', '位置', 'locate', '', 'string', 'form_textbox', '', '1 ; 左上角\r\n2 ; 上居中\r\n3 ; 右上角\r\n4 ; 左居中\r\n5 ; 居中\r\n6 ; 右居中\r\n7 ; 左下角\r\n8 ; 下居中\r\n9 ; 右下角', '2015-09-24 16:38:05', '2015-09-24 15:36:06', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('156', '13', '150', '位置偏移量', 'offset', '', 'number', 'form_textbox', '', '', '2015-09-24 16:38:26', '2015-09-24 16:38:26', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('157', '13', '150', '倾斜度', 'angle', '', 'number', 'form_textbox', '', '', '2015-09-24 16:38:56', '2015-09-24 16:38:56', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('158', '13', '0', '缩略图处理', 'thumb', '', 'string', 'form_textbox', '', '', '2015-09-24 15:59:17', '2015-09-24 15:56:34', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('159', '13', '158', '高', 'height', '120', 'number', 'form_textbox', '', '', '2015-09-24 16:09:10', '2015-09-24 15:33:25', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('160', '13', '158', '宽', 'width', '150', 'number', 'form_textbox', '', '', '2015-09-24 16:09:17', '2015-09-24 15:57:20', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('161', '13', '158', '截取方式', 'type', '3', 'number', 'form_textbox', '', '1 ; 等比例缩放\r\n2 ; 缩放后填充\r\n3 ; 居中裁剪\r\n4 ; 左上角裁剪\r\n5 ; 右下角裁剪\r\n6 ; 固定尺寸缩放', '2015-09-24 16:06:06', '2015-09-24 15:58:55', '1', '1', '0');
INSERT INTO `ebcms_conf` VALUES ('162', '14', '0', '测试字段', 'keys', '123123', 'number', 'form_textbox', '{\"width\":\"\",\"prompt\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"required\":\"1\"}', '23123', '2015-12-09 14:27:02', '2015-12-09 12:03:16', '1', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_confcate`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_confcate`;
CREATE TABLE `ebcms_confcate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `group` varchar(255) NOT NULL DEFAULT 'Common' COMMENT '分组',
  `name` varchar(255) NOT NULL DEFAULT 'Common' COMMENT '生效模块',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `iconcls` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='基本配置分类表';

-- ----------------------------
-- Records of ebcms_confcate
-- ----------------------------
INSERT INTO `ebcms_confcate` VALUES ('2', '常规配置', 'UEDITOR', '百度编辑器', '', 'icon-plugin', '2015-12-28 17:02:13', '2015-07-25 17:04:49', '100', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('3', '常规配置', 'UPLOAD_CONFIG', '上传配置', '', 'icon-file', '2015-12-28 17:02:16', '2015-07-25 17:04:49', '54', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('4', '常规配置', 'VERIFY', '验证码', '', 'icon-nuclear', '2015-12-28 17:02:19', '2015-07-25 17:04:49', '38', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('5', '常规配置', 'WXPAY', '微信支付', '', 'icon-zone', '2015-12-28 17:02:41', '2015-09-09 15:16:12', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('6', '常规配置', 'WECHAT', '微信公众号配置', '', 'icon-zone', '2015-12-28 17:02:39', '2015-09-09 15:22:24', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('7', '常规配置', 'YTX', '云通讯', '', 'icon-zone', '2015-12-28 17:02:35', '2015-09-09 15:24:26', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('8', '常规配置', 'PAYMENT', '支付配置', '', 'icon-zone', '2015-12-28 17:02:33', '2015-09-09 15:42:44', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('9', '常规配置', 'EMAIL', '邮箱配置', '', 'icon-zone', '2015-12-28 17:02:30', '2015-09-09 15:50:03', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('11', '常规配置', 'SEO', 'SEO配置', '', 'icon-report', '2015-12-28 17:02:28', '2015-08-07 09:12:07', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('12', '常规配置', 'HOME_VERIFY', '验证码', '', 'icon-wait', '2015-12-28 17:02:25', '2015-09-18 12:17:22', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('13', '常规配置', 'THUMB', '图片处理', '', 'icon-zone', '2015-12-28 17:02:22', '2015-09-24 15:27:32', '1', '1', '0');
INSERT INTO `ebcms_confcate` VALUES ('15', '定时任务', 'cron_clearbackuplockfile', '清理备份文件', '', 'icon-world', '2015-12-09 11:32:55', '2015-12-09 11:32:55', '1', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_config`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_config`;
CREATE TABLE `ebcms_config` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `text` varchar(250) NOT NULL DEFAULT '' COMMENT '标题',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '配置项',
  `value` text COMMENT '配置值',
  `render` varchar(255) NOT NULL DEFAULT '' COMMENT '类型',
  `form` varchar(255) NOT NULL DEFAULT '' COMMENT '表单类型',
  `config` text COMMENT '类型配置',
  `instruction` text COMMENT '说明',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COMMENT='核心配置表';

-- ----------------------------
-- Records of ebcms_config
-- ----------------------------
INSERT INTO `ebcms_config` VALUES ('1', '2', '0', '模块获取变量', 'VAR_MODULE', 'm', 'string', 'form_textbox', '', '', '2015-10-20 09:57:10', '2015-08-31 21:54:04', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('2', '2', '0', '控制器获取变量', 'VAR_CONTROLLER', 'c', 'string', 'form_textbox', '', '', '2015-08-31 21:54:23', '2015-08-31 21:54:23', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('3', '2', '0', '操作获取变量', 'VAR_ACTION', 'a', 'string', 'form_textbox', '', '', '2015-08-31 21:54:38', '2015-08-31 21:54:38', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('4', '2', '0', 'AJAX提交变量', 'VAR_AJAX_SUBMIT', 'ajax', 'string', 'form_textbox', '', '', '2015-08-31 21:54:56', '2015-08-31 21:54:56', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('5', '2', '0', 'VAR_JSONP_HANDLER', 'VAR_JSONP_HANDLER', 'callback', 'string', 'form_textbox', '', '', '2015-08-31 21:55:11', '2015-08-31 21:55:11', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('6', '2', '0', 'PATHINFO获取变量', 'VAR_PATHINFO', 's', 'string', 'form_textbox', '', '兼容模式PATHINFO获取变量例如 ?s=/module/action/id/1 后面的参数取决于URL_PATHINFO_DEPR', '2015-08-31 21:55:47', '2015-08-31 21:55:41', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('7', '2', '0', '模板切换变量', 'VAR_TEMPLATE', 't', 'string', 'form_textbox', '', '', '2015-08-31 21:56:06', '2015-08-31 21:56:06', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('8', '2', '0', '插件控制器命名空间变量', 'VAR_ADDON', 'addon', 'string', 'form_textbox', '', '默认的插件控制器命名空间变量 3.2.2新增', '2015-08-31 21:56:36', '2015-08-31 21:56:36', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('9', '3', '0', '应用类库是否使用命名空间', 'APP_USE_NAMESPACE', '1', 'bool', 'form_bool', '', '应用类库是否使用命名空间 3.2.1新增', '2015-08-31 21:04:44', '2015-08-31 20:59:18', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('10', '3', '0', '是否开启子域名部署', 'APP_SUB_DOMAIN_DEPLOY', '0', 'item', 'form_textbox', '{\"multiline\":true,\"height\":120}', '', '2015-08-31 21:04:48', '2015-08-31 21:00:14', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('11', '3', '0', '子域名部署规则', 'APP_SUB_DOMAIN_RULES', '', 'item', 'form_textbox', '{\"multiline\":true,\"height\":120}', '', '2015-08-31 21:04:52', '2015-08-31 21:00:46', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('12', '3', '0', '域名后缀', 'APP_DOMAIN_SUFFIX', '', 'string', 'form_textbox', '', '如果是com.cn net.cn 之类的后缀必须设置', '2015-08-31 21:04:55', '2015-08-31 21:01:40', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('13', '3', '0', '操作方法后缀', 'ACTION_SUFFIX', '', 'string', 'form_textbox', '', '', '2015-08-31 21:05:00', '2015-08-31 21:03:45', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('14', '3', '0', '是否允许多模块', 'MULTI_MODULE', '1', 'bool', 'form_bool', '', '如果为false 则必须设置 DEFAULT_MODULE', '2015-08-31 21:05:04', '2015-08-31 21:04:11', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('15', '3', '0', '禁止访问的模块列表', 'MODULE_DENY_LIST', 'Common\r\nRuntime', 'item', 'form_textbox', '{\"multiline\":true,\"height\":120}', '', '2015-08-31 21:05:15', '2015-08-31 21:04:38', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('16', '3', '0', '允许访问的模块列表', 'MODULE_ALLOW_LIST', '', 'item', 'form_textbox', '{\"multiline\":true,\"height\":120}', '', '2015-08-31 21:05:45', '2015-08-31 21:05:38', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('17', '3', '0', '控制器层级', 'CONTROLLER_LEVEL', '1', 'number', 'form_numberbox', '', '', '2015-08-31 21:06:15', '2015-08-31 21:06:15', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('18', '3', '0', '自动加载类库层', 'APP_AUTOLOAD_LAYER', 'Controller,Model', 'string', 'form_textbox', '', '（针对非命名空间定义类库） 3.2.1新增', '2015-08-31 21:07:46', '2015-08-31 21:06:59', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('19', '3', '0', '自动加载的路径', 'APP_AUTOLOAD_PATH', '', 'string', 'form_textbox', '{\"width\":\"\",\"prompt\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"required\":\"0\"}', '（针对非命名空间定义类库） 3.2.1新增', '2015-12-09 11:08:29', '2015-08-31 21:07:29', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('20', '4', '0', '默认的模型层名称', 'DEFAULT_M_LAYER', 'Model', 'string', 'form_textbox', '', '', '2015-08-31 21:08:38', '2015-08-31 21:08:38', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('21', '4', '0', '默认的控制器层名称', 'DEFAULT_C_LAYER', 'Controller', 'string', 'form_textbox', '', '', '2015-08-31 21:08:57', '2015-08-31 21:08:57', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('22', '4', '0', '默认的视图层名称', 'DEFAULT_V_LAYER', 'View', 'string', 'form_textbox', '', '', '2015-08-31 21:09:17', '2015-08-31 21:09:17', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('23', '4', '0', '默认语言', 'DEFAULT_LANG', 'zh-cn', 'string', 'form_textbox', '', '', '2015-08-31 21:09:37', '2015-08-31 21:09:37', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('24', '4', '0', '默认模板主题名称', 'DEFAULT_THEME', '', 'string', 'form_textbox', '', '', '2015-08-31 21:09:54', '2015-08-31 21:09:54', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('25', '4', '0', '默认模块', 'DEFAULT_MODULE', 'Home', 'string', 'form_textbox', '', '', '2015-08-31 21:10:15', '2015-08-31 21:10:15', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('26', '4', '0', '默认控制器名称', 'DEFAULT_CONTROLLER', 'Index', 'string', 'form_textbox', '', '', '2015-08-31 21:10:36', '2015-08-31 21:10:36', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('27', '4', '0', '默认操作名称', 'DEFAULT_ACTION', 'index', 'string', 'form_textbox', '', '', '2015-08-31 21:10:51', '2015-08-31 21:10:51', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('28', '4', '0', '默认输出编码', 'DEFAULT_CHARSET', 'utf-8', 'string', 'form_textbox', '', '', '2015-08-31 21:11:08', '2015-08-31 21:11:08', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('29', '4', '0', '默认时区', 'DEFAULT_TIMEZONE', 'PRC', 'string', 'form_textbox', '', '', '2015-08-31 21:11:28', '2015-08-31 21:11:23', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('30', '4', '0', '默认AJAX 数据返回格式', 'DEFAULT_AJAX_RETURN', 'JSON', 'string', 'form_textbox', '', '可选JSON XML ...', '2015-08-31 21:11:59', '2015-08-31 21:11:48', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('31', '4', '0', '默认JSONP格式返回的处理方法', 'DEFAULT_JSONP_HANDLER', 'jsonpReturn', 'string', 'form_textbox', '', '', '2015-08-31 21:12:17', '2015-08-31 21:12:17', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('32', '4', '0', '默认参数过滤方法', 'DEFAULT_FILTER', 'htmlspecialchars', 'string', 'form_textbox', '{\"width\":\"\",\"prompt\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"required\":\"0\"}', '用于I函数...', '2015-12-09 11:11:52', '2015-08-31 21:12:41', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('33', '5', '0', '404 跳转页面', 'URL_404_REDIRECT', '', 'string', 'form_textbox', '', '部署模式有效', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('34', '5', '0', 'URL映射定义规则', 'URL_MAP_RULES', '[]', 'json', 'form_textbox', '{\"multiline\":true,\"height\":120}', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('35', '5', '0', '路由规则', 'URL_ROUTE_RULES', '[]', 'json', 'form_textbox', '{\"multiline\":true,\"height\":120}', '默认路由规则 针对模块', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('36', '5', '0', '是否开启URL路由', 'URL_ROUTER_ON', '0', 'bool', 'form_bool', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('37', '5', '0', 'URL访问模式', 'URL_MODEL', '0', 'number', 'form_numberbox', '', '可选参数0、1、2、3,代表以下四种模式：\r\n// 0 (普通模式); 1 (PATHINFO 模式); 2 (REWRITE  模式); 3 (兼容模式)  默认为PATHINFO 模式', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('38', '5', '0', '伪静态后缀', 'URL_HTML_SUFFIX', 'html', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('39', '5', '0', 'PATHINFO分隔符', 'URL_PATHINFO_DEPR', '/', 'string', 'form_textbox', '', 'PATHINFO模式下，各参数之间的分割符号', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('40', '5', '0', 'URL禁止访问的后缀', 'URL_DENY_SUFFIX', 'ico|png|gif|jpg|js|css', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('41', '5', '0', '是否区分大小写', 'URL_CASE_INSENSITIVE', '0', 'bool', 'form_bool', '', '默认false 表示URL区分大小写 true则表示不区分大小写', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('42', '5', '0', 'URL_PATHINFO_FETCH', 'URL_PATHINFO_FETCH', 'ORIG_PATH_INFO,REDIRECT_PATH_INFO,REDIRECT_URL', 'string', 'form_textbox', '', '用于兼容判断PATH_INFO 参数的SERVER替代变量列表', '2015-08-31 21:49:52', '2015-08-31 21:49:52', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('43', '5', '0', 'URL_REQUEST_URI', 'URL_REQUEST_URI', 'REQUEST_URI', 'string', 'form_textbox', '', '获取当前页面地址的系统变量 默认为REQUEST_URI', '2015-08-31 21:50:31', '2015-08-31 21:50:25', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('44', '5', '0', '绑定到参数', 'URL_PARAMS_BIND', '1', 'bool', 'form_bool', '', 'URL变量绑定到Action方法参数', '2015-08-31 21:51:33', '2015-08-31 21:51:33', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('45', '5', '0', '变量绑定类型', 'URL_PARAMS_BIND_TYPE', '0', 'number', 'form_numberbox', '', '0 按变量名绑定 1 按变量顺序绑定', '2015-08-31 21:52:24', '2015-08-31 21:52:24', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('46', '6', '0', '错误跳转对应的模板文件', 'TMPL_ACTION_ERROR', '{{THINK_PATH}}Tpl/dispatch_jump.tpl', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('47', '6', '0', '是否去除模板文件里面的html空格与换行', 'TMPL_STRIP_SPACE', '1', 'bool', 'form_bool', '', '', '2015-08-31 21:44:54', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('48', '6', '0', '模板引擎', 'TMPL_ENGINE_TYPE', 'Think', 'string', 'form_textbox', '', '', '2015-08-11 17:22:34', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('49', '6', '0', '模板文件分割符', 'TMPL_FILE_DEPR', '/', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('50', '6', '0', '模板文件后缀', 'TMPL_TEMPLATE_SUFFIX', '.html', 'string', 'form_textbox', '', '', '2015-08-13 20:47:10', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('51', '6', '0', '模板输出类型', 'TMPL_CONTENT_TYPE', 'text/html', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('52', '6', '0', '异常页面的模板文件', 'TMPL_EXCEPTION_FILE', '{{THINK_PATH}}Tpl/think_exception.tpl', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('53', '6', '0', '成功跳转对应的模板文件', 'TMPL_ACTION_SUCCESS', '{{THINK_PATH}}Tpl/dispatch_jump.tpl', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('54', '6', '0', '自动侦测模板主题', 'TMPL_DETECT_THEME', '0', 'bool', 'form_bool', '', '', '2015-08-31 21:39:42', '2015-08-31 21:39:33', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('55', '6', '0', '模板缓存后缀', 'TMPL_CACHFILE_SUFFIX', '.php', 'string', 'form_textbox', '', '', '2015-08-31 21:40:32', '2015-08-31 21:40:32', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('56', '6', '0', '模板引擎禁用函数', 'TMPL_DENY_FUNC_LIST', 'echo,exit', 'string', 'form_textbox', '', '', '2015-08-31 21:40:58', '2015-08-31 21:40:58', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('57', '6', '0', '禁用PHP原生代码', 'TMPL_DENY_PHP', '0', 'bool', 'form_bool', '', '', '2015-08-31 21:41:36', '2015-08-31 21:41:36', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('58', '6', '0', '开始标记', 'TMPL_L_DELIM', '{', 'string', 'form_textbox', '', '模板引擎普通标签开始标记', '2015-08-31 21:42:08', '2015-08-31 21:42:08', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('59', '6', '0', '结束标记', 'TMPL_R_DELIM', '}', 'string', 'form_textbox', '', '模板引擎普通标签结束标记', '2015-08-31 21:42:31', '2015-08-31 21:42:31', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('60', '6', '0', '模板变量识别', 'TMPL_VAR_IDENTIFY', 'array', 'string', 'form_textbox', '', '留空自动判断,参数为\'obj\'则表示对象', '2015-08-31 21:43:17', '2015-08-31 21:43:17', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('61', '6', '0', '模板编译缓存', 'TMPL_CACHE_ON', '1', 'bool', 'form_bool', '', '是否开启模板编译缓存,设为false则每次都会重新编译', '2015-08-31 21:44:34', '2015-08-31 21:44:28', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('62', '6', '0', '模板缓存前缀标识', 'TMPL_CACHE_PREFIX', '', 'string', 'form_textbox', '', '模板缓存前缀标识，可以动态改变', '2015-08-31 21:45:55', '2015-08-31 21:45:55', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('63', '6', '0', '模板缓存有效期', 'TMPL_CACHE_TIME', '0', 'number', 'form_numberbox', '', ' 0 为永久，(以数字为值，单位:秒)', '2015-08-31 21:46:43', '2015-08-31 21:46:43', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('64', '6', '0', '布局标识符', 'TMPL_LAYOUT_ITEM', '{__CONTENT__}', 'string', 'form_textbox', '', '布局模板的内容替换标识', '2015-08-31 21:47:34', '2015-08-31 21:47:28', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('65', '6', '0', '是否启用布局', 'LAYOUT_ON', '0', 'bool', 'form_bool', '', '', '2015-08-31 21:48:07', '2015-08-31 21:48:07', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('66', '6', '0', '当前布局名称', 'LAYOUT_NAME', 'layout', 'string', 'form_textbox', '', '默认为layout', '2015-08-31 21:48:29', '2015-08-31 21:48:29', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('67', '7', '0', '错误显示信息', 'ERROR_MESSAGE', '页面错误！请稍后再试～', 'string', 'form_textbox', '', '非调试模式有效', '2015-08-31 21:31:06', '2015-08-31 21:31:06', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('68', '7', '0', '错误定向页面', 'ERROR_PAGE', '', 'string', 'form_textbox', '', '', '2015-08-31 21:31:25', '2015-08-31 21:31:25', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('69', '7', '0', '显示错误信息', 'SHOW_ERROR_MSG', '0', 'bool', 'form_bool', '', '', '2015-09-03 15:16:35', '2015-08-31 21:31:49', '1', '0', '1');
INSERT INTO `ebcms_config` VALUES ('70', '7', '0', '记录数', 'TRACE_MAX_RECORD', '100', 'number', 'form_numberbox', '', '每个级别的错误信息 最大记录数', '2015-12-22 21:44:15', '2015-08-31 21:32:43', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('71', '8', '0', '是否记录', 'LOG_RECORD', '0', 'bool', 'form_bool', '', '默认不记录日志', '2015-08-31 21:33:40', '2015-08-31 21:33:40', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('72', '8', '0', '日志记录类型', 'LOG_TYPE', 'File', 'string', 'form_textbox', '', '默认为文件方式', '2015-08-31 21:34:27', '2015-08-31 21:34:27', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('73', '8', '0', '记录级别', 'LOG_LEVEL', 'EMERG,ALERT,CRIT,ERR', 'string', 'form_textbox', '', '允许记录的日志级别', '2015-08-31 21:34:59', '2015-08-31 21:34:59', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('74', '8', '0', '记录异常日志', 'LOG_EXCEPTION_RECORD', '0', 'bool', 'form_bool', '', '是否记录异常信息日志', '2015-08-31 21:35:39', '2015-08-31 21:35:39', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('75', '9', '0', '是否校验缓存', 'DATA_CACHE_CHECK', '0', 'bool', 'form_bool', '', '', '2015-08-31 21:27:13', '2015-08-31 21:27:13', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('76', '9', '0', '是否压缩缓存', 'DATA_CACHE_COMPRESS', '0', 'bool', 'form_bool', '', '', '2015-08-31 21:26:55', '2015-08-31 21:26:55', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('77', '9', '0', '缓存有效期', 'DATA_CACHE_TIME', '0', 'number', 'form_numberbox', '', '0表示永久缓存', '2015-08-31 21:26:26', '2015-08-31 21:26:26', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('78', '9', '0', '缓存前缀', 'DATA_CACHE_PREFIX', '', 'string', 'form_textbox', '', '', '2015-08-31 21:27:28', '2015-08-31 21:27:28', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('79', '9', '0', '缓存类型', 'DATA_CACHE_TYPE', 'File', 'string', 'form_textbox', '', '支持:File|Db|Apc|Memcache|Shmop|Sqlite|Xcache|Apachenote|Eaccelerator等，前提需要您的服务器支持', '2015-08-31 21:28:23', '2015-08-31 21:28:23', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('80', '9', '0', '缓存路径', 'DATA_CACHE_PATH', '{{TEMP_PATH}}', 'string', 'form_textbox', '', '(仅对File方式缓存有效)', '2015-08-31 21:28:59', '2015-08-31 21:28:59', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('81', '9', '0', '使用子目录缓存', 'DATA_CACHE_SUBDIR', '0', 'bool', 'form_bool', '', '(自动根据缓存标识的哈希创建子目录)', '2015-08-31 21:29:30', '2015-08-31 21:29:30', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('82', '9', '0', '子目录缓存级别', 'DATA_PATH_LEVEL', '1', 'number', 'form_numberbox', '', '', '2015-08-31 21:29:52', '2015-08-31 21:29:52', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('83', '10', '0', '自动开启', 'SESSION_AUTO_START', '1', 'bool', 'form_bool', '\"\"', '是否自动开启Session', '2015-10-20 16:54:18', '2015-08-31 21:36:53', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('84', '10', '0', '配置数组', 'SESSION_OPTIONS', '', 'string', 'form_textbox', '', '支持type name id path expire domain 等参数', '2015-08-31 21:37:45', '2015-08-31 21:37:39', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('85', '10', '0', 'hander类型', 'SESSION_TYPE', '', 'string', 'form_textbox', '', '默认无需设置 除非扩展了session hander驱动', '2015-09-21 17:14:02', '2015-08-31 21:38:11', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('86', '10', '0', 'session 前缀', 'SESSION_PREFIX', '{{substr(__ROOT__,1)}}_', 'string', 'form_textbox', '{\"required\":\"0\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '2015-12-10 21:10:28', '2015-08-31 21:38:30', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('87', '11', '0', 'Cookie有效期', 'COOKIE_EXPIRE', '0', 'number', 'form_numberbox', '', '', '2015-08-31 21:13:30', '2015-08-31 21:13:30', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('88', '11', '0', 'Cookie有效域名', 'COOKIE_DOMAIN', '', 'string', 'form_textbox', '', '', '2015-08-31 21:13:51', '2015-08-31 21:13:46', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('89', '11', '0', 'Cookie路径', 'COOKIE_PATH', '{{__ROOT__}}/', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:51:23', '2015-08-31 21:14:11', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('90', '11', '0', 'Cookie前缀', 'COOKIE_PREFIX', '{{substr(__ROOT__,1)}}_', 'string', 'form_textbox', '{\"required\":\"0\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '避免冲突', '2015-12-10 21:10:42', '2015-08-31 21:14:32', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('91', '11', '0', 'COOKIE_HTTPONLY', 'COOKIE_HTTPONLY', '', 'string', 'form_textbox', '', 'Cookie的httponly属性 3.2.2新增', '2015-08-31 21:14:55', '2015-08-31 21:14:55', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('92', '12', '0', '服务器地址', 'DB_HOST', '127.0.0.1', 'string', 'form_textbox', '{\"required\":true}', '若发现数据库执行慢，可尝试切换数据库地址\r\nlocalhost\r\n127.0.0.1', '2015-10-20 16:44:04', '2015-07-25 17:04:49', '99', '1', '0');
INSERT INTO `ebcms_config` VALUES ('93', '12', '0', '端口', 'DB_PORT', '80', 'number', 'form_numberbox', '\"\"', 'mysql的默认端口是3306，可不填', '2015-10-20 16:45:01', '2015-07-25 17:04:49', '97', '0', '0');
INSERT INTO `ebcms_config` VALUES ('94', '12', '0', '数据库类型', 'DB_TYPE', 'mysql', 'string', 'form_radio', '{\"values\":\"Mysql|mysql\"}', '', '2015-12-10 20:39:59', '2015-07-25 17:04:49', '95', '1', '0');
INSERT INTO `ebcms_config` VALUES ('95', '12', '0', '用户名', 'DB_USER', 'root', 'string', 'form_textbox', '\"\"', '不建议使用超级账户', '2015-10-20 16:49:38', '2015-07-25 17:04:49', '71', '1', '0');
INSERT INTO `ebcms_config` VALUES ('96', '12', '0', '密码', 'DB_PWD', 'root', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:49:48', '2015-07-25 17:04:49', '60', '1', '0');
INSERT INTO `ebcms_config` VALUES ('97', '12', '0', '数据库名', 'DB_NAME', 'abc1', 'string', 'form_textbox', '\"\"', '', '2015-10-20 16:49:54', '2015-07-25 17:04:49', '59', '1', '0');
INSERT INTO `ebcms_config` VALUES ('98', '12', '0', '数据库表前缀', 'DB_PREFIX', 'ebcms_', 'string', 'form_textbox', '\"\"', '若非必要，此项请不要修改\r\n修改后需要手动对数据库表前缀重命名', '2015-10-20 16:49:58', '2015-07-25 17:04:49', '41', '1', '0');
INSERT INTO `ebcms_config` VALUES ('99', '12', '0', '字段缓存', 'DB_FIELDS_CACHE', '', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('100', '12', '0', '其他参数', 'DB_PARAMS', '{&quot;8&quot;:0}', 'json', 'form_textbox', '{\"multiline\":true,\"height\":120}', '', '2015-10-20 16:50:27', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_config` VALUES ('101', '12', '0', '数据库调试', 'DB_DEBUG', '1', 'bool', 'form_bool', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('102', '12', '0', 'Lite模式', 'DB_LITE', '0', 'bool', 'form_bool', '', '3.2.3新增 ', '2015-08-31 21:21:39', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('103', '12', '0', '数据库字符集', 'DB_CHARSET', 'utf8', 'string', 'form_textbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('104', '12', '0', '数据库部署方式', 'DB_DEPLOY_TYPE', '0', 'number', 'form_numberbox', '', '0 集中式(单一服务器),1 分布式(主从服务器)', '2015-08-31 21:17:45', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('105', '12', '0', '数据库写入数据自动参数绑定', 'DB_BIND_PARAM', '0', 'number', 'form_numberbox', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('106', '12', '0', '读写分离', 'DB_RW_SEPARATE', '0', 'bool', 'form_bool', '', '主从式有效', '2015-08-31 21:18:29', '2015-08-31 21:18:20', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('107', '12', '0', '主服务器数量', 'DB_MASTER_NUM', '1', 'number', 'form_numberbox', '', '主从式有效', '2015-08-31 21:19:12', '2015-08-31 21:19:01', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('108', '12', '0', '从服务器序号', 'DB_SLAVE_NO', '', 'number', 'form_numberbox', '', '主从式有效', '2015-08-31 21:19:45', '2015-08-31 21:19:45', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('119', '22', '0', '超级管理员用户名', 'SUPERADMIN', 'admin', 'string', 'form_textbox', '\"\"', '默认为admin，即用户名为admin的账号为超级账号', '2015-12-02 14:37:37', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('126', '15', '0', '是否开启表单令牌', 'TOKEN_ON', '1', 'bool', 'form_bool', '', '注意，开启表单令牌之前，请务必在配置目录下的tags.php中添加\r\n\'view_filter\' =&gt; array(\'Behavior\\\\TokenBuildBehavior\'),', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('127', '15', '0', '令牌表单', 'TOKEN_NAME', '__hash__', 'string', 'form_textbox', '', '该字段请不要用常规的表单域，一般请使用双下划线避免重复', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('128', '15', '0', '令牌类型', 'TOKEN_TYPE', 'md5', 'string', 'form_textbox', '', '令牌域值的生成算法', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('129', '15', '0', '开启安全重置', 'TOKEN_RESET', '0', 'bool', 'form_bool', '', '表单令牌是否', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('130', '16', '0', '备份路径', 'DATA_BACKUP_PATH', '.{{__GROUP__}}/Backup', 'string', 'form_textbox', '\"\"', '为了系统安全，建议您将备份目录设置在网站目录之外，例如：../Backup', '2015-11-03 18:55:22', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('131', '16', '0', '卷大小', 'DATA_BACKUP_PART_SIZE', '2048000', 'number', 'form_numberbox', '', '单位B', '2015-08-30 17:15:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('132', '16', '0', '是否启用ZIP压缩', 'DATA_BACKUP_COMPRESS', '0', 'bool', 'form_bool', '', '不建议开启', '2015-10-15 15:29:25', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('133', '16', '0', '压缩等级', 'DATA_BACKUP_COMPRESS_LEVEL', '4', 'number', 'form_numberbox', '', '', '2015-08-30 17:14:46', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('134', '17', '0', 'COOKIE有效期', 'COOKIE_EXPIRE', '', 'number', 'form_numberbox', '', '', '2015-09-03 18:20:44', '2015-09-03 18:20:44', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('135', '17', '0', '有效域名', 'COOKIE_DOMAIN', '', 'string', 'form_textbox', '', '', '2015-09-03 18:21:02', '2015-09-03 18:21:02', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('136', '17', '0', 'COOKIE前缀', 'COOKIE_PREFIX', '{{substr(__ROOT__,1)}}_', 'string', 'form_textbox', '\"\"', '', '2015-10-30 16:45:39', '2015-09-03 18:21:30', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('137', '17', '0', 'COOKIE_HTTPONLY', 'COOKIE_HTTPONLY', '', 'bool', 'form_bool', '', '', '2015-09-03 18:23:34', '2015-09-03 18:21:42', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('138', '17', '0', 'COOKIE_PATH', 'COOKIE_PATH', '{{__ROOT__}}/', 'string', 'form_textbox', '', '', '2015-09-24 15:10:30', '2015-09-24 15:04:01', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('139', '18', '0', 'SESSION类型', 'SESSION_TYPE', '', 'string', 'form_textbox', '', 'session处理方式 默认无需设置，若开启后台账号单点登录，可填写Ebcms', '2015-09-01 10:36:30', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('140', '19', '0', '模板替换配置', 'TMPL_PARSE_STRING', '', 'string', 'form_textbox', '\"\"', '', '2015-11-03 17:01:57', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('141', '19', '140', 'THIRD', '__THIRD__', './Public/Third', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:47:19', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('142', '19', '140', 'CSS', '__CSS__', './Public/Css', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:47:15', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('143', '19', '140', 'JS', '__JS__', './Public/Js', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:47:11', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('144', '19', '140', 'IMAGE', '__IMAGE__', './Public/Image', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:47:06', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('145', '19', '0', '是否显示调试信息', 'SHOW_PAGE_TRACE', '1', 'bool', 'form_bool', '', '', '2015-09-03 15:24:16', '2015-07-25 17:04:49', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('148', '19', '0', '模板管理中可以编辑的文件类型', 'EDITFILE_EXT', 'html\r\ncss\r\njs\r\ntxt', 'item', 'form_multitextbox', '{\"multiline\":true,\"height\":120}', '', '2015-12-10 21:11:57', '2015-08-30 17:17:46', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('149', '19', '0', 'URL模式', 'URL_MODEL', '0', 'bool', 'form_bool', '', '', '2015-09-03 17:32:00', '2015-09-03 17:32:00', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('150', '21', '0', '关闭网站', 'SITE_CLOSED', '0', 'bool', 'form_bool', '', '仅仅关闭前台', '2015-10-15 14:29:42', '2015-09-03 17:33:57', '99', '1', '0');
INSERT INTO `ebcms_config` VALUES ('151', '21', '0', '关闭原因', 'SITE_CLOSED_REASON', '网站维护中...', 'string', 'form_multitextbox', '', '', '2015-12-10 21:13:11', '2015-09-03 17:35:37', '98', '1', '0');
INSERT INTO `ebcms_config` VALUES ('152', '21', '0', '显示调试信息', 'SHOW_PAGE_TRACE', '0', 'bool', 'form_bool', '', '网站上线后建议关闭', '2015-09-03 18:50:25', '2015-08-07 09:12:26', '93', '1', '0');
INSERT INTO `ebcms_config` VALUES ('153', '21', '0', '模版主题', 'DEFAULT_THEME', 'default', 'string', 'form_textbox', '', '更改模板主题名称可以防止别人下载模板文件，\r\n更改后，还要到网站里面修改对应的文件夹名称', '2015-09-03 18:50:35', '2015-08-07 09:12:41', '90', '1', '0');
INSERT INTO `ebcms_config` VALUES ('154', '21', '0', '模板替换', 'TMPL_PARSE_STRING', '', 'string', 'form_hidden', '', '', '2015-12-10 21:13:43', '2015-09-02 15:11:12', '82', '1', '1');
INSERT INTO `ebcms_config` VALUES ('155', '21', '154', '第三方组件', '__THIRD__', './Public/Third', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:48:33', '2015-09-02 15:11:35', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('156', '21', '154', 'CSS样式表', '__CSS__', './Public/Css', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:48:28', '2015-09-02 15:11:53', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('157', '21', '154', 'JS资源地址', '__JS__', './Public/Js', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:48:25', '2015-09-02 15:12:17', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('158', '21', '154', '资源图片地址', '__IMAGE__', './Public/Image', 'string', 'form_textbox', '\"\"', '', '2016-01-27 17:48:19', '2015-09-02 15:12:37', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('159', '21', '0', 'SESSION_TYPE', 'SESSION_TYPE', '', 'string', 'form_textbox', '', '', '2015-08-07 09:13:20', '2015-08-07 09:13:20', '1', '1', '1');
INSERT INTO `ebcms_config` VALUES ('160', '21', '0', 'USER_AUTH_KEY', 'USER_AUTH_KEY', 'adf', 'string', 'form_textbox', '', '', '2015-08-07 09:13:11', '2015-08-07 09:13:11', '1', '1', '1');
INSERT INTO `ebcms_config` VALUES ('161', '21', '0', 'TAGLIB_BUILD_IN', 'TAGLIB_BUILD_IN', 'Cx,Home\\Taglib\\Qy', 'string', 'form_textbox', '', '', '2015-08-11 17:47:10', '2015-08-07 09:13:00', '1', '1', '1');
INSERT INTO `ebcms_config` VALUES ('162', '21', '0', '是否开启留言验证码', 'GUESTBOOK_VERIFY', '1', 'bool', 'form_bool', '', '', '2015-09-18 12:14:50', '2015-09-18 12:08:41', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('163', '21', '0', '是否记录点击量', 'RECORD_CLICK', '1', 'bool', 'form_bool', '', '', '2015-09-18 12:09:48', '2015-09-18 12:09:48', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('164', '21', '0', '留言分页数', 'GUESTBOOK_PAGENUM', '20', 'number', 'form_numberbox', '', '留言板的分页条数，默认20条', '2015-09-21 17:16:00', '2015-09-21 17:16:00', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('165', '19', '0', 'TAGLIB_BUILD_IN', 'TAGLIB_BUILD_IN', 'Cx,Common\\Taglib\\Core', 'string', 'form_textbox', '\"\"', '', '2015-11-16 13:04:47', '2015-11-16 13:04:26', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('166', '22', '0', '权限认证配置', 'AUTH_CONFIG', '', 'string', 'form_textbox', '\"\"', '', '2015-12-02 10:57:02', '2015-12-02 10:57:02', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('167', '22', '166', '认证开关', 'AUTH_ON', '1', 'bool', 'form_bool', '\"\"', '', '2015-12-02 10:57:32', '2015-12-02 10:57:32', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('168', '22', '166', '认证模式', 'AUTH_TYPE', '2', 'number', 'form_combotree', '\"\"', '', '2015-12-09 15:51:53', '2015-12-02 10:59:01', '1', '1', '0');
INSERT INTO `ebcms_config` VALUES ('169', '22', '166', '分组表', 'AUTH_GROUP', '{{C(&quot;DB_PREFIX&quot;)}}auth_group', 'string', 'form_textbox', '\"\"', '', '2016-01-10 10:41:14', '2015-12-02 10:59:30', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('170', '22', '166', '分组权限表', 'AUTH_GROUP_ACCESS', '{{C(&quot;DB_PREFIX&quot;)}}auth_group_access', 'string', 'form_textbox', '\"\"', '', '2016-01-10 10:41:22', '2015-12-02 10:59:45', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('171', '22', '166', '权限规则表', 'AUTH_RULE', '{{C(&quot;DB_PREFIX&quot;)}}auth_rule', 'string', 'form_textbox', '\"\"', '', '2016-01-10 10:40:54', '2015-12-02 10:59:58', '1', '0', '0');
INSERT INTO `ebcms_config` VALUES ('172', '22', '166', '用户信息表', 'AUTH_USER', '{{C(&quot;DB_PREFIX&quot;)}}user', 'string', 'form_textbox', '\"\"', '', '2016-01-10 10:40:39', '2015-12-02 11:00:10', '1', '0', '0');

-- ----------------------------
-- Table structure for `ebcms_configcate`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_configcate`;
CREATE TABLE `ebcms_configcate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `module` varchar(255) NOT NULL DEFAULT 'Common' COMMENT '生效模块',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `iconcls` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='核心配置分类表';

-- ----------------------------
-- Records of ebcms_configcate
-- ----------------------------
INSERT INTO `ebcms_configcate` VALUES ('2', '基本配置', 'Common', '系统变量名', '', 'icon-zone', '2015-12-28 16:58:06', '2015-08-31 21:53:41', '99', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('3', '基本配置', 'Common', '应用设定', '', 'icon-zone', '2015-12-28 16:58:09', '2015-08-31 20:58:33', '95', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('4', '基本配置', 'Common', '默认设定', '', 'icon-zone', '2015-12-28 16:58:13', '2015-08-31 21:08:15', '88', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('5', '基本配置', 'Common', 'URL配置', '', 'icon-ruler_crop', '2015-12-28 16:58:16', '2015-07-25 17:04:49', '79', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('6', '基本配置', 'Common', '模板配置', '', 'icon-blog', '2015-12-28 16:58:19', '2015-07-25 17:04:49', '74', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('7', '基本配置', 'Common', '错误设置', '', 'icon-zone', '2015-12-28 16:58:23', '2015-08-31 21:30:19', '49', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('8', '基本配置', 'Common', '日志设置', '', 'icon-zone', '2015-12-28 16:58:26', '2015-08-31 21:33:10', '39', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('9', '基本配置', 'Common', '数据缓存设置', '', 'icon-zone', '2015-12-28 16:58:28', '2015-08-31 21:25:57', '22', '0', '0');
INSERT INTO `ebcms_configcate` VALUES ('10', '基本配置', 'Common', 'SESSION设置', '', 'icon-zone', '2015-12-28 16:58:31', '2015-08-31 21:36:23', '10', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('11', '基本配置', 'Common', 'Cookie设置', '', 'icon-zone', '2015-12-28 16:58:34', '2015-08-31 21:13:05', '7', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('12', '基本配置', 'Common', '数据库配置', '', 'icon-database', '2015-12-28 16:58:36', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('15', '后台配置', 'Admin', '表单令牌', '开启表单令牌能防止数据重复提交', 'icon-accept', '2015-12-28 16:59:01', '2015-07-25 17:04:49', '97', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('16', '后台配置', 'Admin', '数据备份', '', 'icon-databases', '2015-12-28 16:58:54', '2015-07-25 17:04:49', '96', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('17', '后台配置', 'Admin', 'cookie设置', '', 'icon-zone', '2015-12-28 16:59:35', '2015-09-03 18:20:12', '70', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('18', '后台配置', 'Admin', 'SESSION配置', '', 'icon-shape_aling_center', '2015-12-28 16:59:37', '2015-07-25 17:04:49', '18', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('19', '后台配置', 'Admin', '其他配置', '', 'icon-zone', '2015-12-28 16:59:44', '2015-07-28 19:56:30', '1', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('21', '前台配置', 'Home', '基本配置', '', '', '2015-12-28 16:59:49', '2015-08-07 09:11:57', '1', '1', '0');
INSERT INTO `ebcms_configcate` VALUES ('22', '基本配置', 'Common', 'AUTH认证', '', 'icon-zone', '2016-01-10 10:41:56', '2015-12-02 10:56:10', '1', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_cron`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_cron`;
CREATE TABLE `ebcms_cron` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `cron` varchar(20) NOT NULL DEFAULT '' COMMENT '任务文件',
  `intervals` int(10) unsigned NOT NULL DEFAULT '3600' COMMENT '任务执行间隔时间',
  `initruntime` datetime NOT NULL DEFAULT '1970-00-00 00:00:00' COMMENT '初始执行时间',
  `config` text COMMENT '扩展信息',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行次数',
  `iconcls` varchar(20) NOT NULL DEFAULT '' COMMENT '图标类',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='定时任务表';

-- ----------------------------
-- Records of ebcms_cron
-- ----------------------------
INSERT INTO `ebcms_cron` VALUES ('14', '0', 'clearbackuplockfile', '14400', '2015-10-09 00:00:00', '', '9496', 'icon-zone', '清理一天之前的备份安全文件', '2016-01-17 20:24:58', '2015-10-08 22:16:18', '1', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_datadict`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_datadict`;
CREATE TABLE `ebcms_datadict` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `category_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分类ID',
  `text` varchar(20) NOT NULL DEFAULT '' COMMENT '标题',
  `value` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `ext` text COMMENT '扩展信息',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=321 DEFAULT CHARSET=utf8 COMMENT='数据字典表';

-- ----------------------------
-- Records of ebcms_datadict
-- ----------------------------
INSERT INTO `ebcms_datadict` VALUES ('1', '0', '2', '点击推事件', 'click', '{\"tpl_list\":\"123123\",\"model\":\"mysql\",\"pagenum\":\"3\",\"tpl_detail\":\"sadf\",\"test\":\"2015-11-16 16:34:25\"}', 'f123', '2015-11-16 19:02:30', '2015-09-09 15:27:33', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('2', '0', '2', '跳转URL', 'view', '', '', '2015-09-09 15:27:54', '2015-09-09 15:27:54', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('3', '0', '2', '扫码推事件', 'scancode_push', '', '', '2015-09-09 15:28:09', '2015-09-09 15:28:09', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('4', '0', '2', '扫码等收消息', 'scancode_waitmsg', '', '', '2015-09-09 15:28:21', '2015-09-09 15:28:21', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('5', '0', '2', '系统拍照发图', 'pic_sysphoto', '', '', '2015-09-09 15:28:34', '2015-09-09 15:28:34', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('6', '0', '2', '弹拍照或相册', 'pic_photo_or_album', '', '', '2015-09-09 15:28:46', '2015-09-09 15:28:46', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('7', '0', '2', '弹微信相册', 'pic_weixin', '', '', '2015-09-09 15:28:58', '2015-09-09 15:28:58', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('8', '0', '2', '地理位置', 'location_select', '', '', '2015-09-09 15:29:22', '2015-09-09 15:29:22', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('9', '0', '3', '后台菜单', 'admin', '', '', '2015-08-09 21:16:45', '2015-08-09 21:16:45', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('10', '0', '4', '图片文件', 'jpeg,jpg,gif,bmp,png', '', '', '2015-12-27 00:17:46', '2015-07-25 17:04:49', '20', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('11', '10', '4', 'JPG', 'jpg', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('12', '10', '4', 'PNG', 'png', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('13', '10', '4', 'GIF', 'gif', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('14', '10', '4', 'BMP', 'bmp', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('15', '10', '4', 'JPEG', 'jpeg', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('16', '0', '4', '视频文件', 'rmvb,rmb,mkv,mp4,3gp,avi', '', '', '2015-12-27 00:17:52', '2015-07-25 17:04:49', '10', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('17', '16', '4', 'RMVB', 'rmvb', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('18', '16', '4', 'MKV', 'mkv', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('19', '16', '4', 'RMB', 'rmb', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('20', '16', '4', 'AVI', 'avi', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('21', '16', '4', 'MP4', 'mp4', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('22', '16', '4', '3GP', '3gp', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('23', '0', '4', '其他类型', 'zip,rar,7z', '', '', '2015-08-30 19:17:08', '2015-08-30 19:16:38', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('24', '0', '5', '前台模板', '{{dir_encrypt(\'.\'.__GROUP__.\'/Application/Home/View\')}}', '', '', '2015-11-17 10:38:01', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('25', '0', '5', 'Css样式', '{{dir_encrypt(\'.\'.__GROUP__.\'/Public/Css\')}}', '', '', '2015-11-17 10:38:12', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_datadict` VALUES ('26', '0', '5', 'JS代码', '{{dir_encrypt(\'.\'.__GROUP__.\'/Public/Js\')}}', '', '', '2015-11-17 10:38:18', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_datadict` VALUES ('27', '0', '6', '字符', 'string', '', 'required:true/false //是否必填\r\n', '2015-10-20 09:27:04', '2015-07-25 17:04:49', '250', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('29', '0', '6', '整数', 'number', '', '\r\n\r\n\r\n\r\n\r\n\r\n\r\n**注意事项**\r\n*双大括号括起来的标示变量，例如:{{time()}}、{{__FILE__}、{{$value}}等等', '2015-10-20 15:19:39', '2015-07-25 17:04:49', '222', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('30', '0', '6', '布尔', 'bool', '', '0或1\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n**注意事项**\r\n*双大括号括起来的标示变量，例如:{{time()}}、{{__FILE__}、{{$value}}等等', '2015-10-15 14:21:55', '2015-07-25 17:04:49', '210', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('31', '0', '6', '浮点数', 'float', '', '一行一条，例如：\r\n中国\r\n日本\r\n韩国\r\n德国\r\n\r\n\r\n\r\n**注意事项**\r\n*双大括号括起来的标示变量，例如:{{time()}}、{{__FILE__}、{{$value}}等等', '2015-10-20 15:19:54', '2015-07-25 17:04:49', '195', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('273', '0', '15', '否', '0', '', '', '2015-10-15 14:32:30', '2015-10-15 14:28:28', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('32', '0', '6', 'JSON', 'json', '', '标准的json数据格式！\r\n例如：\r\n{\r\n&quot;key1&quot;:&quot;value1&quot;,\r\n&quot;key2&quot;:&quot;value2&quot;\r\n}\r\n例如：\r\n[&quot;value1&quot;,&quot;value2&quot;]\r\n**注意事项**\r\n*双大括号括起来的标示变量，例如:{{time()}}、{{__FILE__}、{{$value}}等等', '2015-10-15 14:24:12', '2015-07-25 17:04:49', '40', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('33', '0', '6', 'INI', 'ini', '', 'key1=value1;\r\nkey2=value2;\r\n\r\n\r\n\r\n\r\n\r\n\r\n**注意事项**\r\n*双大括号括起来的标示变量，例如:{{time()}}、{{__FILE__}、{{$value}}等等', '2015-10-15 14:24:21', '2015-07-25 17:04:49', '25', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('34', '0', '6', 'YAML', 'yaml', '', '\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n**注意事项**\r\n*双大括号括起来的标示变量，例如:{{time()}}、{{__FILE__}、{{$value}}等等', '2015-10-15 14:28:58', '2015-07-25 17:04:49', '14', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('35', '0', '6', 'XML', 'xml', '', '\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n**注意事项**\r\n*双大括号括起来的标示变量，例如:{{time()}}、{{__FILE__}、{{$value}}等等', '2015-10-15 14:24:32', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('36', '0', '7', '亚洲', 'AISA', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '50', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('37', '36', '7', '阿富汗', 'AFG', '{\"shoudu\":\"\\u5317\\u4eac\",\"quancheng\":\"\\u5927\\u5c0f\\u679c\",\"guoqi\":\"\\/image\\/20151116\\/5649ba972e35a.jpg\"}', 'Afghanistan [æf\'ɡænistæn] [AFG; AF]  喀布尔（Kabul [\'kɔ:bl; kə\'bu:l]）     the Islamic State of Afghanistan 阿富汗伊斯兰国', '2015-11-16 19:14:44', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('38', '36', '7', '阿塞拜疆', 'AZE', '', 'Azerbaijan [͵ɑ:zəbai\'dӡɑ:n] [AZE; AZ]  巴库（Baku [bɑ:\'ku:]）     the Republic of Azerbaijan阿塞拜疆共和国', '2015-07-28 16:31:25', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('39', '36', '7', '阿曼', 'OMA', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('40', '36', '7', '阿联酋', 'UAE', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('41', '36', '7', '中国', 'CHN', '', 'China [\'t∫ainə] [CHN; CN]  北京（Beijing 或Peking[pi:\'kiŋ,\'pi:\'kiŋ]）     the People\'s Republic of China 中国；中华人民共和国     （另外：香港 Hong Kong [`hɔŋ`kɔŋ] / 台湾 Taiwan / 澳门 Macau =Macao [mə\'kau]）', '2015-07-28 16:19:35', '2015-07-28 16:15:56', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('42', '36', '7', '蒙古', 'MNG', '', 'Mongolia [mɔŋ\'ɡəuljə] [MNG; MN] 蒙古    乌兰巴托（Ulan Bator [\'u:lɑ:n \'bɑ:tɔ:]）', '2015-07-28 16:19:26', '2015-07-28 16:16:19', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('43', '36', '7', '朝鲜', 'PRK', '', 'North Korea\\ the Democratic People\'s Republic of Korea [kə\'rɪə] [PRK; KP] 朝鲜民主主义人民 共和国      平壤（Pyongyang [\'pjɔŋ\'jæŋ]）', '2015-07-28 16:19:16', '2015-07-28 16:16:38', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('44', '36', '7', '韩国', 'KOR', '', 'South Korea\\ (the) Republic of Korea [KOR; KR]大韩民国  首尔(Seoul [səul] 旧译“汉城”)', '2015-07-28 16:19:06', '2015-07-28 16:17:10', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('45', '36', '7', '日本', 'JPN', '', 'Japan [dӡə\'pæn] [JPN; JP] 日本国  东京（Tokyo [\'təukjəu]）', '2015-07-28 16:18:56', '2015-07-28 16:17:24', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('46', '36', '7', '菲律宾', 'PHL', '', '(the) Philippines [\'filipi:nz，-painz] [PHL; PH]  马尼拉(Manila [mə\'nilə])     the Republic of the Philippines 菲律宾共和国 ', '2015-07-28 16:18:35', '2015-07-28 16:17:42', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('47', '36', '7', '越南', 'VNM', '', 'Viet Nam [͵vɪət\'næm] [VNM; VN]  河内(Hanoi [hæ\'nɔi])  the Socialist Republic of Viet Nam 越南社会主义共和国', '2015-07-28 16:18:46', '2015-07-28 16:17:55', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('48', '36', '7', '老挝', 'LAO', '', 'Laos [\'lauz] [LAO; LA]  万象（Vientiane [vjen\'tjæn]）     the Lao People\'s Democratic Republic 老挝人民民主共和国', '2015-07-28 16:20:13', '2015-07-28 16:20:13', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('49', '36', '7', '柬埔寨', 'KHM', '', 'Cambodia [kæm\'bəudiə] [KHM; KH]  金边（Phnom Penh [pə\'nɔm \'pen]）     the Kingdom of Cambodia 柬埔寨王国 ', '2015-07-28 16:20:28', '2015-07-28 16:20:28', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('50', '36', '7', '泰国', 'THA', '', 'Thailand [\'tailænd] [THA; TH]  曼谷(Bangkok [bæŋ\'kɔk])     the Kingdom of Thailand 泰国；泰王国', '2015-07-28 16:20:50', '2015-07-28 16:20:50', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('51', '36', '7', '马来西亚', 'MYS', '', 'Malaysia [mə\'lei∫ə, mə\'leiziə] [MYS; MY]马来西亚       吉隆坡（Kuala Lumpur [\'kwɑ:lə \'lumpuə]）', '2015-07-28 16:21:28', '2015-07-28 16:21:13', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('52', '36', '7', '文莱', 'BRN', '', 'Brunei Darussalam [\'bru:nai] [BRN; BN] 文莱达鲁萨兰国      斯里巴加湾市（Bandar Seri Begawan [bæn\'dɑ: \'siəri: bə\'ɡɑ:wæn]）', '2015-07-28 16:21:48', '2015-07-28 16:21:48', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('53', '36', '7', '新加坡', 'SGP', '', 'Singapore [͵siŋɡə\'pɔ:] [SGP; SG]  新加坡市（Singapore City）     the Republic of Singapore 新加坡共和国', '2015-07-28 16:22:03', '2015-07-28 16:22:03', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('54', '36', '7', '印度尼西亚', 'IDN', '', 'Indonesia [͵indəu\'ni:zjə] [IDN; ID]  雅加达（ Jakarta [dӡə\'kɑ:tə]）  the Republic of Indonesia 印度尼西亚共和国 ', '2015-07-28 16:22:19', '2015-07-28 16:22:19', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('55', '36', '7', '缅甸', 'MMR', '', 'Myanmar [\'mjænmɑ:(r)]（亦作Burma [\'bə:mə]）[MMR; MM] 仰光(Rangoon [ræŋ\'ɡu:n])  the Union of Myanmar 缅甸联邦', '2015-07-28 16:22:32', '2015-07-28 16:22:32', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('56', '36', '7', '尼泊尔', 'NPL', '', 'Nepal [ni\'pɔ:l] [NPL; NP]  加德满都 (Kathmandu [\'kɑ:tmɑ:n\'du:])     the Kingdom of Nepal 尼泊尔王国 ', '2015-07-28 16:22:49', '2015-07-28 16:22:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('57', '36', '7', '不丹', 'BTN', '', 'Bhutan [bu:\'tɑ:n] [BTN; BT]  廷布（Thimphu [`θɪmfu:,-pu:,θɪm`pu:]）     the Kingdom of Bhutan 不丹王国 ', '2015-07-28 16:23:06', '2015-07-28 16:23:06', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('58', '36', '7', '孟加拉', 'BGD', '', 'Bangladesh [͵bɑ:ŋɡlə\'de∫] [BGD; BI]   达卡（Dhaka [`dækə]）     the People\'s Republic of Bangladesh孟加拉人民共和国', '2015-07-28 16:23:21', '2015-07-28 16:23:21', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('59', '36', '7', '印度', 'IND', '', 'India [\'indjə] [IND; IN]          新德里（New Delhi [nju: \'deli]）     the Republic of India 印度共和国', '2015-07-28 16:23:35', '2015-07-28 16:23:35', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('60', '36', '7', '巴基斯坦', 'PAK', '', 'Pakistan [͵pɑ:kis\'tɑ:n] [PAK; PK]  伊斯兰堡(Islamabad [is\'lɑ:məbɑ:d])     the Islamic [iz`læmik] Republic of Pakistan巴基斯坦伊斯兰共和国', '2015-07-28 16:23:50', '2015-07-28 16:23:50', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('61', '36', '7', '斯里兰卡', 'LKA', '', 'Sri Lanka [sri\'læŋkə] [LKA; LK]  科伦坡(Colombo [kə\'lʌmbəu])      the Democratic Socialist Republic of Sri Lanka斯里兰卡民主社会主义共和国 ', '2015-07-28 16:24:07', '2015-07-28 16:24:07', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('62', '36', '7', '马尔代夫', 'MDV', '', 'Maldives [\'mɔ:ldaivz, \'mæl-] [MDV; MV] 马累 (Male [\'mɑ:lei])  the Republic of Maldives 马尔代夫共和国', '2015-07-28 16:24:20', '2015-07-28 16:24:20', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('63', '36', '7', '哈萨克斯坦', 'KAZ', '', 'Kazakhstan [͵kɑ:zɑ:k\'stɑ:n] [KAZ; KZ]  阿斯塔纳 (Astana [ə\'stɑ:nə])     the Republic of Kazakhstan 哈萨克斯坦共和国', '2015-07-28 16:24:36', '2015-07-28 16:24:36', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('64', '36', '7', '吉尔吉斯斯坦', 'KGZ', '', 'Kyrgyzstan [\'kɪəgi:͵stɑ:n, ͵kɪəgi: \'stɑ:n] [KGZ; KG] 比什凯克 (Bishkek [\'biʃkek, \'bi:ʃ-])     the Kyrgyz Republic 吉尔吉斯斯坦；吉尔吉斯共和国 ', '2015-07-28 16:24:53', '2015-07-28 16:24:53', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('65', '36', '7', '塔吉克斯坦', 'TJK', '', 'Tajikistan [tɑ:dӡiki\'stɑ:n] [TJK; TJ]    杜尚别 (Dushanbe [dju:`∫ɑ:nbɪ])     the Republic of Tajikistan 塔吉克斯坦共和国', '2015-07-28 16:25:07', '2015-07-28 16:25:07', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('66', '36', '7', '乌兹别克斯坦', 'UZB', '', 'Uzbekistan [͵uzbeki\'stɑ:n] [UZB; UZ]  塔什干 (Tashkent [tæ∫\'kent])     the Republic of Uzbekistan 乌兹别克斯坦共和国', '2015-07-28 16:25:20', '2015-07-28 16:25:20', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('67', '36', '7', '土库曼斯坦', 'TKM', '', 'Turkmenistan [͵tə:kmeni\'stɑ:n] [TKM; TM]土库曼斯坦     阿什哈巴德(Ashgabat [͵ɑ:ʃgɑ: \'bɑ:t] ) ', '2015-07-28 16:25:45', '2015-07-28 16:25:45', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('68', '36', '7', '伊拉克', 'IRQ', '', 'Iraq [i\'rɑ:k] [IRQ; IQ]  巴格达(Baghdad [`bæɡdæd])     the Republic of Iraq 伊拉克共和国', '2015-07-28 16:26:30', '2015-07-28 16:26:30', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('69', '36', '7', '伊朗', 'IRN', '', 'Iran [i\'rɑ:n] [IRN; IR]            德黑兰 (Tehran [teə`ræn,-`rɑ:n])     the Islamic Republic of Iran 伊朗伊斯兰共和国', '2015-07-28 16:26:44', '2015-07-28 16:26:44', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('70', '36', '7', '叙利亚', 'SYR', '', 'Syria [\'siriə] [SYR; SY]           大马士革（Damascus [də\'mæskəs]）     the Syrian Arab Republic阿拉伯叙利亚共和', '2015-07-28 16:27:10', '2015-07-28 16:27:10', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('71', '36', '7', '约旦', 'JOR', '{\"fucs\":\"123\",\"pic\":\"\",\"asd\":\"&lt;p&gt;\\u963f\\u65af\\u8482\\u82ac\\u8428\\u82ac&lt;img alt=&quot;568d241c09ad6.gif&quot; src=&quot;http:\\/\\/localhost\\/abc\\/localhost\\/Uploads\\/image\\/20160106\\/568d241c09ad6.gif&quot;\\/&gt;&lt;\\/p&gt;\"}', 'Jordan [\'dӡɔ:dn] [JOR; JO]  安曼（Amman [æ\'mɑ:n]）      the Hashemite [`hæ∫ɪmaɪt] Kingdom of Jordan约旦哈希姆王国', '2016-01-06 22:29:09', '2015-07-28 16:27:26', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('72', '36', '7', '黎巴嫩', 'LBN', '', 'Lebanon [\'lebənən] [LBN; LB]  贝鲁特（Beirut [bei\'ru:t]）     the Republic of Lebanon 黎巴嫩共和国', '2015-07-28 16:27:39', '2015-07-28 16:27:39', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('73', '36', '7', '以色列', 'ISR', '', 'Israel [\'izreiəl] [ISR; IL]  特拉维夫(Tel Aviv [͵telə\'vi:v])     the State of Israel 以色列国 ', '2015-07-28 16:27:59', '2015-07-28 16:27:59', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('74', '36', '7', '巴勒斯坦', 'Palestine', '', 'Palestine [\'pælistain]       耶路撒冷 (Jerusalem [dӡe\'ru:sələm])     the state of Palestine [\'pælistain]  巴勒斯坦国', '2015-07-28 16:28:28', '2015-07-28 16:28:28', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('75', '36', '7', '沙特', 'SAU', '', 'Saudi Arabia [\'saudi ə\'reibjə] [SAU; SA]  利雅得（Riyadh [ri:\'jɑ:d]）     the Kingdom of Saudi Arabia 沙特阿拉伯王国', '2015-07-28 16:28:57', '2015-07-28 16:28:57', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('76', '36', '7', '巴林', 'BHR', '', 'Bahrain [bɑ: \'reɪn] [BHR; BH]  麦纳麦（Manama [mæ\'næmə]）     the Kingdom of Bahrain 巴林王国', '2015-07-28 16:29:10', '2015-07-28 16:29:10', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('77', '36', '7', '卡塔尔', 'QAT', '', 'Qatar [\'kɑ:tər] (=katar) [QAT; QA] 多哈 (Doha [\'dəuhə])     the State of Qatar 卡塔尔国', '2015-07-28 16:29:24', '2015-07-28 16:29:24', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('78', '36', '7', '科威特', 'KWT', '', 'Kuwait [ku\'weit] [KWT; KW] 科威特城 (Kuwait City)     the State of Kuwait 科威特国', '2015-07-28 16:29:38', '2015-07-28 16:29:38', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('79', '36', '7', '阿联酋', 'ARE', '{\"fucs\":\"1234\",\"pic\":\"\",\"asd\":\"&lt;p&gt;\\u963f\\u65af\\u8482\\u82ac\\u8428\\u82ac&lt;img src=&quot;http:\\/\\/localhost\\/abc\\/localhost\\/Uploads\\/image\\/20160106\\/568d269ef0bd7.png&quot; title=&quot;568d269ef0bd7.png&quot; alt=&quot;scrawl.png&quot;\\/&gt;&lt;\\/p&gt;\"}', 'the United Arab Emirates [\'emɪərət] [ARE; AE] 阿布扎比（Abu Dhabi [\'æbu: \'ðæbi:]）     阿拉伯联合酋长国', '2016-01-06 22:37:24', '2015-07-28 16:30:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('80', '36', '7', '苏丹', 'OMN', '', 'Oman [əu\'mɑ:n] [OMN; OM]  马斯喀特（Muscat [\'mʌskət]）     the Sultanate [\'sʌltənit] of Oman阿曼苏丹国 ', '2015-07-28 16:30:16', '2015-07-28 16:30:16', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('81', '36', '7', '也门', 'YEM', '', 'Yemen [\'jemən] [YEM; YE]  萨那（Sana\'a）     the Republic of Yemen 也门共和国', '2015-07-28 16:30:29', '2015-07-28 16:30:29', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('82', '36', '7', '格鲁吉亚', 'GEO', '', 'Georgia [\'dӡɔ:dӡjə] [GEO; GE]格鲁吉亚  第比利斯（Tbilisi [tə\'bilisi，tbili:\'si:]）', '2015-07-28 16:30:47', '2015-07-28 16:30:47', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('83', '36', '7', '亚美尼亚', 'ARM', '', 'Armenia [ɑ:\'mi:njə] [ARM; AM]  埃里温（Yerevan [\'jere\'vɑ:n]）     the Republic of Armenia 亚美尼亚共和国', '2015-07-28 16:31:03', '2015-07-28 16:31:03', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('84', '36', '7', '土耳其', 'TUR', '{\"fucs\":\"123\\u8428\\u82ac\",\"pic\":\"\\/image\\/20160106\\/568d241c09ad6.gif\",\"asd\":\"&lt;p&gt;\\u963f\\u65af\\u8482\\u82ac\\u8428\\u82ac\\u963f\\u8428\\u5fb7\\u53d1&lt;img src=&quot;http:\\/\\/img.baidu.com\\/hi\\/jx2\\/j_0015.gif&quot;\\/&gt;&lt;\\/p&gt;\"}', 'Turkey [\'tə:ki] [TUR; TR]           安卡拉 (Ankara [\'æŋkərə])  the Republic of Turkey 土耳其共和国', '2016-01-06 22:26:54', '2015-07-28 16:31:44', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('85', '36', '7', '塞浦路斯', 'CYP', '', 'Cyprus [\'saiprəs] [CYP; CY]  尼克西亚（Nicosia [͵nikəu\'si(:)ə]）  the Republic of Cyprus 塞浦路斯共和国', '2015-07-28 16:32:02', '2015-07-28 16:32:02', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('86', '0', '7', '欧洲', 'ouzhou', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '40', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('87', '86', '7', '阿尔巴尼亚', 'ALB', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('88', '86', '7', '安道尔', 'AND', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('89', '86', '7', '奥地利', 'AUT', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('90', '86', '7', '爱沙尼亚', 'EST', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('91', '86', '7', '爱尔兰', 'IRL', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('92', '0', '7', '非洲', 'AFRICA', '', '', '2015-07-28 16:32:19', '2015-07-25 17:04:49', '30', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('93', '92', '7', '阿尔及利亚', 'ALG', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('94', '92', '7', '安哥拉', 'ANG', '', 'Angola [æŋ\'ɡəulə] [AGO; AO]  罗安达（Luanda [lu:\'ɑ:ndə]）     the Republic of Angola 安哥拉共和国 ', '2015-07-28 16:58:26', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('95', '92', '7', '埃及', 'EGY', '', 'Egypt [\'i:dӡɪpt] [EGY; EG]  开罗 （Cairo [\'kaiərəu]）     the Arab Republic of Egypt 阿拉伯埃及共和国', '2015-07-28 16:33:00', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('96', '92', '7', '埃塞俄比亚', 'ETH', '', 'Ethiopia [͵i:θi\'əupjə] [ETH; ET]  亚的斯亚贝巴（Addis Ababa [\'ædis \'æbəbə]）     the Federal Democratic Republic of Ethopia 埃塞俄比亚联邦民主共和国', '2015-07-28 16:48:58', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('97', '92', '7', '利比亚', 'LBY', '', 'Libya [\'libiə] [LBY; LY]  的黎波里（Tripoli [\'tripəli]）      the Great Socialist People\'s Libyan [\'libjən] Arab Jamahiriya阿拉伯利比亚民众国；大阿拉伯', '2015-07-28 16:33:23', '2015-07-28 16:33:23', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('98', '92', '7', '苏丹', 'SDN', '', '(the) Sudan [su:\'dæn] [SDN; SD]  喀土穆(Khartoum [kɑ:\'tu:m])     the Republic of the Sudan 苏丹共和国', '2015-07-28 16:33:38', '2015-07-28 16:33:38', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('99', '92', '7', '突尼斯', 'TUN', '', 'Tunisia [tju(:)\'niziə] [TUN; TN]   突尼斯市（Tunis [\'tju:nis]）     the Republic of Tunisia 突尼斯共和国', '2015-07-28 16:33:51', '2015-07-28 16:33:51', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('100', '92', '7', '阿尔及利亚', 'DZA', '', 'Algeria [æl\'dӡiəriə] [DZA; DZ]  阿尔及尔（Alger[\'ældӡə]或Algiers[æl\'dӡiəz])     the People\'s Democratic Republic of Algeria阿尔及利亚人民民主共和国 ', '2015-07-28 16:34:07', '2015-07-28 16:34:07', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('101', '92', '7', '摩洛哥', 'MAR', '', 'Morocco [mə\'rɔkəu] [MAR; MA]  拉巴特(Rabat [rə\'bɑ:t])     \r\nthe Kingdom of Morocco 摩洛哥王国', '2015-07-28 16:34:22', '2015-07-28 16:34:22', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('102', '92', '7', '厄立特里亚', 'ERI', '', 'Eritrea [͵eri\'tri(:)ə] [ERI; ER]  阿斯马拉（Asmara [æz\'mɑ:rə]）     The State of Eritrea厄立特里亚国', '2015-07-28 16:49:19', '2015-07-28 16:49:19', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('103', '92', '7', '索马里', 'SOM', '', 'Somalia [səu\'mɑ:liə] [SOM; SO]  摩加迪沙 (Mogadishu [͵mɔ:ɡɑ:`di:∫u:])     the Somali Democratic Republic 索马里民主共和国', '2015-07-28 16:49:33', '2015-07-28 16:49:33', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('104', '92', '7', '吉布提', 'DJI', '', 'Djibouti [dӡi\'bu:ti] [DJI; DJ]  吉布提市（Djibouti）     the Republic of Djibouti 吉布提共和国', '2015-07-28 16:49:49', '2015-07-28 16:49:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('105', '92', '7', '肯尼亚', 'KEN', '', 'Kenya [\'ki:njə,\'kenjə] [KEN; KE]  内罗毕（Nairobi [͵naiə\'rəubi]）     the Republic of Kenya 肯尼亚共和国', '2015-07-28 16:50:04', '2015-07-28 16:50:04', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('106', '92', '7', '坦桑尼亚', 'TZA', '', 'Tanzania [͵tænzə\'ni:ə] [TZA; TZ]  达累斯萨拉姆 (Dares Salaam)     the United Republic of Tanzania 坦桑尼亚联合共和国', '2015-07-28 16:50:17', '2015-07-28 16:50:17', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('107', '92', '7', '乌干达', 'UGA', '', 'Uganda [ju(:)\'ɡændə，u:\'ɡændə] [UGA; UG]  坎帕拉 (Kampala [kɑ:m\'pɑ:lə])     the Republic of Uganda 乌干达共和国', '2015-07-28 16:50:29', '2015-07-28 16:50:29', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('108', '92', '7', '布隆迪', 'BDI', '', 'Burundi [bu\'rundi] [BDI; BD]  布琼布拉（Bujumbura [͵bu:dӡəm\'burə]）   the Republic of Burundi 布隆迪共和国', '2015-07-28 16:50:42', '2015-07-28 16:50:42', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('109', '92', '7', '塞舌尔', 'SYC', '', 'Seychelles [sei\'∫elz] [SYC; SC]  维多利亚(Victoria [vik\'tɔ:riə])     the Republic of Seychelles 塞舌尔共和国 ', '2015-07-28 16:50:55', '2015-07-28 16:50:55', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('110', '92', '7', '乍得', 'TCD', '', 'Chad [t∫æd] [TCD; TD]               恩贾梅纳（N\'Djamena [n\'dӡɑ:menɑ:]）     the Republic of Chad 乍得共和国', '2015-07-28 16:51:14', '2015-07-28 16:51:14', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('111', '92', '7', '班吉', 'CAF', '', 'the Central African Republic [CAF; CF]中非共和国    班吉（Bangui [bɑ:ŋ\'ɡi]）', '2015-07-28 16:51:30', '2015-07-28 16:51:30', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('112', '92', '7', '喀麦隆', 'CMR', '', 'Cameroon [\'kæməru:n] [CMR; CM]              雅温得（Yaounde [͵jɑ:un\'dei]）     the Republic of Cameroon 喀麦隆共和国', '2015-07-28 16:51:44', '2015-07-28 16:51:44', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('113', '92', '7', '几内亚', 'GNQ', '', 'Equatorial Guinea [͵ekwə\'tɔ:riəl \'ɡini] [GNQ; GQ]   马拉博（Malabo [\'mɑ:lɑ:bəu]）     the Republic of Equatorial Guinea赤道几内亚共和国', '2015-07-28 16:52:01', '2015-07-28 16:52:01', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('114', '92', '7', '加蓬', 'GAB', '', 'Gabon [ɡæ\'bɔn] [GAB; GA]                  利伯维尔（Libreville [͵li:brə\'vi:l]）     the Gabonese Republic 加蓬共和国', '2015-07-28 16:52:13', '2015-07-28 16:52:13', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('115', '92', '7', '刚果', 'COG', '', 'Congo [\'kɔŋɡəu] [COG; CG]                布拉柴维尔（Brazzaville [\'bræzəvil]）     the Republic of the Congo 刚果共和国', '2015-07-28 16:52:31', '2015-07-28 16:52:31', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('116', '92', '7', '刚果', 'COD', '', 'the Democratic Republic of the Congo[COD; CD]     金沙萨（Kinshasa [kin\'∫ɑ:sə]）     刚果民主共和国（曾名：扎伊尔Zaire[zə\'i:rə]）', '2015-07-28 16:52:44', '2015-07-28 16:52:44', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('117', '92', '7', '圣多美和普林西比', 'STP', '', 'Sao Tome [təum] and Principe [`prɪnsɪpi:] [STP; ST]     圣多美（Sao Tome）  the Democratic Republic of Sao Tome and Principe圣多美和普林西比民主共和国', '2015-07-28 16:53:19', '2015-07-28 16:53:19', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('118', '92', '7', '卢旺达', 'RWA', '', 'Rwanda [ru\'ændə] [RWA; RW]  基加利 (Kigali [ki\'ɡɑ:li])  the Rwandese Republic 卢旺达共和国 ', '2015-07-28 16:53:31', '2015-07-28 16:53:31', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('119', '92', '7', '毛里塔尼亚', 'MRT', '', 'Mauritania [͵mɔ(:)ri\'teinjə] [MRT; MR]  努瓦克肖特(Nouakchott [nu\'ɑ:k∫ɔt])     \r\nthe Islamic Republic of Mauritania 毛里塔尼亚伊斯兰共和国', '2015-07-28 16:53:50', '2015-07-28 16:53:50', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('120', '92', '7', '冈比亚', 'GMB', '', 'Gambia [\'ɡæmbiə] [GMB; GM]  班珠尔（Banjul [\'bændӡu:l]）     the Republic of the Gambia 冈比亚共和国', '2015-07-28 16:54:06', '2015-07-28 16:54:06', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('121', '92', '7', '马里', 'MLI', '', 'Mali [\'mɑ:li:] [MLI; ML]  巴马科 (Bamako [\'bɑ:məkəu])     the Republic of Mali 马里共和国', '2015-07-28 16:54:22', '2015-07-28 16:54:22', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('122', '92', '7', '瓦加杜古', 'BFA', '', 'the Burkina Faso [bз:͵ki:nə \'fæsəu]  [BFA; BF] 布基纳法索     瓦加杜古（Ouagadougou [͵wɑ:ɡə\'du:ɡu:]）', '2015-07-28 16:54:41', '2015-07-28 16:54:41', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('123', '92', '7', '几内亚', 'GIN', '', 'Guinea [\'ɡini] [GIN; GN]  科纳克里 （Conakry [\'kɔnəkri]）  the Republic of Guinea 几内亚共和国', '2015-07-28 16:54:56', '2015-07-28 16:54:56', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('124', '92', '7', '几内亚', 'GNB', '', 'Guinea-Bissau [\'ɡinibi\'səu] [GNB; GW]  比绍（Bissau [bis\'auŋ]）    the Republic of Guinea-Bissau 几内亚比绍共和国', '2015-07-28 16:55:15', '2015-07-28 16:55:15', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('125', '92', '7', '佛得角', 'CPV', '', 'Cape Verde [keɪp vз:d] [CPV; CV]  普拉亚（Praia [\'praiɑ:]）  the Republic of Cape Verde 佛得角共和国', '2015-07-28 16:55:32', '2015-07-28 16:55:32', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('126', '92', '7', '塞拉利昂', 'SLE', '', ' Sierra Leone [\'siərə li\'əun] [SLE; SL]  弗里敦 (Freetown [\'fri:taun]) the Republic of Sierra Leone 塞拉利昂共和国', '2015-07-28 16:55:49', '2015-07-28 16:55:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('127', '92', '7', '利比里亚', 'LBR', '', 'Liberia [lai\'biəriə] [LBR; LR]  蒙罗维亚（Monrovia [mən\'rəuviə]）     the Republic of Liberia 利比里亚共和国', '2015-07-28 16:56:04', '2015-07-28 16:56:04', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('128', '92', '7', '科特迪瓦', 'CIV', '', 'Cote Divoire [kəʊt] [CIV; CI]  亚穆苏克罗（Yamoussoukro [͵jɑ:mu:`su:krəʊ]）     the Republic of Cote d\'Ivoire 科特迪瓦共和国 （旧译“象牙海岸”）     Ivorian [`aɪvərɪən] 伊瓦的；伊瓦人；象牙海岸的', '2015-07-28 16:56:24', '2015-07-28 16:56:24', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('129', '92', '7', '加纳', 'GHA', '', 'Ghana [\'ɡɑ:nə] [GHA; GH]  阿克拉（Accra [ə\'krɑ:]）     the Republic of Ghana 加纳共和国', '2015-07-28 16:56:36', '2015-07-28 16:56:36', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('130', '92', '7', '多哥', 'TGO', '', 'Togo [\'təuɡəu] [TGO; TG]  洛美(Lome[͵lɔ: \'mei])    the Togolese Republic 多哥共和国', '2015-07-28 16:56:48', '2015-07-28 16:56:48', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('131', '92', '7', '贝宁', 'BEN', '', 'Benin [be\'nin] [BEN; BJ]           波多诺夫（Porto-Novo）     the Republic of Benin 贝宁共和国 ', '2015-07-28 16:57:02', '2015-07-28 16:57:02', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('132', '92', '7', '尼日尔', 'NER', '', '(the)Niger [\'naidӡə] [NER; NE]   尼亚美(Niamey [͵nja:\'mei])     the Republic of the Niger 尼日尔共和国', '2015-07-28 16:57:18', '2015-07-28 16:57:18', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('133', '92', '7', '尼日利亚', 'NGA', '', 'Nigeria [nai\'dӡiəriə] [NGA; NG]  阿布贾（Abuja[ɑ: \'bu:dʒɑ:]）(原是拉各斯lagos[\'leiɡɔs])  the Federal Republic of Nigeria 尼日利亚联邦共和国', '2015-07-28 16:57:33', '2015-07-28 16:57:33', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('134', '92', '7', '塞内加尔', 'SEN', '', 'Senegal [͵seni\'ɡɔ:l] [SEN; SN]  达喀尔(Dakar [\'dækə])  the Republic of Senegal 塞内加尔共和国', '2015-07-28 16:57:48', '2015-07-28 16:57:48', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('135', '92', '7', '赞比亚', 'ZMB', '', 'Zambia [\'zæmbiə] [ZMB; ZM]  卢萨卡(Lusaka [lu:\'sɑ:kə])     the Republic of Zambia 赞比亚共和国', '2015-07-28 16:58:07', '2015-07-28 16:58:07', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('136', '92', '7', '津巴布韦', 'ZWE', '', 'Zimbabwe [zim\'bɑ:bwei] [ZWE; ZW]  哈拉雷（Harare [hə\'rɑ:rei], 旧称索尔兹伯里）     the Republic of Zimbabwe 津巴布韦共和国', '2015-07-28 16:58:40', '2015-07-28 16:58:40', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('137', '92', '7', '马拉维', 'MWI', '', 'Malawi [mɑ:\'lɑ:wi] [MWI; MW]  利隆圭（Lilongwe [\'lilɔ:ŋkwi]）     the Republic of Malawi 马拉维共和国', '2015-07-28 16:58:54', '2015-07-28 16:58:54', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('138', '92', '7', '莫桑比克', 'MOZ', '', 'Mozambique [\'məuzəm\'bi:k] [MOZ; MZ]  马普托 (Maputo [mæ\'putə])     the Republic of Mozambique 莫桑比克共和国', '2015-07-28 16:59:06', '2015-07-28 16:59:06', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('139', '92', '7', '博茨瓦纳', 'BWA', '', 'Botswana [bɔt\'swɑ:nə] [BWA; BW]  哈伯罗内（Gaborone [͵ɡɑ:bə\'rəun，hɑ:bɔ:rɔ:\'ne]）     the Republic of Botswana 博茨瓦纳共和国', '2015-07-28 16:59:19', '2015-07-28 16:59:19', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('140', '92', '7', '纳米比亚', 'NAM', '', 'Namibia [nə\'mi:biə] [NAM; NA]  温得和克(Windhoek [\'vinthuk])     the Republic of Namibia 纳米比亚共和国', '2015-07-28 16:59:31', '2015-07-28 16:59:31', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('141', '92', '7', '南非', 'ZAF', '', 'South Africa [sauθ] [\'æfrikə] [ZAF; ZA]  比勒陀利亚(Pretoria [pri\'tɔ:riə])     the Republic of South Africa 南非共和国', '2015-07-28 16:59:44', '2015-07-28 16:59:44', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('142', '92', '7', '斯威士兰', 'SWZ', '', 'Swaziland [\'swɑ:zilænd] [SWZ; SZ]  姆巴巴内  (Mbabane [mbɑ:\'ba:n])     the Kingdom of Swaziland 斯威士兰王国', '2015-07-28 16:59:56', '2015-07-28 16:59:56', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('143', '92', '7', '莱索托', 'LSO', '', 'Lesotho [li\'su:tu:] [LSO; LS]  马塞卢（Maseru [\'mæzəru:]）     the Kingdom of Lesotho 莱索托王国', '2015-07-28 17:00:11', '2015-07-28 17:00:11', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('144', '92', '7', '马达加斯加', 'MDG', '', 'Madagascar [mædə\'ɡæskə] [MDG; MG]  the Republic of Madagascar 马达加斯加共和国     安塔那利佛（Antananarivo [͵æntə͵nænə\'ri:vəʊ, ͵ɑ:ntə͵nɑ:nə-]）', '2015-07-28 17:00:26', '2015-07-28 17:00:26', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('145', '92', '7', '科摩罗', 'COM', '', 'Comoros [\'kɔmərəus] [COM; KM]  莫罗尼（Moroni [mə\'rɔni]）     Union of Comoros 科摩罗联盟', '2015-07-28 17:00:42', '2015-07-28 17:00:42', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('146', '92', '7', '毛里求斯', 'MUS', '', 'Mauritius [mə\'ri∫əs] [MUS; MU]  路易港 (Port Louis [\'pɔ:t \'lu(:)i(s)])     the Republic of Mauritius 毛里求斯共和国', '2015-07-28 17:00:56', '2015-07-28 17:00:56', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('147', '0', '7', '大洋洲', 'dayang', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '5', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('148', '147', '7', '澳大利亚', 'AUS', '', 'Australia [ɔs\'treiljə] [AUS; AU]  堪培拉（Canberra [\'kænbərə]）     The Commonwealth[\'kɔmənwelθ] of Australia澳大利亚', '2015-07-28 17:35:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('149', '147', '7', '新西兰', 'NZL', '', 'New Zealand [nju:\'zi:lənd] [NZL; NZ] 新西兰   惠灵顿（Wellington [\'weliŋtən]）', '2015-07-28 17:36:02', '2015-07-28 17:36:02', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('150', '147', '7', '新几内亚', 'PNG', '', 'Papua New Guinea [\'pæpjuə] [\'ɡini] [PNG; PG]   莫尔斯比港（Port Moresby [\'mɔ:zbi]）    The Independent State of Papua New Guinea巴布亚新几内亚独立国', '2015-07-28 17:36:25', '2015-07-28 17:36:25', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('151', '147', '7', '所罗门', 'SLB', '', 'the Solomon [\'sɔləmən] Islands [SLB; SB] 所罗门群岛  霍尼亚拉（Honiara  [͵həʊnɪ`ɑ:rə]）', '2015-07-28 17:36:42', '2015-07-28 17:36:42', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('152', '147', '7', '瓦努阿图', 'VUT', '', 'Vanuatu [͵vɑ:nu:`ɑ:tu:] [VUT; VU]  维拉港( Port Vila [pɔ:t \'vi:lə])    the Republic of Vanuatu 瓦努阿图共和国 ', '2015-07-28 17:36:54', '2015-07-28 17:36:54', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('153', '147', '7', '密克罗尼西亚', 'FSM', '', 'Micronesia [͵maɪkrəʊ`ni:zɪə,-ӡə] [FSM; FM]  帕利基尔（Palikir [\'pælikə]）     the Federated States of Micronesia密克罗尼西亚联邦', '2015-07-28 17:37:15', '2015-07-28 17:37:15', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('154', '147', '7', '马绍尔', 'MHL', '', '(the)Marshall Island [\'mɑ:∫əl] [MHL; MH]  马朱罗（Majuro）     the Republic of Marshall Island马绍尔群岛共和国', '2015-07-28 17:37:29', '2015-07-28 17:37:29', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('155', '147', '7', '帕劳', 'PLW', '', 'Palau [pɑ:`laʊ] [PLW; PW]  科罗尔（Koror[\'kɔ:ɔ:] [\'kɔrɔr]）     the Republic of Palau 帕劳共和国', '2015-07-28 17:37:42', '2015-07-28 17:37:42', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('156', '147', '7', '瑙鲁', 'NRU', '', 'Nauru [nɑ:\'u:ru:] [NRU; NR]  亚伦 (Yaren [`jɑ:rən])     the Republic of Nauru 瑙鲁共和国［非联合国会员国', '2015-07-28 17:37:58', '2015-07-28 17:37:58', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('157', '147', '7', '基里巴斯', 'KIR', '', 'Kiribati [\'kɪrɪbæs] [KIR; KI]  塔拉瓦（Tarawa [tɑ:\'rɑ:wɑ:]）     the Republic of Kiribati基里巴斯共和国［非联合国会员国］', '2015-07-28 17:38:14', '2015-07-28 17:38:14', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('158', '147', '7', '图瓦卢', 'TUV', '', 'Tuvalu [\'tu:vəlu] [TUV; TV] 图瓦卢［非联合国会员国  富纳富提（Funafuti [͵fju:nə`fju:tɪ]）', '2015-07-28 17:38:31', '2015-07-28 17:38:31', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('159', '147', '7', '萨摩亚', 'WSM', '', 'Samoa [sə\'məuə] [WSM; WS]  阿皮亚(Apia [ɑ:\'pi:ɑ:])     the Independent State of Samoa 萨摩亚独立国', '2015-07-28 17:38:51', '2015-07-28 17:38:51', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('160', '147', '7', '斐济', 'FJI', '', 'Fiji [fi:\'dӡi:,\'fi:dӡi:] [FJI; FJ]  苏瓦 (Suva [\'su:və])     the Republic of the Fiji Islands 斐济群岛共和国 ', '2015-07-28 17:39:04', '2015-07-28 17:39:04', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('161', '0', '7', '南美洲', 'nanmeizhou', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '20', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('162', '161', '7', '阿根廷', 'ARG', '', 'Argentina [͵ɑ:dӡən\'ti:nə] [ARG; AR]  布宜诺斯艾利斯（Buenos Aires [\'bwenəs\'aiəriz]）    the Argentine [\'ɑ:dӡəntain] Republic  阿根廷共和国 ', '2015-07-28 17:34:53', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('163', '161', '7', '哥伦比亚', 'COL', '', 'Colombia [kə\'lɔmbiə] [COL; CO]   波哥大（Bogota [bəugə\'ta]）     the Republic of Colombia 哥伦比亚共和国 ', '2015-07-28 17:32:56', '2015-07-28 17:32:56', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('164', '161', '7', '委内瑞拉', 'VEN', '', 'Venezuela [͵vene\'zweilə] [VEN; VE]  加拉加斯(Caracas [kə\'rækəs])  the Republic of Venezuela 委内瑞拉共和国', '2015-07-28 17:33:09', '2015-07-28 17:33:09', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('165', '161', '7', '圭亚那', 'GUY', '', 'Guyana [ɡai\'ɑ:nə,ɡai\'ænə] [GUY; GY]  乔治敦（Georgetown [\'dӡɔ:dӡtaun]）     the Republic of Guyana 圭亚那共和国', '2015-07-28 17:33:21', '2015-07-28 17:33:21', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('166', '161', '7', '苏里南', 'SUR', '', 'Suriname [,su ərɪ`nɑ:mə] [SUR; SR]  帕拉马里博(Paramaribo [͵pærə`mærɪbəʊ])     the Republic of Suriname 苏里南共和国 ', '2015-07-28 17:33:32', '2015-07-28 17:33:32', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('167', '161', '7', '厄瓜多尔', 'ECU', '', 'Ecuador [͵ekwə\'dɔ:] [ECU; EC]  基多（Quito [\'ki:təu]）     the Republic of Ecuador 厄瓜多尔共和国', '2015-07-28 17:33:44', '2015-07-28 17:33:44', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('168', '161', '7', '秘鲁', 'PER', '', 'Peru [pə\'ru:，pi\'ru:] [PER; PE]  利马(Lima [\'laimə])     the Republic of Peru 秘鲁共和国', '2015-07-28 17:33:54', '2015-07-28 17:33:54', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('169', '161', '7', '玻利维亚', 'BOL', '', 'Bolivia [bə\'liviə] [BOL; BO]  拉巴斯（La Paz [lɑ:\'pæz]）     the Republic of Bolivia 玻利维亚共和国', '2015-07-28 17:34:05', '2015-07-28 17:34:05', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('170', '161', '7', '巴西', 'BRA', '', 'Brazil [brə\'zil] [BRA; BR]  巴西利亚（Brasilia [brə\'zi:ljə]） the Federative Republic of Brazil巴西联邦共和国', '2015-07-28 17:34:18', '2015-07-28 17:34:18', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('171', '161', '7', '智利', 'CHL', '', 'Chile [\'t∫ili] [CHL; CL]  圣地亚哥（Santiago [sænti\'ɑ:ɡəu]）     the Republic of Chile 智利共和国', '2015-07-28 17:34:30', '2015-07-28 17:34:30', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('172', '161', '7', '乌拉圭', 'URY', '', 'Uruguay [\'uruɡwai] [URY; UY]  蒙得维的亚(Montevideo [͵mɔntivi\'deiəu]) the Eastern Republic of Uruguay 乌拉圭东岸共和国', '2015-07-28 17:35:10', '2015-07-28 17:35:10', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('173', '161', '7', '巴拉圭', 'PRY', '', 'Paraguay [\'pærəɡwai] [PRY; PY]  亚松森(Asuncion [ɑ:.su:n\'sjɔ:n])     the Republic of Paraguay 巴拉圭共和国', '2015-07-28 17:35:23', '2015-07-28 17:35:23', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('174', '0', '7', '北美洲', 'beimeizhou', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '10', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('175', '174', '7', '安提瓜和巴布达', 'ANT', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('176', '174', '7', '阿鲁巴', 'ARU', '', '', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('177', '174', '7', '加拿大', 'CAN', '', 'Canada [\'kænədə] [CAN; CA] 加拿大   渥太华（Ottawa [\'ɔtəwə]）', '2015-07-28 17:02:00', '2015-07-28 17:02:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('178', '174', '7', '美国', 'USA', '', 'United States\\America  [ə\'merikə] [USA; US] the United States of America美利坚合众国（美国）     华盛顿哥伦比亚特区（Washington, District of Columbia [kə\'lʌmbiə] / Washington D. C.）', '2015-07-28 17:02:21', '2015-07-28 17:02:21', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('179', '174', '7', '墨西哥', 'MEX', '', 'Mexico [\'meksɪkəʊ] [MEX; MX]   墨西哥城 (Mexico City)      the United Mexican States [\'meksɪkən] (The United States of Mexico)墨西哥合众国', '2015-07-28 17:02:55', '2015-07-28 17:02:43', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('180', '174', '7', '危地马拉', 'GTM', '', 'Guatemala [͵ɡwæti\'mɑ:lə] [GTM; GT]  危地马拉城（Guatemala City）     the Republic of Guatemala 危地马拉共和国', '2015-07-28 17:03:42', '2015-07-28 17:03:42', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('181', '174', '7', '伯利兹', 'BLZ', '', 'Belize [be\'li:z] [BLZ; BZ] 伯利兹   贝尔莫潘（Belmopan [\'belməlpæn] ', '2015-07-28 17:04:02', '2015-07-28 17:04:02', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('182', '174', '7', '萨尔瓦多', 'SLV', '', 'El Salvador [el\'sælvədɔ:] [SLV; SV]  圣萨尔瓦多（San Salvador [sæn\'sælvədɔ:]）     the Republic of El Salvador 萨尔瓦多共和国', '2015-07-28 17:04:16', '2015-07-28 17:04:16', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('183', '174', '7', '洪都拉斯', 'HND', '', 'Honduras [hɔn\'djuərəs] [HND; HN]  特古巴加尔巴（Tegucigalpa [tə͵ɡu:si\'ɡælpə]）     the Republic of Honduras 洪都拉斯共和国', '2015-07-28 17:04:31', '2015-07-28 17:04:31', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('184', '174', '7', '尼加拉瓜', 'NIC', '', 'Nicaragua [͵nikə\'rɑ:ɡwə,͵nikə\'ræɡjuə] [NIC; NI]  马那瓜 (Managua [mə\'nɑ:ɡwə])     the Republic of Nicaragua 尼加拉瓜共和国', '2015-07-28 17:04:45', '2015-07-28 17:04:45', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('185', '174', '7', '哥斯达黎加', 'CRI', '', 'Costa Rica [\'kɔstə\'ri:kə] [CRI; CR]  圣何塞 (San Jose [sɑ:n həu\'zei])     the Republic of Costa Rica 哥斯达黎加共和国 ', '2015-07-28 17:05:03', '2015-07-28 17:05:03', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('186', '174', '7', '巴拿马', 'PAN', '', 'Panama [͵pænə\'mɑ:] [PAN; PN]  巴拿马城（Panama city）     the Republic of Panama 巴拿马共和国', '2015-07-28 17:05:16', '2015-07-28 17:05:16', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('187', '174', '7', '巴哈马', 'BHS', '', 'Bahamas [bə\'hɑ:məz] [BHS; BS]  拿骚 （Nassau [\'næsɔ:]）     the Commonwealth of the Bahamas 巴哈马联邦', '2015-07-28 17:28:22', '2015-07-28 17:28:22', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('188', '174', '7', '古巴', 'CUB', '', 'Cuba [\'kju:bə] [CUB; CU]  哈瓦那（Havana [hə\'vænə]）     the Republic of Cuba 古巴共和国', '2015-07-28 17:28:36', '2015-07-28 17:28:36', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('189', '174', '7', '牙买加', 'JAM', '', 'Jamaica [dӡə\'meikə] [JAM; JM] 牙买加   金斯敦（Kingston [\'kiŋstən]）', '2015-07-28 17:28:51', '2015-07-28 17:28:51', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('190', '174', '7', '海地', 'HTI', '', 'Haiti [\'heiti] [HTI; HT]  太子港（Port-au-Prince [͵pɔ:təu\'prins]）     the Republic of Haiti 海地共和国', '2015-07-28 17:29:10', '2015-07-28 17:29:10', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('191', '174', '7', '多米尼加', 'DOM', '', 'the Dominican Republic[DOM; DO] 多米尼加共和国', '2015-07-28 17:29:34', '2015-07-28 17:29:34', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('192', '174', '7', '安提瓜和巴布达', 'ATG', '', 'Antigua and Barbuda [æn\'ti:ɡə] [bɑ:`bu:də] [ATG; AG] 圣约翰（Saint[seint,sənt] John\'s）     安提瓜和巴布达', '2015-07-28 17:30:04', '2015-07-28 17:30:04', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('193', '174', '7', '巴斯特尔', 'KNA', '', 'Saint Kitts and Nevis [seint] [`ni:vɪs，`ne-] （原称圣克里斯托弗和尼维斯St. Christopher [\'kristəfə]  and Nevis仍通用）] the Federation of Saint Kitts and Nevis圣基茨和尼维斯联邦     [KNA; KN]  巴斯特尔 (Basseterre [bɑ:s`teə(r)])', '2015-07-28 17:30:26', '2015-07-28 17:30:26', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('194', '174', '7', '多米尼克', 'DMA', '', 'Dominica [͵dɔmi\'ni:kə] [DMA; DM]  罗索（Roseau [rəʊ`zəʊ]）  the Commonwealth of Dominica 多米尼克国（曾译“多米尼加联邦”）', '2015-07-28 17:30:43', '2015-07-28 17:30:43', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('195', '174', '7', '圣卢西亚', 'LCA', '', 'Saint Lucia [\'lu:ʃə, lu: \'si:ə] [LCA; LC] 圣卢西亚  卡斯特里 (Castries [kæ`stri:,`kɑ:strɪs])', '2015-07-28 17:31:03', '2015-07-28 17:31:03', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('196', '174', '7', '金斯敦', 'VCT', '', 'Saint Vincent[\'vinsənt] and the Grenadines [\'ɡrenədi:ns] [VCT; VC] 圣文森特和格林纳丁斯     金斯敦（Kingstown [`kɪŋstaʊn]）', '2015-07-28 17:31:23', '2015-07-28 17:31:23', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('197', '174', '7', '布里奇顿', 'BRB', '', 'Barbados [bɑ:\'beidəuz] [BRB; BB] 巴巴多斯    布里奇顿（Bridgetown [\'bridӡtaun]） ', '2015-07-28 17:31:43', '2015-07-28 17:31:43', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('198', '174', '7', '特立尼达和多巴哥', 'TTO', '', 'Trinidad and Tobago [\'trɪnɪdæd ənd tə\'beɪɡəʊ] [TTO; TT]  西班牙港（Port of Spain）     the Republic of Trinidad and Tobago 特立尼达和多巴哥共和国', '2015-07-28 17:32:19', '2015-07-28 17:32:19', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('199', '174', '7', '格林纳达', 'GRD', '', 'Grenada [ɡrə\'neidə] [GRD; GD]  圣乔治（St. George\'s）格林纳达', '2015-07-28 17:32:37', '2015-07-28 17:32:37', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('200', '0', '9', '当前栏目', '1', '', '', '2015-09-03 18:06:10', '2015-08-07 09:56:03', '247', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('201', '0', '9', '不调用', '0', '', '', '2015-09-03 18:06:18', '2015-08-07 09:56:11', '232', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('202', '0', '9', '当前栏目及子栏目', '2', '', '', '2015-09-03 18:07:06', '2015-08-07 09:57:36', '198', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('203', '0', '9', '所有子孙栏目', '3', '', '', '2015-09-03 18:07:13', '2015-08-09 21:57:38', '165', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('204', '0', '9', '所有栏目', '4', '', '', '2015-09-03 18:07:10', '2015-08-13 21:24:08', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('205', '0', '10', '首页', 'Home/Index/index', '', '', '2015-09-03 18:05:14', '0000-00-00 00:00:00', '249', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('206', '0', '10', '新闻', 'Home/Article/index', '', '', '2015-09-03 18:05:21', '0000-00-00 00:00:00', '225', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('207', '206', '10', '栏目', 'Home/Article/category?id=栏目ID', '', '', '2015-08-10 10:15:12', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('208', '0', '10', '产品', 'Home/Product/index', '', '', '2015-09-03 18:05:28', '0000-00-00 00:00:00', '217', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('209', '208', '10', '栏目', 'Home/Product/category?id=栏目ID', '', '', '2015-08-10 10:15:16', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('210', '0', '10', '招聘', 'Home/Job/index', '', '', '2015-09-03 18:05:33', '0000-00-00 00:00:00', '188', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('211', '210', '10', '栏目', 'Home/Job/category?id=栏目ID', '', '', '2015-08-10 10:15:36', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('212', '0', '10', '下载', 'Home/Download/index', '', '', '2015-09-03 18:05:37', '0000-00-00 00:00:00', '184', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('213', '212', '10', '栏目', 'Home/Download/category?id=栏目ID', '', '', '2015-08-10 10:15:22', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('214', '0', '10', '文章', 'Home/Article/index?mark=分类标识', '', '', '2015-11-03 13:14:11', '0000-00-00 00:00:00', '7', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('215', '0', '10', '留言', 'Home/Guestbook/index', '', '', '2015-09-03 18:05:48', '0000-00-00 00:00:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('217', '0', '12', '代理检测', 'Behavior\\\\AgentCheckBehavior', '{\"configmodel\":\"3\"}', 'LIMIT_PROXY_VISIT 是否启用代理屏蔽', '2015-11-16 19:37:12', '2015-10-08 16:40:29', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('218', '0', '12', 'Boris行为扩展', 'Behavior\\\\BorisBehavior', '', '', '2015-10-08 16:41:10', '2015-10-08 16:41:10', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('219', '0', '12', '浏览器防刷新检测', 'Behavior\\\\BrowserCheckBehavior', '', '一般有验证码之类的页面请不要启用', '2015-10-12 11:04:33', '2015-10-08 16:43:44', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('221', '0', '12', '操作路由检测', 'Behavior\\\\CheckActionRouteBehavior', '', '', '2015-10-08 16:44:46', '2015-10-08 16:44:46', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('222', '0', '12', '语言检测', 'Behavior\\\\CheckLangBehavior', '', '', '2015-10-08 16:45:10', '2015-10-08 16:45:10', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('223', '0', '12', 'Chrome页面Trace显示输出', 'Behavior\\\\ChromeShowPageTraceBehavior', '', '', '2015-10-08 16:45:48', '2015-10-08 16:45:48', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('225', '0', '12', '自动执行任务', 'Common\\\\Behavior\\\\CronRunBehavior', '', '', '2015-10-12 10:12:18', '2015-10-08 16:46:30', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('226', '0', '12', '火狐页面Trace显示输出', 'Behavior\\\\FireShowPageTraceBehavior', '', '', '2015-10-08 16:47:08', '2015-10-08 16:47:08', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('229', '0', '12', '机器人检测', 'Behavior\\\\RobotCheckBehavior', '', '', '2015-10-08 16:49:45', '2015-10-08 16:47:56', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('231', '0', '12', '运行时间信息显示', 'Behavior\\\\ShowRuntimeBehavior', '', '', '2015-10-08 16:49:51', '2015-10-08 16:48:27', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('232', '0', '12', '表单令牌生成', 'Behavior\\\\TokenBuildBehavior', '', '', '2015-10-08 16:49:55', '2015-10-08 16:48:41', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('233', '0', '12', 'ThinkPHP升级短信通知', 'Behavior\\\\UpgradeNoticeBehavior', '', '', '2015-10-08 16:49:59', '2015-10-08 16:49:07', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('235', '0', '11', '应用初始化', 'app_init', '', '', '2015-10-08 17:06:37', '2015-10-08 16:51:26', '250', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('236', '0', '11', '模块检测', 'module_check', '', '', '2015-10-08 17:06:49', '2015-10-08 16:52:28', '224', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('237', '0', '11', '检测路由规则', 'path_info', '', '', '2015-10-08 17:06:54', '2015-10-08 16:53:14', '216', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('238', '0', '11', '应用开始标签', 'app_begin', '', '', '2015-10-08 17:07:02', '2015-10-08 16:53:54', '200', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('239', '0', '11', '动作开始标签', 'action_begin', '', '', '2015-10-08 17:07:14', '2015-10-08 16:54:35', '194', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('240', '0', '11', '视图开始标签', 'view_begin', '', '', '2015-10-08 17:07:21', '2015-10-08 16:55:39', '185', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('241', '0', '11', '模版编译过滤标签', 'template_filter', '', '', '2015-10-08 17:07:26', '2015-10-08 16:58:34', '175', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('242', '0', '11', '视图解析标签', 'view_parse', '', '', '2015-10-08 17:07:45', '2015-10-08 16:59:04', '162', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('243', '0', '11', '内容过滤标签', 'view_filter', '', '', '2015-10-08 17:07:51', '2015-10-08 16:59:19', '154', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('244', '0', '11', 'ajax返回标签', 'ajax_return', '', '', '2015-10-08 17:08:10', '2015-10-08 16:59:53', '136', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('245', '0', '11', '执行后续操作', 'action_end', '', '', '2015-10-08 17:08:45', '2015-10-08 17:00:54', '126', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('246', '0', '11', '应用结束标签', 'app_end', '', '', '2015-10-08 17:06:25', '2015-10-08 17:01:21', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('247', '0', '11', 'URL调度结束标签', 'url_dispatch', '', '', '2015-10-08 17:06:45', '2015-10-08 17:04:02', '239', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('248', '0', '11', '视图结束标签', 'view_end', '', '', '2015-10-08 17:08:01', '2015-10-08 17:04:38', '145', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('269', '0', '13', '清理备份安全文件', 'clearbackuplockfile', '{\"model\":\"6\"}', '清理一天之前的备份安全文件', '2015-11-17 17:02:02', '2015-10-08 22:15:15', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('251', '0', '14', '1分钟', '60', '', '', '2015-10-08 21:09:47', '2015-10-08 21:08:29', '50', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('252', '0', '14', '5分钟', '300', '', '', '2015-10-08 21:10:07', '2015-10-08 21:09:38', '49', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('253', '0', '14', '10分钟', '600', '', '', '2015-10-08 21:09:58', '2015-10-08 21:09:58', '48', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('254', '0', '14', '15分钟', '900', '', '', '2015-10-08 21:10:16', '2015-10-08 21:10:16', '47', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('255', '0', '14', '30分钟', '1800', '', '', '2015-10-08 21:10:26', '2015-10-08 21:10:26', '46', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('256', '0', '14', '1小时', '3600', '', '', '2015-10-08 21:10:35', '2015-10-08 21:10:35', '45', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('257', '0', '14', '2小时', '7200', '', '', '2015-10-08 21:10:45', '2015-10-08 21:10:45', '44', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('258', '0', '14', '3小时', '10800', '', '', '2015-10-08 21:11:05', '2015-10-08 21:11:05', '43', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('259', '0', '14', '4小时', '14400', '', '', '2015-10-08 21:11:24', '2015-10-08 21:11:24', '42', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('260', '0', '14', '5小时', '18000', '', '', '2015-10-08 21:11:43', '2015-10-08 21:11:43', '41', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('261', '0', '14', '6小时', '21600', '', '', '2015-10-08 21:12:06', '2015-10-08 21:12:06', '40', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('262', '0', '14', '8小时', '28800', '', '', '2015-10-08 21:12:38', '2015-10-08 21:12:38', '39', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('263', '0', '14', '10小时', '36000', '', '', '2015-10-08 21:12:54', '2015-10-08 21:12:54', '38', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('264', '0', '14', '12小时', '39600', '', '', '2015-10-08 21:13:20', '2015-10-08 21:13:20', '37', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('265', '0', '14', '1天', '86400', '', '', '2015-10-08 21:14:09', '2015-10-08 21:14:09', '36', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('266', '0', '14', '2天', '172800', '', '', '2015-10-08 21:14:33', '2015-10-08 21:14:33', '35', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('267', '0', '14', '3天', '259200', '', '', '2015-10-08 21:15:02', '2015-10-08 21:15:02', '34', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('268', '0', '14', '5天', '432000', '', '', '2015-10-08 21:15:34', '2015-10-08 21:15:34', '33', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('270', '0', '11', '后台权限认证之后', 'auth_after', '', 'sadfasdfsadf', '2015-10-12 10:37:30', '2015-10-09 12:06:16', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('290', '0', '6', '枚举', 'item', '', '一样一个，例如\r\nhaha\r\nhehe\r\nxixi', '2015-10-20 15:24:12', '2015-10-20 15:23:41', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('276', '0', '17', 'mysql', 'mysql', '', '', '2015-11-16 11:40:00', '2015-10-20 14:30:38', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('274', '0', '15', '是', '1', '', '', '2015-10-15 14:32:34', '2015-10-15 14:28:35', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('275', '0', '16', '对不起，你的配置可能错误', '0', '', '', '2015-10-15 14:40:51', '2015-10-15 14:40:51', '1', '1', '1');
INSERT INTO `ebcms_datadict` VALUES ('292', '0', '12', '记录点击量', 'Home\\\\Behavior\\\\ClickBehavior', '', '', '2015-11-01 16:38:45', '2015-11-01 16:38:45', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('293', '214', '10', '内容', 'Home/Article/detail?id=文章id', '', '', '2015-11-17 11:10:42', '2015-11-03 13:14:22', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('297', '0', '20', '添加', 'add', '', '', '2015-12-03 16:37:15', '2015-12-03 16:37:15', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('298', '0', '20', '修改', 'save', '', '', '2015-12-03 16:37:20', '2015-12-03 16:37:20', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('299', '0', '21', '单行文本', 'textbox', '', '', '2015-12-06 21:46:19', '2015-12-04 16:10:54', '241', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('300', '0', '21', '编辑器', 'ueditor', '', '', '2015-12-06 21:47:00', '2015-12-04 16:11:31', '217', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('301', '0', '21', '布尔选择', 'bool', '', '', '2015-12-06 21:48:11', '2015-12-04 16:11:50', '212', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('302', '0', '21', '数字框', 'numberbox', '', '', '2015-12-06 21:47:58', '2015-12-04 16:12:15', '215', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('303', '0', '21', '下拉选项', 'combotree', '', '', '2015-12-06 21:48:29', '2015-12-04 16:12:31', '211', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('304', '0', '21', '图片', 'image', '', '', '2015-12-06 21:47:39', '2015-12-04 16:13:32', '210', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('305', '0', '21', '多图', 'images', '', '', '2015-12-06 21:47:31', '2015-12-04 16:13:47', '209', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('306', '0', '21', '附件', 'file', '', '', '2015-12-06 21:47:10', '2015-12-04 16:13:54', '208', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('307', '0', '21', '多附件', 'files', '', '', '2015-12-06 21:47:17', '2015-12-04 16:14:05', '207', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('308', '0', '21', '日期', 'datebox', '', '', '2015-12-06 21:48:55', '2015-12-04 16:14:25', '159', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('309', '0', '21', '日期时间', 'datetimebox', '', '', '2015-12-06 21:49:02', '2015-12-04 16:14:35', '147', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('310', '0', '21', '时间', 'timespinner', '', '', '2015-12-06 21:49:07', '2015-12-04 16:15:09', '138', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('311', '0', '21', '隐藏域', 'hidden', '', '', '2015-12-04 16:15:38', '2015-12-04 16:15:38', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('313', '0', '21', '多行文本', 'multitextbox', '', '', '2015-12-06 21:46:25', '2015-12-04 21:45:55', '231', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('314', '0', '21', '数据字典', 'datadict', '', '', '2015-12-06 21:49:17', '2015-12-05 14:23:24', '167', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('315', '0', '22', '超链接', 'url', '', '', '2015-12-05 16:16:36', '2015-12-05 16:16:36', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('316', '0', '22', '电子邮箱', 'email', '{\"test\":\"332\"}', '', '2015-12-08 15:29:57', '2015-12-05 16:17:00', '1', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('318', '0', '21', '单选', 'radio', '', '', '2015-12-07 20:22:18', '2015-12-07 20:22:18', '199', '1', '0');
INSERT INTO `ebcms_datadict` VALUES ('319', '0', '21', '复选', 'checkbox', '', '', '2015-12-07 20:46:38', '2015-12-07 20:46:38', '200', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_datadictcate`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_datadictcate`;
CREATE TABLE `ebcms_datadictcate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `text` varchar(20) NOT NULL DEFAULT '' COMMENT '标题',
  `field` varchar(20) NOT NULL DEFAULT '' COMMENT '字段',
  `model_id` varchar(255) NOT NULL DEFAULT '' COMMENT '模型',
  `iconcls` varchar(20) NOT NULL DEFAULT '' COMMENT '图标类',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='数据字典分类表';

-- ----------------------------
-- Records of ebcms_datadictcate
-- ----------------------------
INSERT INTO `ebcms_datadictcate` VALUES ('2', '核心字典', '微信菜单类型', 'wx_menu_type', '6', '', '', '2015-12-26 21:25:29', '2015-09-09 15:27:08', '1', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('3', '核心字典', '菜单分类', 'menutype', '10', '未填写', '未填写', '2015-12-07 22:00:09', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_datadictcate` VALUES ('4', '核心字典', '附件分类', 'attachtype', '', '未填写', '', '2015-12-26 21:25:26', '2015-07-25 17:04:49', '44', '1', '1');
INSERT INTO `ebcms_datadictcate` VALUES ('5', '核心字典', '模板分类', 'tpltype', '', '', '', '2015-12-26 21:25:23', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_datadictcate` VALUES ('6', '核心字典', '配置解析类型', 'configtype', '', '', '', '2015-12-07 22:00:05', '2015-07-25 17:04:49', '99', '1', '1');
INSERT INTO `ebcms_datadictcate` VALUES ('7', '核心字典', '地区信息', 'area', '4', '', '', '2016-01-06 22:24:04', '2015-07-25 17:04:49', '1', '1', '1');
INSERT INTO `ebcms_datadictcate` VALUES ('9', '核心字典', '内容调用方式', 'recursion', '', '', '', '2015-12-26 21:25:17', '2015-08-07 09:55:54', '1', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('10', '核心字典', '导航实例', 'navexample', '', '', '', '2015-12-26 21:25:13', '2015-08-07 09:53:20', '1', '1', '1');
INSERT INTO `ebcms_datadictcate` VALUES ('11', '核心字典', '系统钩子', 'hook', '', '', '', '2015-12-07 22:00:01', '2015-10-08 16:38:08', '55', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('12', '核心字典', '系统行为', 'behavior', '', '', '', '2016-01-01 16:55:27', '2015-10-08 16:38:25', '66', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('13', '核心字典', '定时任务', 'cron', '4', '', '', '2015-12-07 21:59:54', '2015-10-08 17:12:41', '77', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('14', '核心字典', '秒', 'second', '', '', '', '2015-12-26 21:25:10', '2015-10-08 21:05:43', '1', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('15', '核心字典', '布尔选项', 'bool', '', '', '', '2015-12-26 21:25:07', '2015-10-15 14:28:05', '1', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('16', '核心字典', '错误', 'error', '', '', '系统个项目，请不要删除', '2015-12-07 21:59:50', '2015-10-15 14:40:13', '1', '1', '1');
INSERT INTO `ebcms_datadictcate` VALUES ('17', '核心字典', '数据类型', 'db_type', '', '', '', '2015-12-26 21:25:04', '2015-10-20 14:30:10', '1', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('20', '核心字典', '表单模板', 'formtpl', '0', '', '', '2015-12-08 21:11:58', '2015-12-03 16:37:05', '1', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('21', '核心字典', '表单字段类型', 'formfieldtype', '0', '', '', '2015-12-07 21:59:42', '2015-12-04 16:09:46', '88', '1', '0');
INSERT INTO `ebcms_datadictcate` VALUES ('22', '核心字典', '表单认证类型', 'formvalidtype', '1', '', '', '2015-12-08 15:19:41', '2015-12-05 16:16:19', '1', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_form`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_form`;
CREATE TABLE `ebcms_form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `sys_mark` varchar(255) NOT NULL DEFAULT '' COMMENT '系统标致',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='表单表';

-- ----------------------------
-- Records of ebcms_form
-- ----------------------------
INSERT INTO `ebcms_form` VALUES ('19', '表单配置', '单行文本', 'form_textbox', '', '101', '2015-12-08 22:20:22', '2015-12-04 17:17:51', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('20', '表单配置', '布尔选项', 'form_bool', '', '56', '2015-12-05 16:31:48', '2015-12-04 21:20:51', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('21', '表单配置', '隐藏域', 'form_hidden', '', '1', '2015-12-04 21:22:46', '2015-12-04 21:22:46', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('22', '表单配置', '百度编辑器', 'form_ueditor', '', '86', '2015-12-05 16:30:18', '2015-12-04 21:24:08', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('23', '表单配置', '数字框', 'form_numberbox', '', '91', '2015-12-25 21:11:34', '2015-12-04 21:25:04', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('24', '表单配置', '图片', 'form_image', '', '69', '2015-12-05 16:30:38', '2015-12-04 21:25:44', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('25', '表单配置', '多图上传', 'form_images', '', '66', '2015-12-05 16:30:46', '2015-12-04 21:26:40', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('26', '表单配置', '附件', 'form_file', '', '68', '2015-12-05 16:30:52', '2015-12-04 21:27:19', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('27', '表单配置', '多附件', 'form_files', '', '62', '2015-12-05 16:30:56', '2015-12-04 21:28:05', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('28', '表单配置', '日期', 'form_datebox', '', '33', '2015-12-05 16:31:17', '2015-12-04 21:28:52', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('29', '表单配置', '时间', 'form_time', '', '31', '2016-01-10 21:48:32', '2015-12-04 21:29:33', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('30', '表单配置', '时间日期', 'form_datetimebox', '', '32', '2015-12-05 17:37:39', '2015-12-04 21:30:09', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('32', '表单配置', '下拉选项', 'form_combotree', '', '45', '2015-12-05 16:32:18', '2015-12-04 21:31:44', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('33', '表单配置', '多行文本', 'form_multitextbox', '', '97', '2015-12-05 16:29:37', '2015-12-04 21:45:11', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('34', '表单配置', '数据字典', 'form_datadict', '', '47', '2015-12-05 16:31:59', '2015-12-05 14:23:59', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('47', '模板管理', '添加', 'Admin_Template_add', '', '121', '2015-12-06 16:15:24', '2015-12-06 16:15:16', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('48', '模板管理', '修改', 'Admin_Template_save', '', '1', '2015-12-06 16:16:00', '2015-12-06 16:16:00', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('49', '附件管理', '编辑', 'Admin_Attachment_save', '', '124', '2015-12-06 19:57:59', '2015-12-06 19:57:59', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('50', '用户管理', '添加角色', 'Admin_Group_add', '', '66', '2016-01-10 17:06:25', '2015-12-06 20:16:37', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('51', '用户管理', '修改角色', 'Admin_Group_save', '', '56', '2016-01-10 17:06:31', '2015-12-06 20:17:10', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('52', '用户管理', '添加用户', 'Admin_User_add', '', '136', '2016-01-10 17:06:07', '2015-12-06 20:31:35', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('53', '用户管理', '修改用户', 'Admin_User_save', '', '75', '2016-01-10 17:06:18', '2015-12-06 20:32:14', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('54', '模型管理', '添加模型', 'Admin_Model_add', '', '141', '2015-12-06 20:56:30', '2015-12-06 20:56:16', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('55', '模型管理', '修改模型', 'Admin_Model_save', '', '71', '2015-12-08 15:44:48', '2015-12-06 20:57:12', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('56', '模型管理', '添加字段', 'Admin_Modelfield_add', '', '60', '2015-12-08 15:44:56', '2015-12-06 20:57:45', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('57', '模型管理', '修改字段', 'Admin_Modelfield_save', '', '1', '2015-12-06 20:58:09', '2015-12-06 20:58:09', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('64', '表单配置', '单选', 'form_radio', '', '72', '2015-12-08 15:45:13', '2015-12-07 20:21:27', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('65', '表单配置', '复选', 'form_checkbox', '', '71', '2015-12-08 15:45:20', '2015-12-07 20:44:19', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('70', '定时任务', '添加', 'Admin_Cron_add', '', '125', '2015-12-08 21:26:10', '2015-12-08 21:26:10', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('71', '定时任务', '修改', 'Admin_Cron_save', '', '1', '2015-12-08 21:26:44', '2015-12-08 21:26:44', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('76', '内容管理', '添加分类', 'Admin_Articlecate_add', '', '160', '2015-12-09 16:01:32', '2015-12-09 16:00:31', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('77', '内容管理', '修改分类', 'Admin_Articlecate_save', '', '50', '2015-12-09 16:01:48', '2015-12-09 16:01:48', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('78', '内容管理', '添加内容', 'Admin_Article_add', '', '30', '2015-12-09 16:02:14', '2015-12-09 16:02:14', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('79', '内容管理', '修改内容', 'Admin_Article_save', '', '20', '2015-12-09 16:02:30', '2015-12-09 16:02:30', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('80', '导航管理', '添加导航', 'Admin_Nav_add', '', '155', '2015-12-09 17:05:04', '2015-12-09 17:03:45', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('81', '导航管理', '修改导航', 'Admin_Nav_save', '', '50', '2015-12-09 17:05:13', '2015-12-09 17:04:21', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('82', '导航管理', '添加导航分类', 'Admin_Navcate_add', '', '30', '2015-12-09 17:04:54', '2015-12-09 17:04:54', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('83', '导航管理', '修改导航分类', 'Admin_Navcate_save', '', '20', '2015-12-09 17:05:33', '2015-12-09 17:05:33', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('84', '友情链接', '添加友情链接', 'Admin_Link_add', '', '154', '2015-12-10 14:07:45', '2015-12-10 14:07:45', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('85', '友情链接', '修改友情链接', 'Admin_Link_save', '', '1', '2015-12-10 14:08:12', '2015-12-10 14:08:12', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('86', '留言中心', '修改留言', 'Admin_Guestbook_save', '', '153', '2015-12-10 14:23:11', '2015-12-10 14:23:11', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('87', '留言中心', '回复留言', 'Admin_Guestbook_reply', '', '50', '2015-12-10 14:24:14', '2015-12-10 14:23:59', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('88', '推荐管理', '添加推荐位', 'Admin_Recommendcate_add', '', '152', '2015-12-10 14:36:46', '2015-12-10 14:36:46', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('89', '推荐管理', '修改推荐位', 'Admin_Recommendcate_save', '', '45', '2015-12-10 14:37:32', '2015-12-10 14:37:32', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('90', '推荐管理', '添加推荐内容', 'Admin_Recommend_add', '', '40', '2015-12-10 14:38:05', '2015-12-10 14:38:05', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('91', '推荐管理', '修改推荐内容', 'Admin_Recommend_save', '', '35', '2015-12-10 14:38:24', '2015-12-10 14:38:24', '1', '0', '');
INSERT INTO `ebcms_form` VALUES ('98', '基本表单', '修改密码', 'Admin_Index_password', '', '102', '2016-01-17 20:14:56', '2016-01-17 20:14:56', '1', '0', '');

-- ----------------------------
-- Table structure for `ebcms_formfield`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_formfield`;
CREATE TABLE `ebcms_formfield` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类',
  `group` varchar(250) NOT NULL DEFAULT '' COMMENT '分组',
  `text` varchar(250) NOT NULL DEFAULT '' COMMENT '标题',
  `subtable` varchar(250) NOT NULL DEFAULT '' COMMENT '副表',
  `extfield` varchar(250) NOT NULL DEFAULT '' COMMENT '扩展字段',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '键',
  `value` text COMMENT '默认值',
  `defaultvaluetype` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '取值类型',
  `defaultvalue` text COMMENT '默认值',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '表单类型',
  `config` text COMMENT '表单配置',
  `instruction` text COMMENT '表单说明',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  `sys_mark` varchar(255) NOT NULL DEFAULT '' COMMENT '系统标致',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=577 DEFAULT CHARSET=utf8 COMMENT='表单字段表';

-- ----------------------------
-- Records of ebcms_formfield
-- ----------------------------
INSERT INTO `ebcms_formfield` VALUES ('10', '11', '阿斯顿发', '附件', '', '', 'attachment', '', '0', '', 'form_files', '', '', '1', '2015-12-03 16:15:40', '2015-11-16 19:59:02', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('72', '31', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '50', '2015-12-09 14:53:25', '2015-12-05 15:26:07', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('26', '19', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-04 21:17:01', '2015-12-04 21:11:27', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('71', '31', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:25:35', '2015-12-05 15:25:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('27', '19', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '45', '2015-12-09 15:26:50', '2015-12-04 21:18:59', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('32', '33', '基本配置', '宽度', '', 'config', 'width', '', '3', 'width', 'form_numberbox', '', '不填写表示 100% ', '45', '2015-12-09 15:25:48', '2015-12-04 21:58:15', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('29', '33', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '48', '2015-12-09 15:24:35', '2015-12-04 21:47:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('30', '33', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-04 21:48:29', '2015-12-04 21:48:29', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('31', '33', '基本配置', '高度', '', 'config', 'height', '', '3', 'height', 'form_numberbox', '', '最好不要超过200 默认120', '46', '2015-12-28 14:54:42', '2015-12-04 21:50:40', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('39', '19', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '40', '2015-12-28 14:42:25', '2015-12-05 13:57:08', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('34', '33', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '{\"width\":\"\",\"prompt\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"required\":\"0\"}', '', '15', '2015-12-09 15:25:24', '2015-12-04 22:06:51', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('36', '33', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '47', '2015-12-28 14:54:01', '2015-12-04 22:14:40', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('41', '19', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '{\"width\":\"\",\"prompt\":\"\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"required\":\"0\"}', '', '5', '2015-12-09 15:26:42', '2015-12-05 13:58:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('42', '33', '基本配置', '最少字符数', '', 'config', 'minlength', '', '3', 'minlength', 'form_numberbox', '', '默认0', '41', '2015-12-09 15:24:59', '2015-12-05 14:18:19', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('43', '33', '基本配置', '最大字符数', '', 'config', 'maxlength', '', '3', 'maxlength', 'form_numberbox', '', '默认999999', '42', '2015-12-09 15:24:53', '2015-12-05 14:18:57', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('45', '34', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 14:24:41', '2015-12-05 14:24:41', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('47', '34', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '45', '2015-12-09 15:14:21', '2015-12-05 14:26:16', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('48', '34', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '10', '2015-12-09 15:15:00', '2015-12-05 14:26:43', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('53', '34', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '12', '2015-12-28 14:28:21', '2015-12-05 14:31:04', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('55', '34', '基本配置', '数据来源', '', 'config', 'datadict', '', '3', 'datadict', 'form_textbox', '', '', '50', '2015-12-09 15:14:15', '2015-12-05 14:33:26', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('56', '32', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-06 17:14:56', '2015-12-05 14:44:56', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('57', '32', '基本配置', '模型名称', '', 'config', 'model', '', '3', 'model', 'form_textbox', '', '', '50', '2015-12-26 16:15:32', '2015-12-05 14:46:49', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('58', '32', '其他配置', '宽度', '', 'config', 'width', '', '3', 'width', 'form_numberbox', '', '', '1', '2015-12-06 17:15:26', '2015-12-05 14:56:26', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('60', '32', '基本配置', '必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '45', '2015-12-28 12:04:45', '2015-12-05 14:57:14', '0', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('61', '32', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '10', '2015-12-09 15:12:46', '2015-12-05 14:57:38', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('63', '32', '基本配置', '只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '43', '2015-12-28 12:04:07', '2015-12-05 14:58:36', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('567', '32', '基本配置', '树型化', '', 'config', 'tree', '', '3', 'tree', 'form_bool', '', '', '35', '2015-12-28 11:50:32', '2015-12-28 11:45:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('68', '32', '其他配置', '递归字段', '', 'config', 'pid', '', '3', 'pid', 'form_textbox', '', '通常请不要修改 默认pid', '30', '2015-12-09 15:13:13', '2015-12-05 15:04:00', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('69', '32', '基本配置', '根选项', '', 'config', 'rootitem', '', '3', 'rootitem', 'form_bool', '', '根选项开启后 所有条目都会在根选项之下，通常用于选择父级 默认0', '40', '2015-12-09 15:08:54', '2015-12-05 15:05:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('73', '31', '基本配置', '提示信息', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '35', '2015-12-09 14:54:21', '2015-12-05 15:26:45', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('74', '31', '基本配置', '是否只读', '', 'config', 'readonly', '', '3', 'readonly', 'form_bool', '', '', '30', '2015-12-09 14:54:09', '2015-12-05 15:27:06', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('75', '31', '基本配置', '是否失效', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '10', '2015-12-09 14:54:32', '2015-12-05 15:27:46', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('76', '31', '基本配置', '是否可编辑', '', 'config', 'editable', '', '3', 'editable', 'form_bool', '', '', '40', '2015-12-09 14:54:01', '2015-12-05 15:28:11', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('77', '30', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:33:59', '2015-12-05 15:33:59', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('78', '29', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:34:21', '2015-12-05 15:34:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('79', '28', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:34:35', '2015-12-05 15:34:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('80', '27', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:34:52', '2015-12-05 15:34:52', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('81', '26', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:35:02', '2015-12-05 15:35:02', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('82', '25', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:35:19', '2015-12-05 15:35:19', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('83', '24', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:35:30', '2015-12-05 15:35:30', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('84', '23', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:35:42', '2015-12-05 15:35:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('85', '22', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:35:56', '2015-12-05 15:35:56', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('86', '21', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:36:13', '2015-12-05 15:36:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('87', '20', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-05 15:36:25', '2015-12-05 15:36:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('88', '19', '基本配置', '认证类型', '', 'config', 'validtype', '', '3', 'validtype', 'form_datadict', '{\"datadict\":\"formvalidtype\",\"required\":\"0\",\"rootitem\":\"1\",\"disabled\":\"0\",\"prompt\":\"\"}', '', '30', '2015-12-28 14:29:13', '2015-12-05 16:14:22', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('89', '34', '基本配置', '根选项', '', 'config', 'rootitem', '', '3', 'rootitem', 'form_bool', '', '', '40', '2015-12-09 15:14:32', '2015-12-05 16:23:46', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('90', '23', '基本配置', '最小值', '', 'config', 'min', '', '3', 'min', 'form_numberbox', '', '', '25', '2015-12-09 15:21:51', '2015-12-05 16:36:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('91', '23', '基本配置', '最大值', '', 'config', 'max', '', '3', 'max', 'form_numberbox', '', '', '30', '2015-12-09 15:21:44', '2015-12-05 16:36:41', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('97', '23', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '45', '2015-12-09 15:21:06', '2015-12-05 16:40:43', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('99', '23', '基本配置', '提示信息', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '15', '2015-12-09 15:22:14', '2015-12-05 16:41:59', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('101', '23', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '40', '2015-12-28 14:55:36', '2015-12-05 16:42:44', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('104', '22', '基本配置', '高度', '', 'config', 'initialframeheight', '', '3', 'initialframeheight', 'form_numberbox', '', '', '50', '2015-12-09 15:20:31', '2015-12-05 16:45:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('105', '22', '基本配置', '显示元素路径', '', 'config', 'elementpathenabled', '', '3', 'elementpathenabled', 'form_bool', '', '', '20', '2015-12-09 15:20:56', '2015-12-05 16:47:41', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('106', '22', '基本配置', '开启字数统计', '', 'config', 'wordcount', '', '3', 'wordcount', 'form_bool', '', '', '30', '2015-12-09 15:20:51', '2015-12-05 16:48:04', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('107', '22', '基本配置', '最大字符数', '', 'config', 'maximumwords', '', '3', 'maximumwords', 'form_numberbox', '{\"required\":\"0\",\"editable\":\"1\",\"readonly\":\"0\",\"max\":\"\",\"min\":\"\",\"prompt\":\"\",\"disabled\":\"0\",\"width\":\"\",\"prefix\":\"\",\"suffix\":\"\",\"groupseparator\":\"\",\"decimalseparator\":\"\",\"precision\":\"\"}', '', '40', '2015-12-10 15:14:07', '2015-12-05 16:48:32', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('108', '22', '基本配置', '自动高度', '', 'config', 'autoheightenabled', '', '3', 'autoheightenabled', 'form_bool', '', '', '45', '2015-12-09 15:20:37', '2015-12-05 16:49:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('112', '24', '基本配置', '允许的类型', '', 'config', 'extensions', '', '3', 'extensions', 'form_textbox', '', '多个类型用‘,’分割，例如：jpg,png,gif,bmp', '40', '2015-12-09 15:19:20', '2015-12-05 17:16:45', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('113', '24', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '50', '2015-12-09 15:19:14', '2015-12-05 17:17:20', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('114', '24', '基本配置', '提示信息', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '20', '2015-12-09 15:19:39', '2015-12-05 17:17:32', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('116', '24', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '45', '2015-12-28 14:57:08', '2015-12-05 17:18:00', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('118', '26', '基本配置', '允许的类型', '', 'config', 'extensions', '', '3', 'extensions', 'form_textbox', '', '', '35', '2015-12-09 15:18:35', '2015-12-05 17:18:30', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('119', '26', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '50', '2015-12-09 15:18:39', '2015-12-05 17:18:47', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('121', '26', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '40', '2015-12-28 14:57:51', '2015-12-05 17:19:15', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('123', '25', '基本配置', '允许的类型', '', 'config', 'extensions', '', '3', 'extensions', 'form_textbox', '', '', '40', '2015-12-09 15:17:53', '2015-12-05 17:19:50', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('124', '25', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '45', '2015-12-28 14:58:12', '2015-12-05 17:20:04', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('126', '25', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '50', '2015-12-09 15:17:37', '2015-12-05 17:20:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('127', '27', '基本配置', '允许的类型', '', 'config', 'extensions', '', '3', 'extensions', 'form_textbox', '', '', '40', '2015-12-09 15:17:13', '2015-12-05 17:21:23', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('129', '27', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '50', '2015-12-09 15:16:28', '2015-12-05 17:22:17', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('132', '27', '基本配置', '是否只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '45', '2015-12-28 14:58:45', '2015-12-05 17:22:57', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('133', '28', '基本配置', '面板宽度', '', 'config', 'panelwidth', '', '3', 'panelwidth', 'form_numberbox', '', '默认180', '15', '2015-12-09 15:08:02', '2015-12-05 17:23:43', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('134', '28', '基本配置', '面板高度', '', 'config', 'panelheight', '', '3', 'panelheight', 'form_numberbox', '', '默认自动', '10', '2015-12-09 15:07:57', '2015-12-05 17:24:04', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('138', '28', '基本配置', '是否可编辑', '', 'config', 'editable', '', '3', 'editable', 'form_bool', '', '', '40', '2015-12-09 15:07:19', '2015-12-05 17:27:06', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('139', '28', '基本配置', '是否失效', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '30', '2015-12-09 15:07:47', '2015-12-05 17:27:17', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('140', '28', '基本配置', '是否只读', '', 'config', 'readonly', '', '3', 'readonly', 'form_bool', '', '', '35', '2015-12-09 15:07:39', '2015-12-05 17:27:27', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('141', '28', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '25', '2015-12-09 15:07:52', '2015-12-05 17:30:05', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('142', '26', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '20', '2015-12-09 15:19:00', '2015-12-05 17:30:34', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('143', '25', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '20', '2015-12-09 15:18:12', '2015-12-05 17:30:52', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('144', '27', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '20', '2015-12-09 15:17:22', '2015-12-05 17:31:04', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('146', '30', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '45', '2015-12-09 15:05:28', '2015-12-05 17:32:12', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('147', '28', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '45', '2015-12-09 15:07:08', '2015-12-05 17:32:40', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('148', '30', '基本配置', '提示字符', '', 'config', 'prompt', '', '3', 'prompt', 'form_textbox', '', '', '30', '2015-12-09 15:05:59', '2015-12-05 17:33:12', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('149', '30', '基本配置', '是否只读', '', 'config', 'readonly', '', '3', 'readonly', 'form_bool', '', '', '35', '2015-12-09 15:05:50', '2015-12-05 17:33:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('150', '30', '基本配置', '是否失效', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '20', '2015-12-09 15:06:11', '2015-12-05 17:33:39', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('151', '30', '基本配置', '是否可编辑', '', 'config', 'editable', '', '3', 'editable', 'form_bool', '', '', '40', '2015-12-09 15:05:40', '2015-12-05 17:33:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('155', '30', '基本配置', '时间宽度', '', 'config', 'spinnerwidth', '', '3', 'spinnerwidth', 'form_numberbox', '', '', '5', '2015-12-09 15:06:44', '2015-12-05 17:35:57', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('156', '30', '基本配置', '显示秒', '', 'config', 'showseconds', '', '3', 'showseconds', 'form_bool', '', '', '25', '2015-12-09 15:06:19', '2015-12-05 17:36:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('157', '30', '基本配置', '时间分隔符', '', 'config', 'timeseparator', '', '3', 'timeseparator', 'form_textbox', '', '', '10', '2015-12-09 15:06:25', '2015-12-05 17:36:39', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('158', '29', '基本配置', '是否必填', '', 'config', 'required', '', '3', 'required', 'form_bool', '', '', '50', '2015-12-09 14:54:44', '2015-12-05 17:38:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('159', '29', '基本配置', '是否可编辑', '', 'config', 'editable', '', '3', 'editable', 'form_bool', '', '', '40', '2015-12-09 15:04:24', '2015-12-05 17:38:32', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('160', '29', '基本配置', '是否失效', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '10', '2015-12-09 15:04:48', '2015-12-05 17:38:49', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('161', '29', '基本配置', '是否只读', '', 'config', 'readonly', '', '3', 'readonly', 'form_bool', '', '', '30', '2015-12-09 15:04:39', '2015-12-05 17:39:03', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('162', '29', '基本配置', '分隔符', '', 'config', 'separator', '', '3', 'separator', 'form_textbox', '', '', '15', '2015-12-09 15:05:01', '2015-12-05 17:39:59', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('163', '29', '基本配置', '显示秒', '', 'config', 'showseconds', '', '3', 'showseconds', 'form_bool', '', '', '20', '2015-12-09 15:04:55', '2015-12-05 17:40:14', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('190', '32', '其他配置', '查询条件', '', 'config', 'queryparams', '', '3', 'queryparams', 'form_multitextbox', '', '', '40', '2015-12-09 15:12:57', '2015-12-05 22:28:51', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('258', '47', '基本信息', '目录', '', '', 'text', '', '3', 'text', 'form_textbox', '{\"required\":\"1\",\"disabled\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\"}', '', '40', '2016-01-27 17:19:43', '2015-12-06 16:19:28', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('259', '47', '基本信息', '文件名', '', '', 'name', '', '0', 'newfile.txt', 'form_textbox', '{\"required\":\"1\",\"disabled\":\"0\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\"}', '请填写完整的文件名 包含后缀', '30', '2016-01-27 17:19:50', '2015-12-06 16:34:48', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('260', '47', '基本信息', '路径', '', '', 'path', '', '1', 'path', 'form_hidden', '', '', '1', '2015-12-06 16:35:22', '2015-12-06 16:35:22', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('261', '47', '基本信息', '内容', '', '', 'content', '', '0', '默认值，呵呵', 'form_multitextbox', '{\"required\":\"1\",\"disabled\":\"0\",\"height\":\"20\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"0\",\"prompt\":\"\"}', '', '20', '2016-01-06 21:40:11', '2015-12-06 16:35:46', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('262', '48', '基本信息', '文件', '', '', 'text', '', '3', 'text', 'form_textbox', '{\"required\":\"1\",\"disabled\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\"}', '', '40', '2016-01-27 17:19:29', '2015-12-06 16:37:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('263', '48', '基本信息', '内容', '', '', 'content', '', '3', 'content', 'form_multitextbox', '{\"required\":\"0\",\"disabled\":\"0\",\"height\":\"20\",\"width\":\"\",\"maxlength\":\"\",\"minlength\":\"0\",\"prompt\":\"\"}', '', '30', '2016-01-06 21:40:28', '2015-12-06 16:37:43', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('264', '48', '基本信息', '文件路径', '', '', 'filename', '', '1', 'filename', 'form_hidden', '', '', '1', '2015-12-06 16:54:46', '2015-12-06 16:38:22', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('267', '49', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-06 19:58:13', '2015-12-06 19:58:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('268', '49', '基本信息', '标题', '', '', 'title', '', '3', 'title', 'form_textbox', '', '', '99', '2015-12-06 19:58:52', '2015-12-06 19:58:29', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('269', '49', '基本信息', '简介', '', '', 'instruction', '', '3', 'instruction', 'form_multitextbox', '{\"width\":\"\",\"height\":\"\",\"prompt\":\"\",\"minlength\":\"1\",\"maxlength\":\"250\",\"validtype\":\"\"}', '', '22', '2015-12-06 20:02:46', '2015-12-06 19:58:47', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('270', '51', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-06 20:17:49', '2015-12-06 20:17:49', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('271', '51', '基本信息', '父级', '', '', 'pid', '', '3', 'pid', 'form_combotree', '{\"model\":\"group\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"1\",\"prompt\":\"\",\"queryparams\":\"\",\"pid\":\"\",\"textfield\":\"title\",\"valuefield\":\"\",\"width\":\"\"}', '', '99', '2016-01-07 20:20:34', '2015-12-06 20:18:05', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('272', '51', '基本信息', '名称', '', '', 'title', '', '3', 'title', 'form_textbox', '', '', '82', '2015-12-06 20:23:43', '2015-12-06 20:19:06', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('273', '51', '基本信息', '图标', '', '', 'iconcls', '', '3', 'iconcls', 'form_textbox', '', '', '10', '2015-12-09 15:38:48', '2015-12-06 20:19:34', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('274', '50', '基本信息', '父级', '', '', 'pid', '', '1', 'pid', 'form_combotree', '{\"model\":\"group\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"1\",\"prompt\":\"\",\"queryparams\":\"\",\"pid\":\"\",\"textfield\":\"title\",\"valuefield\":\"\",\"width\":\"\"}', '', '98', '2016-01-07 20:20:50', '2015-12-06 20:20:38', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('275', '50', '基本信息', '名称', '', '', 'title', '', '0', '', 'form_textbox', '', '', '91', '2015-12-06 20:23:31', '2015-12-06 20:20:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('276', '50', '基本信息', '图标', '', '', 'iconcls', '', '0', '', 'form_textbox', '', '', '10', '2015-12-09 15:38:56', '2015-12-06 20:21:08', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('277', '52', '基本信息', '昵称', '', '', 'nickname', '', '0', '', 'form_textbox', '{\"width\":\"\",\"prompt\":\"2-6\\u4e2a\\u5b57\\u7b26\",\"maxlength\":\"6\",\"minlength\":\"2\",\"validtype\":\"\",\"required\":\"1\",\"editable\":\"1\",\"disabled\":\"0\",\"readonly\":\"0\"}', '', '97', '2015-12-06 20:40:01', '2015-12-06 20:33:01', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('278', '52', '基本信息', '密码', '', '', 'password', '', '0', '', 'form_textbox', '{\"width\":\"\",\"prompt\":\"5-10\\u4e2a\\u5b57\\u7b26\",\"maxlength\":\"10\",\"minlength\":\"5\",\"validtype\":\"\",\"required\":\"1\",\"editable\":\"1\",\"disabled\":\"0\",\"readonly\":\"0\"}', '', '40', '2015-12-09 15:39:13', '2015-12-06 20:33:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('279', '53', '基本信息', '昵称', '', '', 'nickname', '', '3', 'nickname', 'form_textbox', '{\"width\":\"\",\"prompt\":\"\",\"maxlength\":\"6\",\"minlength\":\"2\",\"validtype\":\"\",\"required\":\"1\",\"editable\":\"1\",\"disabled\":\"0\",\"readonly\":\"0\"}', '', '40', '2015-12-09 15:39:04', '2015-12-06 20:35:01', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('280', '53', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-06 20:35:12', '2015-12-06 20:35:12', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('281', '19', '基本配置', '最小字符数', '', 'config', 'minlength', '', '3', 'minlength', 'form_numberbox', '', '', '34', '2015-12-09 15:27:46', '2015-12-06 20:37:06', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('282', '19', '基本配置', '最多字符数', '', 'config', 'maxlength', '', '3', 'maxlength', 'form_numberbox', '', '', '35', '2015-12-09 15:27:41', '2015-12-06 20:37:39', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('283', '55', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-06 21:00:25', '2015-12-06 21:00:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('284', '55', '基本信息', '标题', '', '', 'text', '', '3', 'text', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '97', '2015-12-10 16:14:47', '2015-12-06 21:01:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('338', '64', '基本配置', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-07 20:22:53', '2015-12-07 20:22:53', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('286', '55', '基本信息', '图标', '', '', 'iconcls', '', '3', 'iconcls', 'form_textbox', '', '', '77', '2015-12-06 21:02:01', '2015-12-06 21:02:01', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('287', '55', '基本信息', '说明', '', '', 'instruction', '', '3', 'instruction', 'form_multitextbox', '', '', '46', '2015-12-06 21:02:18', '2015-12-06 21:02:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('288', '55', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-09 15:39:30', '2015-12-06 21:02:33', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('289', '55', '基本信息', '分组', '', '', 'group', '', '3', 'group', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '99', '2015-12-10 16:14:42', '2015-12-06 21:03:46', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('290', '54', '基本信息', '分组', '', '', 'group', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '99', '2015-12-10 16:14:33', '2015-12-06 21:08:06', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('291', '54', '基本信息', '标题', '', '', 'text', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '96', '2015-12-10 16:14:24', '2015-12-06 21:08:17', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('339', '64', '基本配置', '可选值', '', 'config', 'values', '', '3', 'values', 'form_multitextbox', '', '一行一个，例如：\r\n哈哈|haha\r\n中国|china\r\n北京|beijing', '50', '2015-12-09 15:20:21', '2015-12-07 20:23:36', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('293', '54', '基本信息', '说明', '', '', 'instruction', '', '0', '', 'form_multitextbox', '', '', '75', '2015-12-06 21:08:55', '2015-12-06 21:08:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('294', '54', '基本信息', '图标', '', '', 'iconcls', '', '0', '', 'form_textbox', '', '', '79', '2015-12-06 21:09:09', '2015-12-06 21:09:09', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('295', '54', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '10', '2015-12-09 15:39:35', '2015-12-06 21:09:18', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('296', '57', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-06 21:11:44', '2015-12-06 21:11:44', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('297', '57', '基本信息', '分组', '', '', 'group', '', '3', 'group', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '99', '2015-12-10 16:19:33', '2015-12-06 21:12:01', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('298', '57', '基本信息', '标题', '', '', 'text', '', '3', 'text', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '95', '2015-12-10 17:04:14', '2015-12-06 21:12:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('299', '57', '基本信息', '字段', '', '', 'name', '', '3', 'name', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '94', '2015-12-10 17:04:27', '2015-12-06 21:12:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('300', '57', '基本信息', '默认值', '', '', 'value', '', '3', 'value', 'form_multitextbox', '', '', '87', '2015-12-06 21:13:13', '2015-12-06 21:13:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('301', '57', '基本信息', '表单类型', '', '', 'type', '', '3', 'type', 'form_combotree', '{\"model\":\"form\",\"disabled\":\"0\",\"rootitem\":\"0\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u8868\\u5355\\u914d\\u7f6e\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"name\",\"width\":\"\"}', '', '93', '2016-01-06 22:20:07', '2015-12-06 21:13:54', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('302', '57', '基本信息', '说明', '', '', 'instruction', '', '3', 'instruction', 'form_multitextbox', '', '', '67', '2015-12-06 21:14:53', '2015-12-06 21:14:53', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('303', '57', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-09 15:39:20', '2015-12-06 21:15:04', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('304', '56', '基本信息', '分组', '', '', 'group', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '99', '2015-12-10 16:14:57', '2015-12-06 21:15:52', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('305', '56', '基本信息', '标题', '', '', 'text', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"10\",\"minlength\":\"1\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '96', '2015-12-10 16:15:29', '2015-12-06 21:16:03', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('306', '56', '基本信息', '字段', '', '', 'name', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"20\",\"minlength\":\"1\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '89', '2015-12-10 16:19:19', '2015-12-06 21:16:18', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('307', '56', '基本信息', '默认值', '', '', 'value', '', '0', '', 'form_multitextbox', '', '', '78', '2015-12-06 21:16:35', '2015-12-06 21:16:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('308', '56', '基本信息', '表单类型', '', '', 'type', '', '0', '', 'form_combotree', '{\"model\":\"form\",\"disabled\":\"0\",\"rootitem\":\"0\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u8868\\u5355\\u914d\\u7f6e\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"name\",\"width\":\"\"}', '', '91', '2016-01-06 22:20:34', '2015-12-06 21:16:54', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('309', '56', '基本信息', '说明', '', '', 'instruction', '', '0', '', 'form_multitextbox', '', '', '60', '2015-12-06 21:18:00', '2015-12-06 21:18:00', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('310', '56', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '1', '2015-12-06 21:18:09', '2015-12-06 21:18:09', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('573', '51', '基本信息', '分组', '', '', 'group', '', '3', 'group', 'form_radio', '{\"disabled\":\"0\",\"values\":\"\\u7cfb\\u7edf|\\u7cfb\\u7edf\\r\\n\\u666e\\u901a|\\u666e\\u901a\"}', '', '99', '2016-01-10 17:42:26', '2016-01-10 17:42:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('570', '64', '基本配置', '只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '99', '2016-01-10 17:23:44', '2016-01-10 17:23:12', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('571', '65', '基本信息', '只读', '', 'config', 'disabled', '', '3', 'disabled', 'form_bool', '', '', '99', '2016-01-10 17:24:13', '2016-01-10 17:24:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('572', '50', '基本信息', '分组', '', '', 'group', '', '3', 'group', 'form_radio', '{\"disabled\":\"0\",\"values\":\"\\u7cfb\\u7edf|\\u7cfb\\u7edf\\r\\n\\u666e\\u901a|\\u666e\\u901a\"}', '', '99', '2016-01-10 17:41:55', '2016-01-10 17:41:09', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('343', '65', '基本信息', '可选值', '', 'config', 'values', '', '3', 'values', 'form_multitextbox', '', '一样一个，用|分割键值对，例如：\r\n红色|red\r\n绿色|green\r\n黑色|black\r\n灰色|gray', '50', '2015-12-09 15:20:12', '2015-12-07 20:46:17', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('342', '65', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-07 20:44:42', '2015-12-07 20:44:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('409', '70', '基本信息', '文件', '', '', 'cron', '', '0', '', 'form_textbox', '{\"datadict\":\"cron\",\"required\":\"1\",\"rootitem\":\"0\",\"prompt\":\"\",\"width\":\"\",\"editable\":\"0\",\"disabled\":\"0\",\"readonly\":\"0\",\"validtype\":\"\",\"maxlength\":\"\",\"minlength\":\"\"}', '请填写网站目录 /Application/Common/Cron/ 下的文件名称\r\n配置项 对应 模型管理中 定时任务分组下的对应文件名模型', '50', '2016-01-17 21:36:48', '2015-12-08 22:54:51', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('410', '70', '基本信息', '初始执行时间', '', '', 'initruntime', '', '0', '', 'form_datetimebox', '{\"timeseparator\":\"\",\"spinnerwidth\":\"\",\"closetext\":\"\",\"currenttext\":\"\",\"okText\":\"\",\"editable\":\"0\",\"disabled\":\"0\",\"readonly\":\"0\",\"prompt\":\"\"}', '', '45', '2016-01-10 21:42:01', '2015-12-08 22:55:54', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('411', '70', '基本信息', '执行时间间隔', '', '', 'intervals', '', '0', '', 'form_datadict', '{\"datadict\":\"second\",\"required\":\"1\",\"rootitem\":\"0\",\"prompt\":\"\",\"width\":\"\",\"editable\":\"1\",\"disabled\":\"0\",\"readonly\":\"0\",\"validtype\":\"\",\"maxlength\":\"\",\"minlength\":\"\"}', '', '40', '2015-12-08 22:58:58', '2015-12-08 22:56:40', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('412', '70', '基本信息', '说明', '', '', 'instruction', '', '0', '', 'form_multitextbox', '', '', '20', '2015-12-08 22:57:11', '2015-12-08 22:57:11', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('413', '70', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '10', '2015-12-09 15:37:56', '2015-12-08 22:57:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('414', '71', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '0', '2015-12-08 23:00:41', '2015-12-08 23:00:41', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('415', '71', '基本信息', '文件', '', '', 'cron', '', '3', 'cron', 'form_textbox', '{\"datadict\":\"cron\",\"required\":\"1\",\"prompt\":\"\",\"width\":\"\",\"editable\":\"0\",\"disabled\":\"0\",\"readonly\":\"0\",\"validtype\":\"\",\"maxlength\":\"\",\"minlength\":\"\"}', '请填写网站目录 /Application/Common/Cron/ 下的文件名称\r\n配置项 对应 模型管理中 定时任务分组下的对应文件名模型', '50', '2016-01-17 21:36:53', '2015-12-08 23:01:06', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('416', '71', '基本信息', '初始执行时间', '', '', 'initruntime', '', '3', 'initruntime', 'form_datetimebox', '', '', '45', '2016-01-10 21:41:51', '2015-12-08 23:01:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('417', '71', '基本信息', '执行时间间隔', '', '', 'intervals', '', '3', 'intervals', 'form_datadict', '{\"datadict\":\"second\",\"required\":\"1\",\"rootitem\":\"0\",\"prompt\":\"\",\"width\":\"\",\"editable\":\"0\",\"disabled\":\"0\",\"readonly\":\"0\",\"validtype\":\"\",\"maxlength\":\"\",\"minlength\":\"\"}', '', '40', '2015-12-08 23:06:08', '2015-12-08 23:02:22', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('418', '71', '基本信息', '说明', '', '', 'instruction', '', '3', 'instruction', 'form_multitextbox', '', '', '20', '2015-12-08 23:03:16', '2015-12-08 23:03:16', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('419', '71', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-09 15:37:50', '2015-12-08 23:03:29', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('420', '70', '基本信息', '图标', '', '', 'iconcls', '', '0', '', 'form_textbox', '', '', '30', '2015-12-08 23:04:09', '2015-12-08 23:04:09', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('421', '71', '基本信息', '图标', '', '', 'iconcls', '', '3', 'iconcls', 'form_textbox', '', '', '30', '2015-12-08 23:04:30', '2015-12-08 23:04:30', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('422', '76', '基本信息', '父级', '', '', 'pid', '', '1', 'pid', 'form_combotree', '{\"model\":\"articlecate\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"1\",\"prompt\":\"\",\"queryparams\":\"\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '50', '2016-01-11 15:04:12', '2015-12-09 16:06:32', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('423', '76', '基本信息', '名称', '', '', 'text', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"10\",\"minlength\":\"2\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\\u4ec5\\u7528\\u4e8e\\u540e\\u53f0\\u663e\\u793a\",\"width\":\"\"}', '', '35', '2015-12-09 16:13:31', '2015-12-09 16:07:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('424', '76', '基本信息', '标识', '', '', 'mark', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"20\",\"minlength\":\"2\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\\u8bf7\\u586b\\u5199\\u82f1\\u6587\\u5b57\\u7b26\",\"width\":\"\"}', '', '33', '2015-12-09 16:14:00', '2015-12-09 16:07:23', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('425', '76', '基本信息', '模型', '', '', 'model', '', '0', '', 'form_combotree', '{\"model\":\"model\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u6587\\u7ae0\\u6a21\\u578b\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '30', '2016-01-11 15:04:28', '2015-12-09 16:08:11', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('426', '76', '基本信息', '内容页模板', '', '', 'tpl_detail', '', '0', '', 'form_textbox', '', '', '25', '2015-12-09 16:08:40', '2015-12-09 16:08:40', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('427', '76', '基本信息', '列表页模板', '', '', 'tpl', '', '0', '', 'form_textbox', '', '', '28', '2015-12-09 16:09:10', '2015-12-09 16:09:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('428', '76', '基本信息', '列表分页大小', '', '', 'pagenum', '', '0', '', 'form_numberbox', '', '', '26', '2015-12-09 16:09:51', '2015-12-09 16:09:51', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('429', '76', '基本信息', 'SEO标题', '', '', 'title', '', '0', '', 'form_textbox', '{\"required\":\"0\",\"editable\":\"1\",\"maxlength\":\"80\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '20', '2015-12-09 16:16:44', '2015-12-09 16:10:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('430', '76', '基本信息', 'SEO关键字', '', '', 'keywords', '', '0', '', 'form_textbox', '{\"required\":\"0\",\"editable\":\"1\",\"maxlength\":\"80\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '18', '2015-12-09 16:18:11', '2015-12-09 16:11:15', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('431', '76', '基本信息', 'SEO摘要', '', '', 'description', '', '0', '', 'form_multitextbox', '{\"required\":\"0\",\"height\":\"\",\"width\":\"\",\"editable\":\"1\",\"maxlength\":\"255\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\",\"readonly\":\"0\",\"disabled\":\"0\"}', '', '17', '2015-12-09 16:16:59', '2015-12-09 16:11:41', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('432', '76', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '5', '2015-12-09 16:12:01', '2015-12-09 16:12:01', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('433', '77', '基本信息', '父级', '', '', 'pid', '', '3', 'pid', 'form_combotree', '{\"model\":\"articlecate\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"1\",\"prompt\":\"\",\"queryparams\":\"\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '45', '2016-01-11 15:04:39', '2015-12-09 16:20:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('434', '77', '基本信息', '标题', '', '', 'text', '', '3', 'text', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '44', '2015-12-09 16:25:29', '2015-12-09 16:20:44', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('435', '77', '基本信息', '标识', '', '', 'mark', '', '3', 'mark', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '40', '2015-12-09 16:25:36', '2015-12-09 16:21:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('436', '77', '基本信息', '模型', '', '', 'model', '', '3', 'model', 'form_combotree', '{\"model\":\"model\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u6587\\u7ae0\\u6a21\\u578b\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '35', '2016-01-11 15:04:54', '2015-12-09 16:21:30', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('437', '77', '基本信息', '列表页模板', '', '', 'tpl', '', '3', 'tpl', 'form_textbox', '', '', '31', '2015-12-09 16:22:16', '2015-12-09 16:21:50', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('438', '77', '基本信息', '列表页分页大小', '', '', 'pagenum', '', '3', 'pagenum', 'form_numberbox', '', '', '32', '2015-12-09 16:22:10', '2015-12-09 16:22:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('439', '77', '基本信息', '内容页模板', '', '', 'tpl_detail', '', '3', 'tpl_detail', 'form_textbox', '', '', '30', '2015-12-09 16:22:36', '2015-12-09 16:22:36', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('440', '77', '基本信息', 'SEO标题', '', '', 'title', '', '3', 'title', 'form_textbox', '', '', '25', '2015-12-09 16:23:03', '2015-12-09 16:23:03', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('441', '77', '基本信息', 'SEO关键字', '', '', 'keywords', '', '3', 'keywords', 'form_textbox', '', '', '24', '2015-12-09 16:23:24', '2015-12-09 16:23:24', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('442', '77', '基本信息', 'SEO摘要', '', '', 'description', '', '3', 'description', 'form_multitextbox', '', '', '23', '2015-12-09 16:23:41', '2015-12-09 16:23:41', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('443', '77', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-09 16:24:00', '2015-12-09 16:24:00', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('444', '77', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-09 16:24:23', '2015-12-09 16:24:23', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('445', '79', '基本信息', '标题', '', '', 'title', '', '3', 'title', 'form_textbox', '', '', '50', '2015-12-09 16:38:42', '2015-12-09 16:38:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('446', '79', '基本信息', '缩略图', '', '', 'thumb', '', '3', 'thumb', 'form_image', '', '', '45', '2015-12-09 16:39:21', '2015-12-09 16:39:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('447', '79', '基本信息', '内容', 'article_body', '', 'body', '', '3', 'body', 'form_ueditor', '', '', '40', '2015-12-09 16:40:18', '2015-12-09 16:40:18', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('448', '79', '基本信息', '关键字', '', '', 'keywords', '', '3', 'keywords', 'form_textbox', '', '', '47', '2016-01-21 21:00:19', '2015-12-09 16:41:03', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('449', '79', '基本信息', '摘要', '', '', 'description', '', '3', 'description', 'form_multitextbox', '', '', '46', '2016-01-21 21:00:27', '2015-12-09 16:41:30', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('450', '79', '其他信息', '模板', '', '', 'tpl', '', '3', 'tpl', 'form_textbox', '', '', '30', '2015-12-09 16:42:26', '2015-12-09 16:42:26', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('451', '79', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '12', '2015-12-09 16:42:42', '2015-12-09 16:42:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('452', '79', '其他信息', '标识', '', '', 'mark', '', '3', 'mark', 'form_textbox', '', '', '25', '2015-12-09 16:43:42', '2015-12-09 16:43:16', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('453', '79', '其他信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-09 16:43:35', '2015-12-09 16:43:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('454', '78', '基本信息', '分类', '', '', 'category_id', '', '1', 'category_id', 'form_hidden', '', '', '0', '2015-12-09 16:57:02', '2015-12-09 16:57:02', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('455', '78', '基本信息', '标题', '', '', 'title', '', '0', '', 'form_textbox', '', '', '50', '2015-12-09 16:57:27', '2015-12-09 16:57:27', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('456', '78', '基本信息', '缩略图', '', '', 'thumb', '', '0', '', 'form_image', '', '', '45', '2015-12-09 16:57:39', '2015-12-09 16:57:39', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('457', '78', '基本信息', '内容', '', 'article_body', 'body', '', '0', '', 'form_ueditor', '', '', '40', '2015-12-09 16:57:59', '2015-12-09 16:57:59', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('458', '78', '基本信息', '关键字', '', '', 'keywords', '', '0', '', 'form_textbox', '', '', '47', '2016-01-21 20:59:57', '2015-12-09 16:58:28', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('459', '78', '基本信息', '摘要', '', '', 'description', '', '0', '', 'form_multitextbox', '', '', '46', '2016-01-21 21:00:06', '2015-12-09 16:58:53', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('460', '78', '其他信息', '标志', '', '', 'mark', '', '0', '', 'form_textbox', '', '', '30', '2015-12-09 16:59:17', '2015-12-09 16:59:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('461', '78', '其他信息', '模板', '', '', 'tpl', '', '0', '', 'form_textbox', '', '', '25', '2015-12-09 17:00:02', '2015-12-09 16:59:47', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('462', '78', '其他信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '10', '2015-12-09 17:00:19', '2015-12-09 17:00:19', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('468', '83', '基本信息', '标题', '', '', 'text', '', '3', 'text', 'form_textbox', '', '', '50', '2015-12-09 17:20:37', '2015-12-09 17:20:37', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('464', '82', '基本信息', '标题', '', '', 'text', '', '0', '', 'form_textbox', '', '', '35', '2015-12-09 17:06:40', '2015-12-09 17:06:40', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('465', '82', '基本信息', '标识', '', '', 'mark', '', '0', '', 'form_textbox', '', '', '30', '2015-12-09 17:06:55', '2015-12-09 17:06:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('466', '82', '基本信息', '模型', '', '', 'model', '', '0', '', 'form_combotree', '{\"model\":\"model\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u5bfc\\u822a\\u6a21\\u578b\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '25', '2016-01-11 12:04:07', '2015-12-09 17:07:16', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('467', '82', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '10', '2015-12-09 17:07:36', '2015-12-09 17:07:36', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('469', '83', '基本信息', '标识', '', '', 'mark', '', '3', 'mark', 'form_textbox', '', '', '45', '2015-12-09 17:20:50', '2015-12-09 17:20:50', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('470', '83', '基本信息', '模型', '', '', 'model', '', '3', 'model', 'form_combotree', '{\"model\":\"model\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u5bfc\\u822a\\u6a21\\u578b\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '30', '2016-01-11 12:03:55', '2015-12-09 17:21:05', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('471', '83', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-09 17:21:21', '2015-12-09 17:21:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('472', '83', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-09 17:21:33', '2015-12-09 17:21:33', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('473', '80', '基本信息', '分类', '', '', 'category_id', '', '1', 'category_id', 'form_hidden', '', '', '50', '2015-12-10 10:38:52', '2015-12-10 10:38:52', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('474', '80', '基本信息', '父级', '', '', 'pid', '', '1', 'pid', 'form_combotree', '{\"model\":\"nav\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"1\",\"prompt\":\"\",\"queryparams\":\"category_id|eq|(I)category_id\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '45', '2016-01-11 11:53:19', '2015-12-10 10:39:21', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('475', '80', '基本信息', '标题', '', '', 'text', '', '0', '', 'form_textbox', '', '', '40', '2015-12-10 10:39:38', '2015-12-10 10:39:38', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('476', '80', '基本信息', '链接地址', '', '', 'url', '', '0', '', 'form_textbox', '', '', '35', '2015-12-10 10:40:18', '2015-12-10 10:40:18', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('477', '80', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '10', '2015-12-10 10:40:39', '2015-12-10 10:40:39', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('478', '56', '基本信息', '分类', '', '', 'category_id', '', '1', 'category_id', 'form_hidden', '', '', '2', '2015-12-10 10:47:10', '2015-12-10 10:47:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('479', '81', '基本信息', '父类', '', '', 'pid', '', '3', 'pid', 'form_combotree', '{\"model\":\"nav\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"1\",\"prompt\":\"\",\"queryparams\":\"category_id|eq|($)category_id\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '45', '2016-01-11 11:53:26', '2015-12-10 10:52:10', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('480', '81', '基本信息', '标题', '', '', 'text', '', '3', 'text', 'form_textbox', '', '', '40', '2015-12-10 10:52:28', '2015-12-10 10:52:28', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('481', '81', '基本信息', '链接', '', '', 'url', '', '3', 'url', 'form_textbox', '', '', '35', '2015-12-10 10:53:39', '2015-12-10 10:53:39', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('482', '81', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '30', '2015-12-10 10:53:53', '2015-12-10 10:53:53', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('483', '81', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '1', '2015-12-10 10:54:13', '2015-12-10 10:54:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('484', '81', '基本信息', '分类', '', '', 'category_id', '', '3', 'category_id', 'form_hidden', '', '', '0', '2015-12-10 14:04:29', '2015-12-10 14:04:29', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('485', '85', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '0', '2015-12-10 14:08:30', '2015-12-10 14:08:30', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('486', '85', '基本信息', '名称', '', '', 'title', '', '3', 'title', 'form_textbox', '', '', '45', '2015-12-10 14:10:02', '2015-12-10 14:09:51', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('487', '85', '基本信息', '链接', '', '', 'url', '', '3', 'url', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"url\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '40', '2015-12-10 14:12:11', '2015-12-10 14:10:18', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('488', '85', '基本信息', 'LOGO', '', '', 'logo', '', '3', 'logo', 'form_image', '', '', '35', '2016-01-11 11:18:46', '2015-12-10 14:10:34', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('489', '85', '基本信息', '简介', '', '', 'description', '', '3', 'description', 'form_multitextbox', '', '', '30', '2015-12-10 14:10:57', '2015-12-10 14:10:57', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('490', '85', '基本信息', '备注', '', '', 'info', '', '3', 'info', 'form_multitextbox', '', '', '25', '2015-12-10 14:11:20', '2015-12-10 14:11:20', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('491', '85', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_textbox', '', '', '20', '2015-12-10 14:11:30', '2015-12-10 14:11:30', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('492', '84', '基本信息', '名称', '', '', 'title', '', '0', '', 'form_textbox', '', '', '45', '2015-12-10 14:13:32', '2015-12-10 14:13:32', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('493', '84', '基本信息', '网址', '', '', 'url', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"url\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '40', '2015-12-10 14:19:21', '2015-12-10 14:13:48', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('494', '84', '基本信息', 'LOGO', '', '', 'logo', '', '0', '', 'form_image', '', '', '35', '2015-12-10 14:14:03', '2015-12-10 14:14:03', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('495', '84', '基本信息', '简介', '', '', 'description', '', '0', '', 'form_multitextbox', '', '', '30', '2015-12-10 14:14:22', '2015-12-10 14:14:22', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('496', '84', '基本信息', '备注', '', '', 'info', '', '0', '', 'form_multitextbox', '', '', '25', '2015-12-10 14:14:35', '2015-12-10 14:14:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('497', '84', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '20', '2015-12-10 14:14:51', '2015-12-10 14:14:51', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('498', '85', '基本信息', '分组', '', '', 'group', '', '3', 'group', 'form_radio', '{\"disabled\":\"0\",\"values\":\"\\u9ed8\\u8ba4\\u5206\\u7ec4|\\u9ed8\\u8ba4\\u5206\\u7ec4\"}', '', '50', '2016-01-11 11:12:02', '2015-12-10 14:15:45', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('499', '84', '基本信息', '分组', '', '', 'group', '', '0', '默认分组', 'form_radio', '{\"disabled\":\"0\",\"values\":\"\\u9ed8\\u8ba4\\u5206\\u7ec4|\\u9ed8\\u8ba4\\u5206\\u7ec4\"}', '', '50', '2016-01-11 11:11:50', '2015-12-10 14:16:08', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('500', '86', '基本信息', '留言内容', '', '', 'content', '', '3', 'content', 'form_multitextbox', '{\"required\":\"1\",\"height\":\"\",\"width\":\"\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\",\"readonly\":\"0\",\"disabled\":\"0\"}', '', '45', '2015-12-10 14:26:24', '2015-12-10 14:25:11', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('501', '86', '基本信息', '回复内容', '', '', 'reply', '', '3', 'reply', 'form_multitextbox', '{\"required\":\"0\",\"height\":\"\",\"width\":\"\",\"editable\":\"0\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\",\"readonly\":\"1\",\"disabled\":\"1\"}', '', '40', '2015-12-10 14:26:10', '2015-12-10 14:25:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('502', '86', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '0', '2015-12-10 14:25:49', '2015-12-10 14:25:49', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('503', '87', '基本信息', '留言内容', '', '', 'content', '', '3', 'content', 'form_multitextbox', '{\"required\":\"1\",\"height\":\"\",\"width\":\"\",\"editable\":\"0\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\",\"readonly\":\"1\",\"disabled\":\"1\"}', '', '45', '2015-12-10 14:27:35', '2015-12-10 14:26:47', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('504', '87', '基本信息', '回复内容', '', '', 'reply', '', '3', 'reply', 'form_multitextbox', '{\"required\":\"1\",\"height\":\"\",\"width\":\"\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\",\"readonly\":\"0\",\"disabled\":\"0\"}', '', '40', '2015-12-10 14:27:47', '2015-12-10 14:27:04', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('505', '87', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '12', '2015-12-10 14:27:16', '2015-12-10 14:27:16', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('506', '88', '基本信息', '分组', '', '', 'group', '', '0', '默认分组', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '50', '2015-12-10 14:43:45', '2015-12-10 14:39:58', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('507', '88', '基本信息', '标题', '', '', 'text', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"20\",\"minlength\":\"1\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '45', '2015-12-10 14:44:14', '2015-12-10 14:40:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('508', '88', '基本信息', '标识', '', '', 'mark', '', '0', '', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"20\",\"minlength\":\"2\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\\u82f1\\u6587\\u6807\\u8bc6\",\"width\":\"\"}', '', '40', '2015-12-10 14:42:35', '2015-12-10 14:40:40', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('509', '88', '基本信息', '模型', '', '', 'model', '', '0', '', 'form_combotree', '{\"model\":\"model\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u63a8\\u8350\\u6a21\\u578b\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '30', '2016-01-11 14:44:54', '2015-12-10 14:41:19', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('510', '88', '基本信息', '图标', '', '', 'iconcls', '', '0', '', 'form_textbox', '', '', '25', '2015-12-10 14:41:42', '2015-12-10 14:41:42', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('511', '88', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '20', '2015-12-10 14:41:56', '2015-12-10 14:41:56', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('512', '89', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '0', '2015-12-10 14:44:32', '2015-12-10 14:44:32', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('513', '89', '基本信息', '分组', '', '', 'group', '', '3', 'group', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"20\",\"minlength\":\"2\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '50', '2015-12-10 14:47:32', '2015-12-10 14:44:44', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('514', '89', '基本信息', '标题', '', '', 'text', '', '3', 'text', 'form_textbox', '', '', '45', '2015-12-10 14:44:55', '2015-12-10 14:44:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('515', '89', '基本信息', '标识', '', '', 'mark', '', '3', 'mark', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"10\",\"minlength\":\"2\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\\u82f1\\u6587\\u5b57\\u7b26\",\"width\":\"\"}', '', '40', '2015-12-10 14:47:21', '2015-12-10 14:45:11', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('516', '89', '基本信息', '模型', '', '', 'model', '', '3', 'model', 'form_combotree', '{\"model\":\"model\",\"disabled\":\"0\",\"rootitem\":\"1\",\"tree\":\"0\",\"prompt\":\"\",\"queryparams\":\"group|eq|\\u63a8\\u8350\\u6a21\\u578b\",\"pid\":\"\",\"textfield\":\"\",\"valuefield\":\"\",\"width\":\"\"}', '', '35', '2016-01-11 14:45:06', '2015-12-10 14:45:33', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('517', '89', '基本信息', '图标', '', '', 'iconcls', '', '3', 'iconcls', 'form_textbox', '', '', '30', '2015-12-10 14:45:55', '2015-12-10 14:45:55', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('518', '89', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-10 14:46:11', '2015-12-10 14:46:11', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('519', '91', '基本信息', '推荐位', '', '', 'category_id', '', '3', 'category_id', 'form_hidden', '{\"url\":\"Admin\\/Recommendcate\\/index\",\"required\":\"1\",\"editable\":\"0\",\"rootitem\":\"0\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"queryparams\":\"\",\"textfield\":\"\",\"pid\":\"\",\"validtype\":\"\",\"minlength\":\"\",\"maxlength\":\"\",\"width\":\"\"}', '', '50', '2015-12-10 15:09:04', '2015-12-10 14:59:59', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('520', '91', '基本信息', 'id', '', '', 'id', '', '1', 'id', 'form_hidden', '', '', '22', '2015-12-10 15:00:13', '2015-12-10 15:00:13', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('521', '91', '基本信息', '标题', '', '', 'title', '', '3', 'title', 'form_textbox', '{\"required\":\"1\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '45', '2015-12-10 15:06:24', '2015-12-10 15:00:48', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('522', '91', '基本信息', '图片', '', '', 'thumb', '', '3', 'thumb', 'form_image', '{\"required\":\"0\",\"extensions\":\"jpg\",\"editable\":\"1\",\"readonly\":\"0\",\"prompt\":\"\",\"disabled\":\"0\"}', '', '35', '2015-12-10 15:07:16', '2015-12-10 15:01:05', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('523', '91', '基本信息', '链接地址', '', '', 'url', '', '3', 'url', 'form_textbox', '{\"required\":\"0\",\"editable\":\"1\",\"maxlength\":\"\",\"minlength\":\"\",\"validtype\":\"\",\"readonly\":\"0\",\"disabled\":\"0\",\"prompt\":\"\",\"width\":\"\"}', '', '40', '2015-12-10 15:06:56', '2015-12-10 15:01:47', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('524', '91', '基本信息', '摘要', '', '', 'description', '', '3', 'description', 'form_multitextbox', '{\"required\":\"0\",\"height\":\"\",\"width\":\"\",\"editable\":\"1\",\"maxlength\":\"255\",\"minlength\":\"\",\"validtype\":\"\",\"prompt\":\"\",\"readonly\":\"0\",\"disabled\":\"0\"}', '', '30', '2015-12-10 15:07:35', '2015-12-10 15:02:37', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('525', '91', '基本信息', '排序', '', '', 'sort', '', '3', 'sort', 'form_numberbox', '', '', '10', '2015-12-10 15:02:58', '2015-12-10 15:02:58', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('526', '90', '基本信息', '推荐位', '', '', 'category_id', '', '1', 'category_id', 'form_hidden', '', '', '50', '2015-12-10 15:09:13', '2015-12-10 15:08:20', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('527', '90', '基本信息', '标题', '', '', 'title', '', '0', '', 'form_textbox', '', '', '45', '2015-12-10 15:08:47', '2015-12-10 15:08:47', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('528', '90', '基本信息', '链接地址', '', '', 'url', '', '0', '', 'form_textbox', '', '', '40', '2015-12-10 15:09:35', '2015-12-10 15:09:35', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('529', '90', '基本信息', '图片', '', '', 'thumb', '', '0', '', 'form_image', '', '', '35', '2015-12-10 15:09:51', '2015-12-10 15:09:51', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('530', '90', '基本信息', '摘要', '', '', 'description', '', '0', '', 'form_multitextbox', '', '', '30', '2015-12-10 15:10:09', '2015-12-10 15:10:09', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('531', '90', '基本信息', '排序', '', '', 'sort', '', '0', '', 'form_numberbox', '', '', '10', '2015-12-10 15:10:22', '2015-12-10 15:10:22', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('565', '32', '其他配置', '显示字段', '', 'config', 'textfield', '', '3', 'textfield', 'form_textbox', '', '默认为text', '9', '2015-12-26 23:26:57', '2015-12-26 23:25:31', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('566', '32', '其他配置', '值字段', '', 'config', 'valuefield', '', '3', 'valuefield', 'form_textbox', '', '默认为id', '6', '2015-12-26 23:26:23', '2015-12-26 23:26:23', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('574', '98', '基本信息', '旧密码', '', '', 'oldpassword', '', '0', '', 'form_textbox', '', '', '50', '2016-01-17 20:15:49', '2016-01-17 20:15:49', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('575', '98', '基本信息', '新密码', '', '', 'password', '', '0', '', 'form_textbox', '', '', '45', '2016-01-17 20:16:25', '2016-01-17 20:16:25', '1', '0', '');
INSERT INTO `ebcms_formfield` VALUES ('576', '98', '基本信息', '重复密码', '', '', 'passwordtwo', '', '0', '', 'form_textbox', '', '', '40', '2016-01-17 20:16:40', '2016-01-17 20:16:40', '1', '0', '');

-- ----------------------------
-- Table structure for `ebcms_guestbook`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_guestbook`;
CREATE TABLE `ebcms_guestbook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `mobile` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '手机号码',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '留言内容',
  `reply` varchar(255) NOT NULL DEFAULT '' COMMENT '回复内容',
  `reply_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '回复时间',
  `update_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '操作时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='留言板表';

-- ----------------------------
-- Records of ebcms_guestbook
-- ----------------------------
INSERT INTO `ebcms_guestbook` VALUES ('1', 'asdfasdf', '15225236526', '你好，你们的系统是否是开源的呢？', '你好，感谢你的关注，我们的系统完全开源！', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '1', '1');
INSERT INTO `ebcms_guestbook` VALUES ('2', '小强', '15425445542', '你们的系统很好，我怎么购买呢？', '你好，学生是免费使用我们的系统的哦！', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_link`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_link`;
CREATE TABLE `ebcms_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `info` varchar(255) NOT NULL DEFAULT '' COMMENT '其他信息',
  `logo` varchar(255) NOT NULL DEFAULT '' COMMENT '图片地址',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '连接地址',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- Records of ebcms_link
-- ----------------------------
INSERT INTO `ebcms_link` VALUES ('1', '默认分组', '易贝内容管理系统', '', '', '/image/20160127/56a8c581797f6.jpg', 'http://www.ebcms.com', '1453901188', '1447745179', '80', '1', '1');
INSERT INTO `ebcms_link` VALUES ('2', '默认分组', 'ThinkPHP', '', '', '', 'http://www.thinkphp.cn', '1453643641', '1449728386', '2', '1', '1');
INSERT INTO `ebcms_link` VALUES ('3', '默认分组', 'php学习交流', '', '', '', 'http://www.phpstd.com', '1453643715', '1452482209', '0', '1', '1');

-- ----------------------------
-- Table structure for `ebcms_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_menu`;
CREATE TABLE `ebcms_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜菜单ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '类型',
  `text` varchar(20) NOT NULL DEFAULT '' COMMENT '标题',
  `url` varchar(250) NOT NULL DEFAULT '' COMMENT '附加参数',
  `iconcls` varchar(40) NOT NULL DEFAULT '' COMMENT '图标',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  `sys_mark` varchar(255) NOT NULL DEFAULT '' COMMENT '系统标志',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COMMENT='功能菜单表';

-- ----------------------------
-- Records of ebcms_menu
-- ----------------------------
INSERT INTO `ebcms_menu` VALUES ('6', '96', 'admin', '附件管理', 'Admin/Attachment/index', 'glyphicon glyphicon-paperclip', '2016-01-14 20:10:24', '2015-07-25 17:04:49', '19', '1', '1', '');
INSERT INTO `ebcms_menu` VALUES ('13', '96', 'admin', '模板管理', 'Admin/Template/index', 'glyphicon glyphicon-leaf', '2016-01-14 20:16:21', '2015-07-25 17:04:49', '14', '1', '1', '');
INSERT INTO `ebcms_menu` VALUES ('72', '0', 'admin', '基本管理', '', 'icon-rainbow', '2015-12-02 14:55:12', '2015-07-25 17:04:49', '50', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('74', '72', 'admin', '文章管理', 'Admin/Article/index', 'glyphicon glyphicon-file', '2016-01-14 20:11:13', '2015-07-25 17:04:49', '50', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('79', '72', 'admin', '导航管理', 'Admin/Nav/index', 'glyphicon glyphicon-globe', '2016-01-14 20:12:39', '2015-07-25 17:04:49', '10', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('80', '72', 'admin', '友情链接', 'Admin/Link/index', 'glyphicon glyphicon-link', '2016-01-14 20:13:08', '2015-07-25 17:04:49', '20', '1', '1', '');
INSERT INTO `ebcms_menu` VALUES ('81', '72', 'admin', '留言中心', 'Admin/Guestbook/index', 'glyphicon glyphicon-heart', '2016-01-14 20:12:01', '2015-07-25 17:04:49', '30', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('92', '96', 'admin', '备份优化', 'Admin/Database/index', 'glyphicon glyphicon-floppy-disk', '2016-01-14 20:17:22', '2015-07-25 17:04:49', '10', '1', '1', '');
INSERT INTO `ebcms_menu` VALUES ('95', '72', 'admin', '推荐管理', 'Admin/Recommend/index', 'glyphicon glyphicon-bookmark', '2016-01-14 20:11:50', '2015-09-18 10:16:11', '40', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('96', '0', 'admin', '站点设置', '', 'icon-zone', '2015-10-11 18:46:39', '2015-10-08 11:14:37', '30', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('98', '96', 'admin', '定时任务', 'Admin/Cron/index', 'glyphicon glyphicon-blackboard', '2016-01-14 20:21:50', '2015-10-08 11:19:59', '55', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('100', '0', 'admin', '会员管理', '', 'icon-group', '2015-11-02 10:07:19', '2015-11-02 10:03:25', '40', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('103', '96', 'admin', '模型管理', 'Admin/Model/index', 'glyphicon glyphicon-cloud', '2016-01-14 20:15:52', '2015-11-16 10:54:05', '99', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('105', '100', 'admin', '会员管理', 'Admin/User/index', 'glyphicon glyphicon-user', '2016-01-14 20:13:31', '2015-11-30 15:16:44', '90', '1', '0', '');
INSERT INTO `ebcms_menu` VALUES ('106', '100', 'admin', '角色管理', 'Admin/Group/index', 'glyphicon glyphicon-tree-deciduous', '2016-01-14 20:14:48', '2015-11-30 15:17:12', '85', '1', '0', '');

-- ----------------------------
-- Table structure for `ebcms_model`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_model`;
CREATE TABLE `ebcms_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `instruction` varchar(255) NOT NULL DEFAULT '' COMMENT '说明',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `sys_mark` varchar(255) NOT NULL DEFAULT '' COMMENT '系统标志',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='模型表';

-- ----------------------------
-- Records of ebcms_model
-- ----------------------------
INSERT INTO `ebcms_model` VALUES ('1', '文章模型', '产品模型', '', '9', '2016-01-24 21:05:14', '2015-12-07 22:16:16', '1', '0', '');
INSERT INTO `ebcms_model` VALUES ('2', '导航模型', '顶部导航', '', '7', '2015-12-10 10:42:37', '2015-12-10 10:42:37', '1', '0', '');
INSERT INTO `ebcms_model` VALUES ('3', '推荐模型', '默认模型', '', '5', '2016-01-07 20:48:30', '2015-12-10 15:11:42', '1', '0', '');
INSERT INTO `ebcms_model` VALUES ('4', '系统模型', '地区模型', '', '3', '2016-01-06 22:22:40', '2016-01-06 22:22:40', '1', '0', '');

-- ----------------------------
-- Table structure for `ebcms_modelfield`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_modelfield`;
CREATE TABLE `ebcms_modelfield` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类',
  `group` varchar(250) NOT NULL DEFAULT '' COMMENT '分组',
  `text` varchar(250) NOT NULL DEFAULT '' COMMENT '标题',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '键',
  `value` text COMMENT '默认值',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '表单类型',
  `config` text COMMENT '表单配置',
  `instruction` text COMMENT '表单说明',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  `sys_mark` varchar(255) NOT NULL DEFAULT '' COMMENT '系统标志',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='模型字段表';

-- ----------------------------
-- Records of ebcms_modelfield
-- ----------------------------
INSERT INTO `ebcms_modelfield` VALUES ('13', '1', '产品资料', '产品图', 'pics', '', 'form_images', null, '', '0', '2016-01-24 20:57:21', '2016-01-24 20:57:21', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('2', '2', '测试扩展', '小图标', 'icon', '', 'form_image', '{\"required\":\"0\",\"disabled\":\"0\",\"extensions\":\"\",\"prompt\":\"\"}', '', '2', '2016-01-06 22:22:00', '2015-12-10 10:43:12', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('3', '2', '测试分组。', '大图标', 'bigicon', '', 'form_image', '{\"required\":\"0\",\"disabled\":\"0\",\"extensions\":\"\",\"prompt\":\"\"}', '', '1', '2016-01-28 17:52:21', '2015-12-10 10:47:40', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('4', '3', '我去', '测试字段', 'fuck', 'fuck默认值', 'form_textbox', '', '1', '0', '2016-01-06 22:20:52', '2015-12-10 15:12:10', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('5', '3', '测试字段', '123', 'asd', '阿斯顿发', 'form_textbox', '', '', '0', '2016-01-06 22:21:13', '2015-12-10 15:29:48', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('6', '4', '测试分组', '国家坐标', 'fucs', '123', 'form_textbox', '', '说明', '2', '2016-01-06 22:24:45', '2016-01-06 22:24:45', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('7', '4', '123', '图标', 'pic', '', 'form_image', '', '', '3', '2016-01-06 22:26:02', '2016-01-06 22:26:02', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('8', '4', '阿斯蒂芬', '阿斯蒂芬', 'asd', '阿斯蒂芬萨芬', 'form_ueditor', '', '阿斯蒂芬', '0', '2016-01-06 22:26:19', '2016-01-06 22:26:19', '1', '0', '');
INSERT INTO `ebcms_modelfield` VALUES ('9', '5', '常规', '执行时间间隔', 'times', '阿斯蒂芬', 'form_image', '', '', '0', '2016-01-17 21:28:51', '2016-01-17 21:14:11', '1', '0', '');

-- ----------------------------
-- Table structure for `ebcms_nav`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_nav`;
CREATE TABLE `ebcms_nav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `pid` int(10) unsigned NOT NULL COMMENT '父ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'category_id 冗余',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '连接地址',
  `ext` text COMMENT '扩展信息',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='导航表';

-- ----------------------------
-- Records of ebcms_nav
-- ----------------------------
INSERT INTO `ebcms_nav` VALUES ('1', '0', '1', '首页', 'Home/Index/index', '', '1447731929', '1447743642', '98', '1', '0');
INSERT INTO `ebcms_nav` VALUES ('2', '0', '1', '公司动态', 'Home/Article/index?id=2', '{\"icon\":\"\",\"bigicon\":\"\"}', '1447731954', '1453644125', '93', '1', '0');
INSERT INTO `ebcms_nav` VALUES ('3', '0', '1', '产品中心', 'Home/Article/index?id=3', '{\"icon\":\"\",\"bigicon\":\"\"}', '1447732009', '1453644133', '81', '1', '0');
INSERT INTO `ebcms_nav` VALUES ('4', '0', '1', '公司简介', 'Home/Article/detail?id=2', '', '1447732076', '1447744042', '95', '1', '0');
INSERT INTO `ebcms_nav` VALUES ('5', '0', '1', '资料下载', 'Home/Article/index?mark=zlxz', '', '1447744161', '1447744161', '77', '0', '0');
INSERT INTO `ebcms_nav` VALUES ('6', '0', '1', '开发文档', 'Home/Article/index?mark=kfwd', '', '1447744188', '1447744196', '62', '0', '0');
INSERT INTO `ebcms_nav` VALUES ('7', '6', '1', '基础文档', 'Home/Article/index?mark=jcwd', '', '1447744215', '1447744220', '98', '0', '0');
INSERT INTO `ebcms_nav` VALUES ('8', '6', '1', '友情链接', 'Home/Article/index?mark=yqlj', '{\"icon\":\"123\",\"bigicon\":\"23\"}', '1447744241', '1449727500', '1', '0', '0');
INSERT INTO `ebcms_nav` VALUES ('9', '0', '1', '人才招聘', 'Home/Article/detail?id=11', '{\"icon\":\"\",\"bigicon\":\"\"}', '1447744674', '1453642930', '41', '1', '0');
INSERT INTO `ebcms_nav` VALUES ('10', '0', '1', '留言', 'Home/Guestbook/index', '', '1447744696', '1447744696', '1', '1', '0');
INSERT INTO `ebcms_nav` VALUES ('11', '0', '2', '关于我们', '', '', '1452485103', '1452485103', '0', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_navcate`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_navcate`;
CREATE TABLE `ebcms_navcate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '标识',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `model` varchar(255) NOT NULL DEFAULT '' COMMENT '导航模型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='导航分类表';

-- ----------------------------
-- Records of ebcms_navcate
-- ----------------------------
INSERT INTO `ebcms_navcate` VALUES ('1', 'main', '主导航', '2', '1449727524', '1447731849', '99', '1', '1');
INSERT INTO `ebcms_navcate` VALUES ('2', 'bottom', '底部导航', '', '1452485086', '1452485086', '11', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_recommend`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_recommend`;
CREATE TABLE `ebcms_recommend` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `ext` text COMMENT '扩展信息',
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT '模块',
  `content_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='推荐内容表';

-- ----------------------------
-- Records of ebcms_recommend
-- ----------------------------
INSERT INTO `ebcms_recommend` VALUES ('1', '2', '公司简介', '/image/20160128/56a9c5f54d041.png', '公司简介', '/abc/index.php?m=Home&amp;c=Article&amp;a=detail&amp;id=1', null, 'article', '1', '0', '0', '99', '1', '0');
INSERT INTO `ebcms_recommend` VALUES ('2', '1', '公司简介', '/image/20160128/56a9c5f54d041.png', '公司简介', '/abc/index.php?m=Home&amp;c=Article&amp;a=detail&amp;id=1', null, 'article', '1', '0', '0', '99', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_recommendcate`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_recommendcate`;
CREATE TABLE `ebcms_recommendcate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group` varchar(255) NOT NULL DEFAULT '' COMMENT '分组',
  `text` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `mark` varchar(255) NOT NULL DEFAULT '' COMMENT '标识',
  `model` varchar(255) NOT NULL DEFAULT '' COMMENT '模型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `iconcls` varchar(255) NOT NULL DEFAULT '' COMMENT '小图标',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='推荐位表';

-- ----------------------------
-- Records of ebcms_recommendcate
-- ----------------------------
INSERT INTO `ebcms_recommendcate` VALUES ('1', '默认分组', '轮播图', 'banner', '', '1453039002', '1447744401', 'icon-projection_screen_present', '1', '1', '0');
INSERT INTO `ebcms_recommendcate` VALUES ('2', '默认分组', '左侧推荐', 'index_left', '', '1453038998', '1447744438', 'icon-layout_select_sidebar', '1', '1', '0');

-- ----------------------------
-- Table structure for `ebcms_user`
-- ----------------------------
DROP TABLE IF EXISTS `ebcms_user`;
CREATE TABLE `ebcms_user` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '后台人员ID',
  `nickname` char(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '密码',
  `login_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `login_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '登陆ip地址',
  `login_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '最后登陆时间',
  `update_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `sort` int(4) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '锁定',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`nickname`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='后台账户表';

-- ----------------------------
-- Records of ebcms_user
-- ----------------------------
INSERT INTO `ebcms_user` VALUES ('1', 'admin', 'f9c202f6438dbcb14e71451046b1e4de', '712', '0', '2016-01-28 21:25:49', '2015-07-25 17:04:49', '2015-07-25 17:04:49', '99', '1', '1');
INSERT INTO `ebcms_user` VALUES ('2', '管理员', 'f9c202f6438dbcb14e71451046b1e4de', '40', '0', '2016-01-28 21:26:05', '2015-12-09 10:39:03', '2015-08-05 20:51:04', '1', '1', '1');
INSERT INTO `ebcms_user` VALUES ('3', '编辑', 'f9c202f6438dbcb14e71451046b1e4de', '3', '0', '2016-01-17 20:07:58', '2015-12-09 10:39:12', '2015-08-05 20:51:36', '1', '0', '1');
