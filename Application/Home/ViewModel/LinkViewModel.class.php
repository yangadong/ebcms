<?php
namespace Home\ViewModel;
use Think\Model\ViewModel;
class LinkViewModel extends ViewModel {
	public $viewFields = array(
		'Link'=>array(
			'_table'=>'__LINK__',
			'id',
			'title',
			'url',
			'logo',
			'description',
			'info',
			'sort',
			),
	);
}