<?php
namespace Home\ViewModel;
use Think\Model\ViewModel;
class ArticleViewModel extends ViewModel {
	public $viewFields = array(
		'Article'=>array(
			'_type'=>'LEFT',
			'_table'=>'__ARTICLE__',
			'id',
			'category_id',
			'title',
			'keywords',
			'thumb',
			'description',
			'mark',
			'tpl',
			'click',
			'sort',
			'status',
			'ext',
			'update_time',
			'create_time',
			'locked',
			),
		'Article_body'=>array(
			'_table'=>'__ARTICLE_BODY__',
			'_on'=>'Article_body.id=Article.id',
			'body',
			),
		'Articlecate'=>array(
			'_table'=>'__ARTICLECATE__',
			'_on'=>'Articlecate.id=Article.category_id',
			'mark'=>'category_mark',
			'title'=>'category_title',
			'tpl'=>'category_tpl',
			'tpl_detail'=>'tpl_detail',
			),
	);
}