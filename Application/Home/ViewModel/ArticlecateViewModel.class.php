<?php
namespace Home\ViewModel;
use Think\Model\ViewModel;
class ArticlecateViewModel extends ViewModel {
	public $viewFields = array(
		'Articlecate'=>array(
			'_table'=>"__ARTICLECATE__",
			'id',
			'mark',
			'text',
			'title',
			'keywords',
			'description',
			'sort',
			'status',
			'update_time',
			'create_time',
			'locked',
			),
	);
}