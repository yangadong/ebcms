<?php
namespace Home\ViewModel;
use Think\Model\ViewModel;
class NavcateViewModel extends ViewModel {
	public $viewFields = array(
		'Navcate'=>array(
			'_table'=>"__NAVCATE__",
			'id',
			'mark',
			'text',
			'sort',
			'status',
			'update_time',
			'create_time',
			'locked',
			),
	);
}