<?php
namespace Home\ViewModel;
use Think\Model\ViewModel;
class GuestbookViewModel extends ViewModel {
	public $viewFields = array(
		'Guestbook'=>array(
			'_table'=>'__GUESTBOOK__',
			'id',
			'nickname',
			'mobile',
			'content',
			'update_time',
			'reply',
			'reply_time',
			),
	);
}