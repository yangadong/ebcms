<?php
namespace Home\ViewModel;
use Think\Model\ViewModel;
class NavViewModel extends ViewModel {
	public $viewFields = array(
		'Nav'=>array(
			'_table'=>'__NAV__',
			'id',
			'pid',
			'category_id',
			'text',
			'ext',
			'url',
			'sort',
			),
	);
}