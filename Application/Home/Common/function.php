<?php 
// 获取网站位置
function position($home = '首页'){
	$pos = array();
	array_push($pos, array(
		'text' => $home,
		'url' => 'Index/index',
		));
	$tmp = FC(strtolower('SEO.site_'.CONTROLLER_NAME));
	if (!in_array(strtolower(CONTROLLER_NAME), array('index','article'))) {
		array_push($pos, array(
			'text' => $tmp['title'],
			'url' => CONTROLLER_NAME.'/index',
			));
		switch (strtolower(ACTION_NAME)) {
			case 'category':
				$id = I('id');
				$data = M(CONTROLLER_NAME.'cate') -> where('status=1') -> getField('id,pid,text',true);
				$pids = get_pids($id,$data);
				foreach ($pids as $key => $value) {
					array_push($pos, array(
						'text' => $value['text'],
						'url' => CONTROLLER_NAME.'/category?id='.$value['id'],
						));
				}
				break;
			case 'detail':
				$id = I('id');
				$category_id = M(CONTROLLER_NAME) -> where('id='.$id) -> getField('category_id');
				$data = M(CONTROLLER_NAME.'cate') -> where('status=1') -> getField('id,pid,text',true);
				$pids = get_pids($category_id,$data);
				foreach ($pids as $key => $value) {
					array_push($pos, array(
						'text' => $value['text'],
						'url' => CONTROLLER_NAME.'/category?id='.$value['id'],
						));
				}
				break;
		}
	}elseif (strtolower(CONTROLLER_NAME) == 'article') {
		switch (strtolower(ACTION_NAME)) {
			case 'index':
				$category = M('Articlecate') -> where(array('id'=>array('eq',I('id')))) -> find();
				array_push($pos, array(
					'text' => $category['title']?:$category['text'],
					'url' => CONTROLLER_NAME.'/index?id='.$category['id'],
					));
				break;

			case 'detail':
				if (I('id')) {
					$_where = array('id'=>array('eq',I('id')));
				}
				$data = M('Article') -> where($_where) -> find();
				if ($data) {
					$category = M('Articlecate') -> where(array('id'=>array('eq',$data['category_id']))) -> find();
					array_push($pos, array(
						'text' => $category['title']?:$category['text'],
						'url' => CONTROLLER_NAME.'/index?id='.$category['id'],
						));
					array_push($pos, array(
						'text' => $data['title'],
						'url' => CONTROLLER_NAME.'/detail?id='.$data['id'],
						));
				}
				break;
			
			default:
				# code...
				break;
		}
	}
	return $pos;
}

// 获取父id
function get_pids($id,$data){
	$tmp = array($data[$id]);
	if (isset($data[$data[$id]['pid']])) {
		$tmp = array_merge(get_pids($data[$id]['pid'],$data),$tmp);
	}
	return $tmp;
}