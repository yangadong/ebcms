<?php
namespace Home\Taglib;
use Think\Template\TagLib;
class Qy extends TagLib{

	// 标签定义
	protected $tags =  array(
		'articlelist'			=>	array('attr'=>'id,mark,limit,return','close'=>0),
		'articlecontent'		=>	array('attr'=>'id,mark,return','close'=>0),
		'navlist'				=>	array('attr'=>'mark,recursion,pid,limit,return','close'=>0),
		'linklist'				=>	array('attr'=>'limit,return','close'=>0),
		'guestbooklist'			=>	array('attr'=>'limit,return','close'=>0),
		'recommendlist'			=>	array('attr'=>'mark,limit,return','close'=>0),
		'print'					=>	array('attr'=>'name','close'=>0),
		'fn'					=>	array('attr'=>'name,fn,par,return','close'=>0),
		);

	// 
	public function _articlelist($tag,$content) {
		if (empty($tag['return']) || empty($tag['mark'])) {//return为空 返回空
			return;
		}
		$str = '';
		$str .="<?php ";
		$str .="\$_m = D('Article','ViewModel');\r\n";
		if (isset($tag['order'])) {
			$str .="\$_m -> order('{$tag['order']}');";
		}else{
			$str .="\$_m -> order('sort desc,id desc');\r\n";
		}
		$str .="\$_where = array();\r\n";
		$str .="\$_where['status'] = array('eq',1);\r\n";
		// 解析 mark/id 限制
		if (isset($tag['id'])) {
			if ('$' == substr($tag['id'],0,1)) {
				$tag['id']   = $this->autoBuildVar(substr($tag['id'], 1));
				$str .="\$_category_id = M('Articlecate') -> where(array('id'=>array('eq',{$tag['id']}))) -> limit(1) -> getField('id');\r\n";
			}else{
				$str .="\$_category_id = M('Articlecate') -> where(array('id'=>array('eq','{$tag['id']}'))) -> limit(1) -> getField('id');\r\n";
			}
		}else{
			if ('$' == substr($tag['mark'],0,1)) {
				$tag['mark']   = $this->autoBuildVar(substr($tag['mark'], 1));
				$str .="\$_category_id = M('Articlecate') -> where(array('mark'=>array('eq',{$tag['mark']}))) -> limit(1) -> getField('id');\r\n";
			}else{
				$str .="\$_category_id = M('Articlecate') -> where(array('mark'=>array('eq','{$tag['mark']}'))) -> limit(1) -> getField('id');\r\n";
			}
		}
		$str .="\$_where['category_id'] = array('eq',\$_category_id);\r\n";
		$str .="\$_m -> where(\$_where);\r\n";
		// 解析limit限制属性
		if (isset($tag['limit'])) {
			if ('$' == substr($tag['limit'],0,1)) {
				$tag['limit']   = $this->autoBuildVar(substr($tag['limit'], 1));
				$str .="\$_m -> limit({$tag['limit']});";
			}else{
				$str .="\$_m -> limit('{$tag['limit']}');";
			}
		}
		$str .="\${$tag['return']} = \$_m -> select();\r\n";
		$str .="?>";
		return $str;
	}

	// 解析内容标签 mark,id,return
	public function _articlecontent($tag,$content,$module='Article'){
		if (empty($tag['return']) || (empty($tag['id']) && empty($tag['mark']))) {//module或者return为空 返回空
			return;
		}
		$str = '';
		$str .="<?php ";
		$str .="\$_m = D('{$module}','ViewModel');\r\n";
		$str .="\$_where = array();\r\n";
		$str .="\$_where['status'] = array('eq',1);\r\n";
		// id限制
		if (isset($tag['id'])) {
			if ('$' == substr($tag['id'],0,1)) {
				$tag['id'] = $this->autoBuildVar(substr($tag['id'], 1));
				$str .="\$_where['id'] = array('eq',{$tag['id']});\r\n";
			}else{
				$str .="\$_where['id'] = array('eq','{$tag['id']}');\r\n";
			}
		}else{
			if ('$' == substr($tag['mark'],0,1)) {
				$tag['mark'] = $this->autoBuildVar(substr($tag['mark'], 1));
				$str .="\$_where['mark'] = array('eq',{$tag['mark']});\r\n";
			}else{
				$str .="\$_where['mark'] = array('eq','{$tag['mark']}');\r\n";
			}
		}
		$str .="\$_m -> where(\$_where);\r\n";
		$str .="\${$tag['return']} = \$_m -> find();\r\n";
		$str .="?>";
		return $str;
	}

	// 
	public function _navlist($tag,$content) {
		if (empty($tag['return'])) {//return为空 返回空
			return;
		}
		if (empty($tag['mark'])) {//mark为空 返回空
			return;
		}
		$str = '';
		$str .="<?php ";
		$str .="\$_m = D('Nav','ViewModel');\r\n";
		$str .="\$_m -> order('sort desc,id desc');\r\n";
		$str .="\$_where = array();\r\n";
		$str .="\$_where['status'] = array('eq',1);\r\n";
		// 解析 mark 限制
		if ('$' == substr($tag['mark'],0,1)) {
			$tag['mark']   = $this->autoBuildVar(substr($tag['mark'], 1));
			$str .="\$_category_id = M('Navcate') -> where(array('mark'=>array('eq',{$tag['mark']}),'status'=>array('eq',1))) -> limit(1) -> getField('id');\r\n";
		}else{
			$str .="\$_category_id = M('Navcate') -> where(array('mark'=>array('eq','{$tag['mark']}'),'status'=>array('eq',1))) -> limit(1) -> getField('id');\r\n";
		}
		$str .="\$_where['category_id'] = array('eq',\$_category_id);\r\n";
		// 解析 recursion 限制
		if (isset($tag['recursion'])) {
			$str .="\${$tag['return']}_tmp = \$_m -> where(\$_where) -> select();\r\n";
			// pid限制
			if (isset($tag['pid'])) {
				if ('$' == substr($tag['pid'],0,1)) {
					$tag['pid']   = $this->autoBuildVar(substr($tag['pid'], 1));
					$str .="\${$tag['return']} = data2subtree(\${$tag['return']}_tmp,{$tag['pid']});\r\n";
				}else{
					$str .="\${$tag['return']} = data2subtree(\${$tag['return']}_tmp,'{$tag['pid']}');\r\n";
				}
			}else{
				$str .="\${$tag['return']} = data2tree(\${$tag['return']}_tmp);\r\n";
			}
		}else{
			// 解析limit限制属性
			if (isset($tag['limit'])) {
				if ('$' == substr($tag['limit'],0,1)) {
					$tag['limit']   = $this->autoBuildVar(substr($tag['limit'], 1));
					$str .="\$_m -> limit({$tag['limit']});";
				}else{
					$str .="\$_m -> limit('{$tag['limit']}');";
				}
			}
			// pid限制
			if (isset($tag['pid'])) {
				if ('$' == substr($tag['pid'],0,1)) {
					$tag['pid']   = $this->autoBuildVar(substr($tag['pid'], 1));
					$str .="\$_where['pid'] = array('eq',{$tag['pid']});\r\n";
				}else{
					$str .="\$_where['pid'] = array('eq','{$tag['pid']}');\r\n";
				}
			}
			$str .="\$_m -> where(\$_where);\r\n";
			$str .="\${$tag['return']} = \$_m -> select();\r\n";
		}
		$str .="?>";
		return $str;
	}

	// 
	public function _linklist($tag,$content,$module='Link') {
		if (empty($tag['return'])) {//return为空 返回空
			return;
		}
		$str = '';
		$str .="<?php ";
		$str .="\$_m = D('{$module}','ViewModel');\r\n";
		$str .="\$_m -> order('sort desc,id desc');\r\n";
		$str .="\$_where = array();\r\n";
		$str .="\$_where['status'] = array('eq',1);\r\n";
		$str .="\$_m -> where(\$_where);\r\n";
		// 解析limit限制属性
		if (isset($tag['limit'])) {
			if ('$' == substr($tag['limit'],0,1)) {
				$tag['limit']   = $this->autoBuildVar(substr($tag['limit'], 1));
				$str .="\$_m -> limit({$tag['limit']});";
			}else{
				$str .="\$_m -> limit('{$tag['limit']}');";
			}
		}
		$str .="\${$tag['return']} = \$_m -> select();\r\n";
		$str .="?>";
		return $str;
	}

	public function _guestbooklist($tag,$content){
		return $this -> _linklist($tag,$content,'Guestbook');
	}

	// categoryid,limit,return
	public function _recommendlist($tag,$content) {
		if (empty($tag['mark']) || empty($tag['return'])) {
			return;
		}
		$str = '';
		$str .="<?php ";
		$str .="\$_category_id = M('Recommendcate') -> where(array('mark'=>array('eq','{$tag['mark']}'),'status'=>array('eq',1))) -> getField('id');";
		$str .="\$_m = M('Recommend');\r\n";
		$str .="\$_m -> order('sort desc,id desc');\r\n";
		$str .="\$_where = array();";
		$str .="\$_where['status'] = array('eq','1');";
		$str .="\$_where['category_id'] = array('eq',\$_category_id);";
		$str .="\$_m -> where(\$_where);";
		if (isset($tag['limit'])) {
			if ('$' == substr($tag['limit'],0,1)) {
				$tag['limit']   = $this->autoBuildVar(substr($tag['limit'], 1));
				$str .="\$_m -> limit({$tag['limit']});";
			}else{
				$str .="\$_m -> limit('{$tag['limit']}');";
			}
		}
		$str .="\${$tag['return']} = \$_m -> select();\r\n";
		$str .="?>";
		return $str;
	}

	// print调试标签
	public function _print($tag,$content) {
		if (empty($tag['name'])) {
			return;
		}
		if ('$' == substr($tag['name'],0,1)) {//当为变量的时候
			$name   = $this->autoBuildVar(substr($tag['name'], 1));
		}else{
			$name   = $this->autoBuildVar($tag['name']);
		}
		$str = "<?php ";
		$str .= "p({$name});";
		$str .= "?>";
		return $str;
	}

	// fn函数
	public function _fn($tag,$content){
		if (empty($tag['name']) || empty($tag['fn'])) {
			return;
		}
		// 模板禁止使用函数
		$template_deny_funs = explode(',',C('TMPL_DENY_FUNC_LIST'));
		if(!in_array($tag['fn'],$template_deny_funs)){
			$tag['name'] = $this->autoBuildVar($tag['name']);
			$str = '';
			$str .= '<?php ';
			if (isset($tag['return'])) {
				$str .= "\${$tag['return']} = ";
			} else {
				$str .= "{$tag['name']} = ";
			}
			
			if(isset($tag['param'])){
				if(strstr($tag['param'],'###')){
					$tag['param'] = str_replace('###',$tag['name'],$tag['param']);
					$str .= "{$tag['fn']}({$tag['param']});\r\n";
				}else{
					$str .= "{$tag['fn']}({$tag['name']},{$tag['param']});\r\n";
				}
			}else{
				$str .= "{$tag['fn']}({$tag['name']});\r\n";
			}
			$str .= "?>";
			return $str;
		}
	}

}
