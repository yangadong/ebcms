<?php
namespace Home\Controller;
use Think\Controller;
class ArticleController extends CommonController {
    
    public function index(){
        $id = I('id');
        $category = M('Articlecate') -> where(array('status'=>array('eq',1))) -> find($id);
        if ($category) {
            $this -> category = $category;
            $m = D('Article','ViewModel');
            $_where = array(
                'status' => array('eq',1),
                'category_id' => array('eq',$category['id']),
                );
            if ($category['pagenum']) {
                $count = $m -> where($_where) -> count();
                $Page = new \Common\Vendor\Page($count,$category['pagenum']);
                $this -> lists = $m -> where($_where) -> order('sort desc,id desc') -> limit($Page->firstRow.','.$Page->listRows) -> select();
                $this -> pages = $Page->show();
            }else{
                $this -> lists = $m -> where($_where) -> order('sort desc,id desc') -> select();
            }
            $this -> SEO = array(
                'title' =>  $category['title']?:$category['text'],
                'keywords'  =>  $category['keywords'],
                'description'   =>  $category['description'],
                );
            $this -> display($category['tpl']);
        }else{
            $this -> error('分类不存在！');
        }
    }

    // 内容详细页面
    public function detail(){
        $input = I();
        $_where = array();
        $_where['status'] = array('eq',1);
        $_where['Articlecate.status'] = array('eq',1);
        if (isset($input['id'])) {
            $_where['id'] = array('eq',$input['id']);
        }elseif (isset($input['mark'])) {
            $_where['mark'] = array('eq',$input['mark']);
            if ($data = D('Article','ViewModel') -> where($_where) -> find()) {
                $this->redirect('Home/Article/detail', array('id' => $data['id']));
            }else{
                $this -> error('对不起，你所访问的内容不存在！');
            }
        }else{
            $this -> error('对不起，你所访问的内容不存在！');
        }
        if (!$data = D('Article','ViewModel') -> where($_where) -> find()) {
            $this -> error('对不起，你所访问的内容不存在！');
        }
        $data['ext'] = json_decode($data['ext'],true);
        $this -> assign($data);
        $this -> SEO = array(
            'title' =>  $data['title'],
            'keywords'  =>  $data['keywords'],
            'description'   =>  $data['description'],
            );
        $this -> display($data['tpl']?:$data['tpl_detail']);
    }
}