<?php
namespace Common\Controller;
use Think\Controller;
class BaseController extends Controller {

	public function _initialize(){
		$rbac = new \Org\Util\Rbac();
		$rbac::checkLogin();
		if (!$rbac::AccessDecision()) {
			$this -> error('没有权限');
		}
	}

	public function _empty(){
		if (IS_POST) {
			op_log('[POST]访问不存在的动作');
			$this -> error('访问不存在的动作','',true);
		}elseif (IS_GET) {
			op_log('[GET]访问不存在的动作');
			$this -> msg = '访问不存在的动作';
			$this -> display('Common:showmsg');die;
		}
		die;
	}
}