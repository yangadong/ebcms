<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用入口文件

// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
// define('APP_DEBUG',true);

// 定义网站目录（若要开启虚拟主机多站点，请删掉下面的注释，并且把所有文件夹放入对应域名目录下）
// if (strpos($_SERVER['HTTP_HOST'], 'www.')===0) {
// 	define('__GROUP__','/'.strtolower(substr($_SERVER['HTTP_HOST'], 4)));
// }else{
// 	define('__GROUP__','/'.strtolower($_SERVER['HTTP_HOST']));
// }
define('__GROUP__','');

// 定义应用目录
define('APP_PATH','.'.__GROUP__.'/Application/');

// 引入ThinkPHP入口文件
require '.'.__GROUP__.'/ThinkPHP/ThinkPHP.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单